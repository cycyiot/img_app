#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <camera/camera.h>
#include <net/cmd_handle.h>

int camera_param_cmd_handler(struct cmd_param *cmdParam)
{
	int param, ret = -1;

	if(cmdParam->id == CMD_FACTORY_RESTORE_SET)
		ret = camera_set_default_param();
	else if((!is_error(cmdParam->param)) && (json_object_get_type(cmdParam->param)==json_type_int)){
		param = json_object_get_int(cmdParam->param);
		ret = camera_control(cmdParam->id, param);
	}
	cmd_response(ret, cmdParam);
	return ret;
}

struct cmd camera_cmd[] = {
	{CMD_FACTORY_RESTORE_SET, camera_param_cmd_handler},
	{CMD_AWB_SET, camera_param_cmd_handler},
	{CMD_AE_SET, camera_param_cmd_handler},
	{CMD_BRIGHTNESS_SET, camera_param_cmd_handler},
	{CMD_CONTRAST_SET, camera_param_cmd_handler},
};

int camera_cmd_init(void)
{
	return cmd_register(camera_cmd, ARRAY_SIZE(camera_cmd));
}
