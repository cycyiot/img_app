#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>             
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <asm/types.h>         
#include <time.h>
#include <pthread.h>
#include <camera/camera.h>
#include <ctype.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <semaphore.h>
#include <sys/ipc.h>

union semun
{
    int val;
    struct semid_ds *buf;
    unsigned short *arry;
};

static int sem_id1, sem_id2;
static int shm_id, shm_fmt_id;   
static char * shm_ptr = NULL;
static char * shm_fmt_ptr = NULL;

struct VideoOpr g_VideoSharedOpr;

int set_semvalue(int sem_id,int val)
{
    union semun sem_union;
 
    sem_union.val = val;
    if(semctl(sem_id, 0, SETVAL, sem_union) == -1){
        return 0;
    }
	
    return 1;
}

int semaphore_p(int sem_id)
{
    struct sembuf sem_b;
    sem_b.sem_num = 0;
    sem_b.sem_op = -1;
    sem_b.sem_flg = SEM_UNDO;
    if(semop(sem_id, &sem_b, 1) == -1){
        fprintf(stderr, "semaphore_p failed\n");
        return 0;
    }
	
    return 1;
}
 
int semaphore_v(int sem_id)
{
    struct sembuf sem_b;
    sem_b.sem_num = 0;
    sem_b.sem_op = 1;
    sem_b.sem_flg = SEM_UNDO;
    if(semop(sem_id, &sem_b, 1) == -1){
        fprintf(stderr, "semaphore_v failed\n");
        return 0;
    }
	
    return 1;
}

static int camera_ctrl_set(struct camera_data *camera_dev, int ctrlId, int ctrlVal)
{	
	return 0;
}

static int camera_ctrl_get(struct camera_data *camera_dev, int ctrlId, int *value)
{	
	return 0;
}

static int camera_frame_to_user(struct camera_data *camera_dev)
{
	semaphore_p(sem_id2);
	semaphore_p(sem_id1);
	memcpy(&camera_dev->phy_buf,shm_ptr,sizeof(struct v4l2_buffer));
	semaphore_v(sem_id1);
	
	return 0;
}

static int camera_dev_start(struct camera_data *camera_dev)
{	
	struct camera_data camera_tmp;

	//	semaphore_p(sem_fmt_id);
	memcpy(&camera_tmp, shm_fmt_ptr, sizeof(struct camera_data));
	if(camera_tmp.format != V4L2_PIX_FMT_YUV420){
		fprintf(stderr, "camera1 start failed!\n");
		PRINTF("fps = %d, width = %d, height = %d, format = %d\n", camera_tmp.fps, camera_tmp.width, camera_tmp.height, camera_tmp.format);
		return -1;
	}

	camera_dev->fps = camera_tmp.fps;
	camera_dev->width = camera_tmp.width;
	camera_dev->height = camera_tmp.height;
	camera_dev->format = camera_tmp.format;
	camera_dev->photograph.width = camera_tmp.width;
	camera_dev->photograph.height = camera_tmp.height;
	camera_dev->photograph.format = camera_tmp.format;
	camera_dev->photograph.fps = 20;
	PRINTF("camera1: fps = %d, width = %d, height = %d\n", camera_dev->fps, camera_dev->width, camera_dev->height);

	return 0;
}

static int camera_dev_stop(struct camera_data *camera_dev)
{
	return 0;
}

static int camera_take_picture_init(struct camera_data *camera_dev)
{
	return 0;
}

static int camera_take_picture_exit(struct camera_data *camera_dev)
{
	return 0;
}

static int camera_take_picture_start(struct camera_data *camera_dev)
{
	return 0;
}

static int camera_take_picture_stop(struct camera_data *camera_dev)
{
	return 0;
}

static int camera_init(struct camera_data *camera_dev)
{
	sem_id1 = semget((key_t)2234, 1, 0666 | IPC_CREAT);
	sem_id2 = semget((key_t)2235, 1, 0666 | IPC_CREAT);

//	set_semvalue(sem_id1,1);
//	set_semvalue(sem_id2,0);
	
	shm_id = shmget(777,1024,IPC_CREAT);	
	if(shm_id == -1){
        perror("shmget failed");
		return -1;
    }
	shm_ptr = (char *)shmat(shm_id, 0, 0); 
	shm_fmt_id = shmget(778,1024,IPC_CREAT);	
	if(shm_fmt_id == -1){
        perror("shmget failed");
		return -1;
    }
	shm_fmt_ptr = (char *)shmat(shm_fmt_id, 0, 0); 

	return 0;
}

static void camera_exit(struct camera_data *camera_dev)
{	
}

struct VideoOpr g_VideoSharedOpr = {
    .InitDevice  = camera_init,
    .ExitDevice  = camera_exit,
    .GetFrame    = camera_frame_to_user,
    .StartDevice = camera_dev_start,
    .StopDevice  = camera_dev_stop,
    .CtrlSet	 = camera_ctrl_set,
    .CtrlGet	 = camera_ctrl_get,
    .InitTakePicture  = camera_take_picture_init,
    .ExitTakePicture  = camera_take_picture_exit,
    .StartTakePicture = camera_take_picture_start,
    .StopTakePicture  = camera_take_picture_stop,
};
