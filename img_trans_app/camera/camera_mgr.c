#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <camera/camera.h>
#include <system/event.h>
#include <errno.h>

static struct camera_data *g_VideoDevHead = NULL;

static pthread_t pthread_id;
static pthread_mutex_t mutex;

extern struct VideoOpr g_VideoV4l2Opr;

#ifdef FLOW_USED
static struct camera_data *g_VideoSharedDev = NULL;
extern struct VideoOpr g_VideoSharedOpr;
#endif

int camera0_dev_init(struct camera_data *dev)
{
	dev->fps = VID_FPS;
	dev->width = VID_CAP_WIDTH;
	dev->height = VID_CAP_HEIGHT;
	dev->format = V4L2_PIX_FMT_YUV420;
	dev->photograph.width = IMG_CAP_WIDTH;
	dev->photograph.height = IMG_CAP_HEIGHT;
	dev->photograph.format = V4L2_PIX_FMT_YUV420;
	dev->photograph.fps = 20;
	return 0;
}

void camera_opr_init(struct camera_data *camera_dev)
{
	camera_dev->opr = &g_VideoV4l2Opr;
}

#ifdef FLOW_USED
struct camera_data *video_sharead_dev_init(void)
{

	g_VideoSharedDev = (struct camera_data *)malloc(sizeof(struct camera_data));
	if(g_VideoSharedDev == NULL){
		printf("%s:malloc error\n", __func__);
		goto err_exit;
	}
	g_VideoSharedDev->id = 1;
	g_VideoSharedDev->opr = &g_VideoSharedOpr;
	if(g_VideoSharedDev->opr->InitDevice(g_VideoSharedDev)){
		free(g_VideoSharedDev);
		g_VideoSharedDev = NULL;
		printf("%s failed\n", __func__);
		goto err_exit;
	}
err_exit:
	return g_VideoSharedDev;
}
#endif

struct camera_data *camera_dev_register(const int id)
{
	struct camera_data *Tmp;
	struct camera_data *dev = NULL;
	
	pthread_mutex_lock(&mutex);
	dev = (struct camera_data *)malloc(sizeof(struct camera_data));
	if(dev == NULL){
		printf("%s:malloc error\n", __func__);
		goto err_exit;
	}
	dev->id = id;
	camera_opr_init(dev);
	if(dev->opr->InitDevice(dev)){
		free(dev);
		dev = NULL;
		printf("%s: camera%d init error !!!\n", __func__, id);
		goto err_exit;
	}
	if(!g_VideoDevHead){
		g_VideoDevHead = dev;
	}else{
		Tmp = g_VideoDevHead;
		while (Tmp->ptNext)
		{
			Tmp = Tmp->ptNext;
		}
		Tmp->ptNext = dev;
	}
	dev->ptNext = NULL;
err_exit:
	pthread_mutex_unlock(&mutex);
	return dev;
}

static void *frame_get_function(void *param)
{
	struct camera_data *camera_dev;
	fd_set fdSet;	
	struct timeval time;
//	struct event_data event;
	int ret;
	int fd_max;
	
	while(1)
	{
		time.tv_sec = 2;
		time.tv_usec = 0;
		FD_ZERO(&fdSet);
		camera_dev = g_VideoDevHead;
		fd_max = -1;
		while(camera_dev)
		{
			if(!camera_dev->running_state){			
				FD_SET(camera_dev->fd, &fdSet);
				if(camera_dev->fd > fd_max)
					fd_max = camera_dev->fd;
			}
			camera_dev = camera_dev->ptNext;
		}
		if(fd_max < 0){
			sleep(1);
			continue;
		}
		ret = select(fd_max + 1, &fdSet, NULL, NULL, &time);
		if(ret > 0){
			while(ret--)
			{
				camera_dev = g_VideoDevHead;
				while(camera_dev)
				{
					if(FD_ISSET(camera_dev->fd, &fdSet) > 0){
						camera_frame_get(camera_dev);
						break;
					}
					camera_dev = camera_dev->ptNext;
				}
			}
		}else{
//			event.id = CAMERA_ERROR;
//			event_put(&event);
			PRINTF("camera: select error(%d), fd_max = %d!\n", ret, fd_max);
		}
	}
	return NULL;
}

int camera_mgr_init(void)
{
	int ret;
	struct camera_data *dev;

	pthread_mutex_init(&mutex, NULL);
	
	dev = camera_dev_register(0);
	if(dev != NULL){
		ret = camera0_dev_init(dev);
	}
	
#ifdef FLOW_USED
	video_sharead_dev_init();
#endif

	ret = pthread_create(&pthread_id, NULL, frame_get_function, NULL);
	if(ret != 0) {
		printf("create camera mgr pthread failed!\n");
		goto err_exit;
	}
	ret = camera_cmd_init();
err_exit:
	return ret;
}

struct camera_data *camera_get_video_device(const int id)
{
	struct camera_data *Tmp = g_VideoDevHead;

#ifdef FLOW_USED
	if(id == 1){	//video shared
		return g_VideoSharedDev;
	}else{
#endif
		while(Tmp)
		{
			if(Tmp->id == id)
				return Tmp;
			Tmp = Tmp->ptNext;
		}
#ifdef FLOW_USED
	}
#endif	
	return NULL;
}
