#include <camera/camera.h>
#include <cmd.h>
#include <unistd.h>

static enum v4l2_auto_n_preset_white_balance awb_table[] = {V4L2_WHITE_BALANCE_AUTO,  V4L2_WHITE_BALANCE_DAYLIGHT, V4L2_WHITE_BALANCE_INCANDESCENT,
													 V4L2_WHITE_BALANCE_HORIZON, V4L2_WHITE_BALANCE_FLUORESCENT_H, V4L2_WHITE_BALANCE_FLUORESCENT};

static struct camera_data *gp_camera_dev;

int camera_control(CMD cmd_id, int param)
{
	int ret = -1;
	static int exp_val, auto_exp = 0;

	gp_camera_dev = camera_get_video_device(0);
	if(gp_camera_dev == NULL)
		goto err_exit;
	switch(cmd_id)
	{
		case CMD_AWB_SET:
			if( param < (sizeof(awb_table)/sizeof(awb_table[0]))){
				ret = gp_camera_dev->opr->CtrlSet(gp_camera_dev, V4L2_CID_AUTO_N_PRESET_WHITE_BALANCE, awb_table[param]);
				if(!ret){
					gp_camera_dev->sys_param.awb = param;
				}
			}
			break;
			
		case CMD_AE_SET:
			if(!param){	/* auto exposure */
				ret = gp_camera_dev->opr->CtrlSet(gp_camera_dev, V4L2_CID_EXPOSURE_AUTO, V4L2_EXPOSURE_AUTO);
				auto_exp = 0;
			}else{
				if(!auto_exp){
					auto_exp = 1;
					gp_camera_dev->opr->CtrlGet(gp_camera_dev, V4L2_CID_EXPOSURE, &exp_val);
					gp_camera_dev->opr->CtrlSet(gp_camera_dev, V4L2_CID_EXPOSURE_AUTO, V4L2_EXPOSURE_MANUAL);
				}
				ret = gp_camera_dev->opr->CtrlSet(gp_camera_dev, V4L2_CID_EXPOSURE, exp_val+(param*exp_val/8));
			}
			if(!ret){
				gp_camera_dev->sys_param.ae = param;
			}
			break;
			
		case CMD_BRIGHTNESS_SET:
			if(auto_exp){
				gp_camera_dev->opr->CtrlSet(gp_camera_dev, V4L2_CID_EXPOSURE_AUTO, V4L2_EXPOSURE_AUTO);
				auto_exp = 0;
			}
			ret = gp_camera_dev->opr->CtrlSet(gp_camera_dev, V4L2_CID_AUTO_EXPOSURE_BIAS, param);
			if(!ret){
				gp_camera_dev->sys_param.brightness = param;
			}
			break;
			
		case CMD_CONTRAST_SET:
			ret = gp_camera_dev->opr->CtrlSet(gp_camera_dev, V4L2_CID_CONTRAST, (3+param)<<2);
			if(!ret){
				gp_camera_dev->sys_param.contrast = param;
			}
			break;
			
		default:
			break;
	}
err_exit:
	return ret;
}

int camera_set_default_param(void)
{
	int ret;

	ret = camera_control(CMD_AWB_SET, 0);
	ret |= camera_control(CMD_BRIGHTNESS_SET, 0);
	ret |= camera_control(CMD_AE_SET, 0);
	ret |= camera_control(CMD_CONTRAST_SET, 0);

	return ret;
}

int camera_param_load(void)
{
	int ret = -1;

	gp_camera_dev = camera_get_video_device(0);
	if(gp_camera_dev != NULL){
		ret = camera_control(CMD_AE_SET, gp_camera_dev->sys_param.ae);
		ret |= camera_control(CMD_BRIGHTNESS_SET, gp_camera_dev->sys_param.brightness);
		ret |= camera_control(CMD_CONTRAST_SET, gp_camera_dev->sys_param.contrast);
		ret |= camera_control(CMD_AWB_SET, gp_camera_dev->sys_param.awb);
	}
	return ret;
}
