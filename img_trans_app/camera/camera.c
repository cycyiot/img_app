#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>             
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <asm/types.h>         
#include <time.h>
#include <pthread.h>
#include <camera/camera.h>
#include <ctype.h>
#include <action/action_take_photos.h>


#define CAMERA_CLEAR(x)			memset(&(x), 0, sizeof(x))

struct VideoOpr g_VideoV4l2Opr;

#define WATERMARK_EN

#ifdef WATERMARK_EN
#define WATERMARK_FILE_PATH "/img_app/bin/watermark.yuv"
#define WATERMARK_WIDTH 256
#define WATERMARK_HEIGHT 160
#define WATERMARK_ALPHA 255
#define WATERMARK_X_OFFSET 20
#define WATERMARK_Y_OFFSET 20

static unsigned char background_y;
static unsigned char background_u;
static unsigned char background_v;
static unsigned char *src_buf = NULL;

int camera_watermark_add(struct camera_data *camera_dev)
{
	unsigned char *srcY = NULL;
	unsigned char *srcU = NULL;
	unsigned char *srcV = NULL;
	unsigned char *dstY = NULL;
	unsigned char *dstU = NULL;
	unsigned char *dstV = NULL;
	unsigned char *dstOverlayY = NULL;
	unsigned char *dstOverlayU = NULL;
	unsigned char *dstOverlayV = NULL;
	int row, col, dst_width, dst_height, x_offset, y_offset;

	if((camera_dev == NULL) || (src_buf == NULL)){
		return -1;
	}

	if(camera_dev->mode == V4L2_MODE_IMAGE){
		dst_width = camera_dev->photograph.width;
		dst_height = camera_dev->photograph.height;
	}else{
		dst_width = camera_dev->width;
		dst_height = camera_dev->height;
	}
	x_offset = dst_width - WATERMARK_WIDTH - WATERMARK_X_OFFSET;
	y_offset = dst_height - WATERMARK_HEIGHT - WATERMARK_Y_OFFSET;

	srcY = src_buf;
	srcU = srcY + WATERMARK_WIDTH * WATERMARK_HEIGHT;
	srcV = srcU + ((WATERMARK_WIDTH * WATERMARK_HEIGHT) >> 2);

	dstY = camera_dev->virtual_buf[camera_dev->phy_buf.index];
	dstU = dstY + dst_width * dst_height;
	dstV = dstU + ((dst_width * dst_height) >> 2);

	dstOverlayY = dstY + y_offset * dst_width + x_offset;
	dstOverlayU = dstU + (y_offset >> 1) * (dst_width >> 1) + (x_offset >> 1);
	dstOverlayV = dstV + (y_offset >> 1) * (dst_width >> 1) + (x_offset >> 1);

	for(row = 0; row < WATERMARK_HEIGHT; ++row){
		for(col = 0; col < WATERMARK_WIDTH; ++col){
			if(*srcY != background_y){
				*dstOverlayY = ((255 - WATERMARK_ALPHA) * (*dstOverlayY) + (*srcY) * WATERMARK_ALPHA) >> 8;
			}
			++srcY;
		    ++dstOverlayY;
		}
		dstOverlayY = dstOverlayY + dst_width - WATERMARK_WIDTH;
	}

	for(row = 0; row < (WATERMARK_HEIGHT >> 1); ++row){
		for(col = 0; col < (WATERMARK_WIDTH >> 1); ++col){
			if(*srcU != background_u){
				 *dstOverlayU = ((255 - WATERMARK_ALPHA) * (*dstOverlayU) + (*srcU) * WATERMARK_ALPHA) >> 8;
			}
			++srcU;
			++dstOverlayU;
			if(*srcV != background_v){
				*dstOverlayV = ((255 - WATERMARK_ALPHA) * (*dstOverlayV) + (*srcV) * WATERMARK_ALPHA) >> 8;
			}
			++srcV;
			++dstOverlayV;
		}
		dstOverlayU = dstOverlayU + ((dst_width - WATERMARK_WIDTH) >> 1);
		dstOverlayV = dstOverlayV + ((dst_width - WATERMARK_WIDTH) >> 1);
	}

	return 0;
}

int camera_watermark_init(void)
{
	FILE *fp; 

	fp = fopen(WATERMARK_FILE_PATH, "r");
	if(fp == NULL){
	      perror("open watermark file failed!\n");
	      return -1;
	}
	src_buf = malloc(WATERMARK_WIDTH*WATERMARK_HEIGHT*3/2);
	if(src_buf == NULL){
	      perror("malloc error!\n");
	      return -1;
	}
	fread(src_buf, WATERMARK_WIDTH*WATERMARK_HEIGHT*3/2, 1, fp);
	background_y = src_buf[0];
	background_u = src_buf[WATERMARK_WIDTH*WATERMARK_HEIGHT];
	background_v = src_buf[WATERMARK_WIDTH*WATERMARK_HEIGHT*5/4];
//	PRINTF("%s: y: %x, u: %x, v: %x\n", __func__, background_y, background_u, background_v);
	fclose(fp);
	return 0;
}

void camera_watermark_uninit(void)
{
	if(src_buf != NULL)
		free(src_buf);
}
#endif

static int camera_ctrl_set(struct camera_data *camera_dev, int ctrlId, int ctrlVal)
{	
	int ret = -1;
	struct v4l2_control control;
	
	CAMERA_CLEAR(control);
	control.id = ctrlId;
	control.value = ctrlVal;
	ret = ioctl(camera_dev->fd, VIDIOC_S_CTRL, &control);
	
	return ret;
}

static int camera_ctrl_get(struct camera_data *camera_dev, int ctrlId, int *value)
{	
	int ret = -1;
	if(camera_dev->id == 0){
		struct v4l2_control control;
		
		CAMERA_CLEAR(control);
		control.id = ctrlId;
		ret = ioctl(camera_dev->fd, VIDIOC_G_CTRL, &control);
		*value = control.value;
	}else{
	}
	
	return ret;
}

static int camera_buf_request(struct camera_data *camera_dev, unsigned int count)
{
	int ret;
	unsigned int i;
	struct v4l2_requestbuffers requestbuffers;
	struct v4l2_buffer v4l2Buf; 
	
	CAMERA_CLEAR(requestbuffers);
	requestbuffers.count = count;
	requestbuffers.type	= V4L2_BUF_TYPE_VIDEO_CAPTURE;
	requestbuffers.memory = V4L2_MEMORY_MMAP;	
    ret = ioctl(camera_dev->fd, VIDIOC_REQBUFS, &requestbuffers);
	if(ret != 0){
		PRINTF("%s:ioctl v4l2 buf request error!, ret = %d, count = %d\n", __func__, ret, requestbuffers.count);
		goto errHdl;
    }
	
	for(i = 0; i < requestbuffers.count; i++){
		CAMERA_CLEAR(v4l2Buf);
		v4l2Buf.type		= V4L2_BUF_TYPE_VIDEO_CAPTURE;
		v4l2Buf.memory		= V4L2_MEMORY_MMAP;
		v4l2Buf.index		= i;
		ret = ioctl(camera_dev->fd, VIDIOC_QUERYBUF, &v4l2Buf);
		if(ret != 0){
			PRINTF("%s:ioctl v4l2 buf query error, ret = %d!\n", __func__, ret);
			goto errHdl;
	    }

		camera_dev->buf_len = v4l2Buf.length;;
		camera_dev->virtual_buf[i] = mmap(NULL, v4l2Buf.length, PROT_READ | PROT_WRITE, MAP_SHARED, camera_dev->fd, v4l2Buf.m.offset);
		if(camera_dev->virtual_buf[i] == MAP_FAILED){
			PRINTF("%s:mmap kernel space alloc buf to user space error!\n", __func__);
			goto errHdl;
		}

		v4l2Buf.type	= V4L2_BUF_TYPE_VIDEO_CAPTURE;
		v4l2Buf.memory	= V4L2_MEMORY_MMAP;
		v4l2Buf.index	= i;
		ret = ioctl(camera_dev->fd, VIDIOC_QBUF, &v4l2Buf);
		if(ret != 0){
			PRINTF("%s:ioctl v4l2 buf enqueue error!\n", __func__);
			goto errHdl;
	    }
	}
	
errHdl:
	return ret;
}

static int camera_param_set(struct camera_data *camera_dev)
{
	int ret = 0;
	struct v4l2_streamparm streamparam;

	CAMERA_CLEAR(streamparam);
	streamparam.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    streamparam.parm.capture.timeperframe.numerator = 1;
    streamparam.parm.capture.capturemode = camera_dev->mode;
	if(camera_dev->mode == V4L2_MODE_IMAGE){
		streamparam.parm.capture.timeperframe.denominator = camera_dev->photograph.fps;
	}else{
		streamparam.parm.capture.timeperframe.denominator = camera_dev->fps;
	}
    ret = ioctl(camera_dev->fd, VIDIOC_S_PARM, &streamparam);

	return ret;
}

static int camera_fmt_set(struct camera_data *camera_dev)
{
	int ret;
	struct v4l2_format format;
	struct v4l2_pix_format subch_fmt;
	
	CAMERA_CLEAR(format);
	if(camera_dev->mode == V4L2_MODE_IMAGE){
		format.type                	= V4L2_BUF_TYPE_VIDEO_CAPTURE;
	    format.fmt.pix.width       	= camera_dev->photograph.width; 	
	    format.fmt.pix.height      	= camera_dev->photograph.height;
	    format.fmt.pix.pixelformat 	= camera_dev->photograph.format;
	    format.fmt.pix.field       	= V4L2_FIELD_NONE;
		format.fmt.pix.subchannel	= NULL;
	}else{
		subch_fmt.width 		= FM_CAP_WIDTH;
		subch_fmt.height		= FM_CAP_HEIGHT;
		subch_fmt.pixelformat	= camera_dev->format; 		
		subch_fmt.field 		= V4L2_FIELD_NONE;
		subch_fmt.rot_angle 	= 0;
		format.type                	= V4L2_BUF_TYPE_VIDEO_CAPTURE;
	    format.fmt.pix.width       	= camera_dev->width; 	
	    format.fmt.pix.height      	= camera_dev->height;
	    format.fmt.pix.pixelformat 	= camera_dev->format;
	    format.fmt.pix.field       	= V4L2_FIELD_NONE;
		format.fmt.pix.subchannel	= &subch_fmt;
	}
    ret = ioctl(camera_dev->fd, VIDIOC_S_FMT, &format);
	PRINTF("%s: camera%d: mode = %d, width = %d, height = %d\n", __func__, camera_dev->id, camera_dev->mode, format.fmt.pix.width, format.fmt.pix.height);
	return ret;
}

static int camera_set_capture_mode(struct camera_data *camera_dev)
{
	int ret = 0;

	ret = camera_param_set(camera_dev);
	if(ret != 0){
		PRINTF("%s:set csi stream parameter error!\n", __func__);	
		goto errHdl;
	}
	ret = camera_fmt_set(camera_dev);
	if(ret != 0){
		PRINTF("%s:set csi resolution and format error!\n", __func__);	
		goto errHdl;
	}

errHdl:
	return ret;
}

static int camera_frame_to_user(struct camera_data *camera_dev)
{
	pthread_mutex_lock(&camera_dev->mutex);
	pthread_cond_wait(&camera_dev->frm_update, &camera_dev->mutex);
	pthread_mutex_unlock(&camera_dev->mutex);
	
	return 0;
}

static int camera_dev_start(struct camera_data *camera_dev)
{
	int ret = 0;
	enum v4l2_buf_type type;
	
	pthread_mutex_lock(&camera_dev->mutex);
	if(camera_dev->running_state){
		camera_dev->mode = V4L2_MODE_VIDEO;
		if(!camera_set_capture_mode(camera_dev)){
			if(!camera_buf_request(camera_dev, CAMERA_FRAME_BUF_NUM)){
				type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
				ret = ioctl(camera_dev->fd, VIDIOC_STREAMON, &type);
				if(ret != 0) {
					PRINTF("%s: stream on failed!\n", __func__);
					goto err_exit;
				}
				camera_dev->running_state = 0;
			}
		}
	}
err_exit:
	pthread_mutex_unlock(&camera_dev->mutex);
	
	return ret;
}

static int camera_dev_stop(struct camera_data *camera_dev)
{
	int ret = -1;
	int i;
	enum v4l2_buf_type type;
	
	pthread_mutex_lock(&camera_dev->mutex);
	if(!camera_dev->running_state){
		camera_dev->running_state = 1;
		type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		ret = ioctl(camera_dev->fd, VIDIOC_STREAMOFF, &type);
		for (i = 0; i < CAMERA_FRAME_BUF_NUM; i++)
		{
			ret = munmap(camera_dev->virtual_buf[i], camera_dev->buf_len);
			if(ret == -1){
				PRINTF("munmap error!\n");
			}
		}
	}
	pthread_mutex_unlock(&camera_dev->mutex);
	
	return ret;
}

static int camera_input_set(struct camera_data *camera_dev, unsigned int idx)
{
	int ret;
	struct v4l2_input input;

	CAMERA_CLEAR(input);
	input.index = idx;
	input.type = V4L2_INPUT_TYPE_CAMERA;
    ret = ioctl(camera_dev->fd, VIDIOC_S_INPUT, &input);

	return ret;
}

#if (IMG_CAP_WIDTH != VID_CAP_WIDTH) || (IMG_CAP_HEIGHT != VID_CAP_HEIGHT)
static int camera_take_picture_init(struct camera_data *camera_dev)
{
	return camera_dev_stop(camera_dev);
}

static int camera_take_picture_exit(struct camera_data *camera_dev)
{
	return camera_dev_start(camera_dev);
}

static int camera_take_picture_start(struct camera_data *camera_dev)
{
	int ret;
	enum v4l2_buf_type type;
	fd_set fdSet;	
	struct timeval time;
	
	time.tv_sec = 2;
	time.tv_usec = 0;
	camera_dev->mode = V4L2_MODE_IMAGE;
	ret = camera_set_capture_mode(camera_dev);
	if(ret != 0){
		PRINTF("V4L2_MODE_IMAGE set error!\n");
		goto err_failed;
	}
	ret = camera_buf_request(camera_dev, 1);
	if (ret != 0){
		PRINTF("%s: camera_buf_request error!\n", __func__);
		goto err_failed;
	}

	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	ret = ioctl(camera_dev->fd, VIDIOC_STREAMON, &type);
    if (ret < 0){
        PRINTF("VIDIOC_STREAMON error!\n");
		goto err_failed;
    }

	FD_ZERO(&fdSet);	
	FD_SET(camera_dev->fd, &fdSet);
	ret = select(camera_dev->fd + 1, &fdSet, NULL, NULL, &time);
	if(ret > 0){
		camera_dev->phy_buf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;	
		camera_dev->phy_buf.memory = V4L2_MEMORY_MMAP;
		ret = ioctl(camera_dev->fd, VIDIOC_DQBUF, &camera_dev->phy_buf);
		if(ret == 0){
#ifdef WATERMARK_EN
           if(get_cmdParam_id()== CMD_IMG_LOAD)
			camera_watermark_add(camera_dev);
#endif
			ret = ioctl(camera_dev->fd, VIDIOC_QBUF, &camera_dev->phy_buf);
		}else{
			PRINTF("%s: VIDIOC_DQBUF error!\n", __func__);
		}
	}else{
		PRINTF("%s: select error(%d)!\n", __func__, ret);
	}
err_failed:
	return ret;
}

static int camera_take_picture_stop(struct camera_data *camera_dev)
{
	int ret;
	enum v4l2_buf_type type;
	
	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	ioctl(camera_dev->fd, VIDIOC_STREAMOFF, &type);
  	ret = munmap(camera_dev->virtual_buf[0], camera_dev->buf_len);
	if (ret < 0)
		PRINTF("camera_buf_unmap error!\n");

	return ret;
}

#else
static int camera_take_picture_init(struct camera_data *camera_dev)
{
	return 0;
}

static int camera_take_picture_exit(struct camera_data *camera_dev)
{
	return 0;
}

static int camera_take_picture_start(struct camera_data *camera_dev)
{
	return camera_frame_to_user(camera_dev);
}

static int camera_take_picture_stop(struct camera_data *camera_dev)
{
	usleep(160000);
	return 0;
}

#endif

static int camera_init(struct camera_data *camera_dev)
{
	int ret = -1;
	char dev_path[16];

	camera_dev->running_state = 1;
	sprintf(dev_path, "/dev/video%d", camera_dev->id);
	camera_dev->fd = open(dev_path, O_RDWR);
	if(camera_dev->fd == -1){
		PRINTF("%s:open csi handle is not exist!\n", __func__);	
		goto errHdl;
	}

	ret = camera_input_set(camera_dev, 0);
	if(ret != 0){
		PRINTF("%s:set csi input error!\n", __func__);	
		goto errHdl;
	}
	pthread_mutex_init(&camera_dev->mutex, NULL);
	pthread_cond_init(&camera_dev->frm_update, NULL);
#ifdef WATERMARK_EN
	camera_watermark_init();
#endif
errHdl:
	return ret;
}

static void camera_exit(struct camera_data *camera_dev)
{	
	pthread_cond_destroy(&camera_dev->frm_update);
	pthread_mutex_destroy(&camera_dev->mutex);
	close(camera_dev->fd);
#ifdef WATERMARK_EN
	camera_watermark_uninit();
#endif
}

struct VideoOpr g_VideoV4l2Opr = {
    .InitDevice  = camera_init,
    .ExitDevice  = camera_exit,
    .GetFrame    = camera_frame_to_user,
    .StartDevice = camera_dev_start,
    .StopDevice  = camera_dev_stop,
    .CtrlSet	 = camera_ctrl_set,
    .CtrlGet	 = camera_ctrl_get,
    .InitTakePicture  = camera_take_picture_init,
    .ExitTakePicture  = camera_take_picture_exit,
    .StartTakePicture = camera_take_picture_start,
    .StopTakePicture  = camera_take_picture_stop,
};

int camera_frame_get(struct camera_data *camera_dev)
{
	int ret = -1;

	pthread_mutex_lock(&camera_dev->mutex);
	if(!camera_dev->running_state){
		camera_dev->phy_buf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;	
		camera_dev->phy_buf.memory = V4L2_MEMORY_MMAP;
		ret = ioctl(camera_dev->fd, VIDIOC_DQBUF, &camera_dev->phy_buf);
		if(ret == 0){
//			camera_watermark_add(camera_dev);
			pthread_cond_broadcast(&camera_dev->frm_update);
			ret = ioctl(camera_dev->fd, VIDIOC_QBUF, &camera_dev->phy_buf);
		}
	}
	pthread_mutex_unlock(&camera_dev->mutex);
	return ret;
}
