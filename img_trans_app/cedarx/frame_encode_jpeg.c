#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>
#include <cedarx/frame_encode.h>
#include <cedarx/jpeg_encode.h>
#include <platform_cfg.h>

typedef struct ven_jpeg_obj_context_st {
	struct ven_jpg_obj_st *obj;
	VencOutputBuffer output_buf;
}ven_jpeg_obj_context_st;


static FRM_OBJ *jpeg_create_obj(void)
{
	struct ven_jpg_obj_st *obj = NULL;
	struct ven_jpeg_obj_context_st *p_context = NULL;

	p_context = (struct ven_jpeg_obj_context_st *)malloc(sizeof(struct ven_jpeg_obj_context_st));
	if(!p_context) {
		PRINTF("%s: malloc error\n", __func__);
		return NULL;
	}
	memset(p_context, 0, sizeof(struct ven_jpeg_obj_context_st));

	obj = jpeg_encode_create();
	p_context->obj = obj;
	
	return (FRM_OBJ *)p_context;
}
static int jpeg_destroy_obj(FRM_OBJ *obj)
{
	struct ven_jpeg_obj_context_st *p_context = (struct ven_jpeg_obj_context_st *)obj;
	int ret = 0;

	if(!obj) {
		PRINTF("%s:invalid FRM_OBJ pointer\n", __func__);
		return -1;
	}
	
	ret = jpeg_encode_destroy(p_context->obj);
	if(ret) {
		PRINTF("%s:jpeg_encode_destroy error\n", __func__);
	}
	if(p_context)
		free(p_context);
	
	return 0;
}

static int jpeg_config_obj(FRM_OBJ *obj, frame_enc_config_st *cfg)
{
	struct ven_jpeg_obj_context_st *p_context = (struct ven_jpeg_obj_context_st *)obj;
	struct ven_jpg_cfg_st v_cfg;
	struct ven_thumb_cfg_st *thumb_cfg = NULL;
	struct ven_jpg_exif_st *exif_cfg = NULL;
	VencBaseConfig *p_basecfg = NULL;
	int ret = 0;

	if(!obj) {
		PRINTF("%s:invalid FRM_OBJ pointer\n", __func__);
		return -1;
	}
	if(!cfg) {
		PRINTF("%s:frame encode config error\n", __func__);
		return -1;
	}

	memset(&v_cfg, 0, sizeof(v_cfg));
	p_basecfg = &v_cfg.baseConfig;
	v_cfg.vbv_size = (cfg->spec_param.jpeg_spec.vbv_size > 0) ? 
					  cfg->spec_param.jpeg_spec.vbv_size:(4*1024*1024);

	p_basecfg->nInputWidth  = cfg->input_width;
	p_basecfg->nInputHeight = cfg->input_height;
	p_basecfg->nDstWidth    = cfg->output_width;
	p_basecfg->nDstHeight   = cfg->output_height;
	p_basecfg->nStride      = cfg->input_width;
	p_basecfg->eInputFormat = VENC_PIXEL_YUV420P;

	thumb_cfg = &v_cfg.ThumbCfg;
	if(cfg->spec_param.jpeg_spec.thumb_enable) {
		thumb_cfg->ThumbEn = 1;
		thumb_cfg->ThumbWidth = cfg->spec_param.jpeg_spec.thumb_width;
		thumb_cfg->ThumbHeight= cfg->spec_param.jpeg_spec.thumb_height;
	}
	exif_cfg = &v_cfg.exif_cfg;
	if(cfg->spec_param.jpeg_spec.exif.camera_manufact)
		exif_cfg->camera_manufac = cfg->spec_param.jpeg_spec.exif.camera_manufact;
	if(cfg->spec_param.jpeg_spec.exif.camera_model)
		exif_cfg->camera_model = cfg->spec_param.jpeg_spec.exif.camera_model;
	if(cfg->spec_param.jpeg_spec.exif.data_time)
		exif_cfg->data_time = cfg->spec_param.jpeg_spec.exif.data_time;

	ret = jpeg_encode_config(p_context->obj, &v_cfg);
	if(ret != 0) {
		PRINTF("%s:jpeg encode config error\n", __func__);
		return ret;
	}


	return ret;
}

int jpeg_get_vbvsize(FRM_OBJ *obj, int *p_vbvsize)
{

	struct ven_jpeg_obj_context_st *p_context = (struct ven_jpeg_obj_context_st *)obj;
	if(!obj) {
		PRINTF("%s:invalid FRM_OBJ pointer\n", __func__);
		return -1;
	}

	return jpeg_encode_get_vbv_info(p_context->obj, p_vbvsize);

}



static int jpeg_input_a_frame_by_phy_yc(FRM_OBJ *obj, int phy_addr_y, int phy_addr_c)
{
	struct ven_jpeg_obj_context_st *p_context = (struct ven_jpeg_obj_context_st *)obj;
	if(!obj) {
		PRINTF("%s:invalid FRM_OBJ pointer\n", __func__);
		return -1;
	}

	return jpeg_encode_input_a_frame_by_phy_yc(p_context->obj, phy_addr_y, phy_addr_c);
}

static int jpeg_input_a_frame_by_phy(FRM_OBJ *obj, int phy_addr)
{
	struct ven_jpeg_obj_context_st *p_context = (struct ven_jpeg_obj_context_st *)obj;
	int size_y;

	if(!obj) {
		PRINTF("%s:invalid FRM_OBJ pointer\n", __func__);
		return -1;
	}

	size_y = p_context->obj->input_height*p_context->obj->input_width;
	return jpeg_encode_input_a_frame_by_phy_yc(p_context->obj, phy_addr, phy_addr + size_y);
}
static int jpeg_get_thumbnail_a_frame(FRM_OBJ *obj, frame_enc_outinfo_st *out_info)
{
	struct ven_jpeg_obj_context_st *p_context = (struct ven_jpeg_obj_context_st *)obj;
	unsigned int addr, size;
	if(!obj) {
		PRINTF("%s:invalid FRM_OBJ pointer\n", __func__);
		return -1;
	}
	if(!out_info) {
		PRINTF("%s:frame encode out info error\n", __func__);
		return -1;
	}
	jpeg_encode_get_thumbnail_data(p_context->obj,&addr,&size);
	out_info->p_data0 = (unsigned char*)addr;
	out_info->size0	  = size;

	return 0;
}
static int jpeg_get_a_frame(FRM_OBJ *obj, frame_enc_outinfo_st *out_info)
{
	struct ven_jpeg_obj_context_st *p_context = (struct ven_jpeg_obj_context_st *)obj;
	VencOutputBuffer *p_output_buf;
	int ret = 0;

	if(!obj) {
		PRINTF("%s:invalid FRM_OBJ pointer\n", __func__);
		return -1;
	}
	if(!out_info) {
		PRINTF("%s:frame encode out info error\n", __func__);
		return -1;
	}

	p_output_buf = &p_context->output_buf;
	ret = jpeg_encode_get_one_bitstream_frame(p_context->obj, p_output_buf);
	
    out_info->size0   = p_output_buf->nSize0;
    out_info->size1   = p_output_buf->nSize1;
    out_info->p_data0 = p_output_buf->pData0;
    out_info->p_data1 = p_output_buf->pData1;
    out_info->p_priv  = NULL;

	return ret;
}
static int jpeg_put_a_frame(FRM_OBJ *obj, frame_enc_outinfo_st *out_info)
{
	struct ven_jpeg_obj_context_st *p_context = (struct ven_jpeg_obj_context_st *)obj;
	VencOutputBuffer *p_output_buf;
	int ret = 0;

	if(!obj) {
		PRINTF("%s:invalid FRM_OBJ pointer\n", __func__);
		return -1;
	}
	if(!out_info) {
		PRINTF("%s:frame encode out info error\n", __func__);
		return -1;
	}

	p_output_buf = &p_context->output_buf;
	ret = jpeg_encode_put_one_bitstream_frame(p_context->obj, p_output_buf);

	return ret;
}


static struct frame_encode_context_st jpeg_context = {
	.type = FRM_ENCODE_JPEG,
	.next = NULL,
	.frame_enc_create_obj  = jpeg_create_obj,
	.frame_enc_destroy_obj = jpeg_destroy_obj,
	.frame_enc_get_head_info = NULL,
	.frame_enc_config_obj = jpeg_config_obj,
	.frame_enc_get_vbvsize = jpeg_get_vbvsize,
	.frame_enc_input_a_frame_by_phy_yc = jpeg_input_a_frame_by_phy_yc,
	.frame_enc_input_a_frame_by_phy = jpeg_input_a_frame_by_phy,
	.frame_enc_input_a_frame_by_vrt = NULL,
	.frame_enc_get_a_frame = jpeg_get_a_frame,
	.frame_enc_put_a_frame = jpeg_put_a_frame,
	.frame_enc_get_thumbnail = jpeg_get_thumbnail_a_frame,
};



int frame_encode_jpeg_init(void)
{
	return frame_encode_context_register(&jpeg_context);
}

int frame_encode_jpeg_uninit(void)
{
	return frame_encode_context_unregister(&jpeg_context);
}


#if 0
INT32S main(int argc, char **argv)
{
	frame_enc_config_st frm_config;
	struct frame_encode_context_st *p_jpeg_frame_encode_ctx;
	FRM_OBJ *img_tranc_jpeg_frm_obj;
	int src_fd, out_fd;
	struct ion_mem_hdl_t *ion_mem = NULL;
	int ion_size = 0;
	struct frame_enc_outinfo_st frame_enc_outinfo;


	if(argc != 3) {
		PRINTF("./%s yuv.src outfile\n", argv[0]);
		return -1;
	}

	src_fd = open(argv[1], O_RDWR);
	if(src_fd == -1) {
		perror("open yuv source fail");
		return -1;
	}

	out_fd = open(argv[2], O_RDWR|O_CREAT|O_TRUNC);
	if(out_fd == -1) {
		perror("open out file fail");
		return -1;
	}



	frame_encode_init();

	p_jpeg_frame_encode_ctx = frame_encode_get_context(FRM_ENCODE_JPEG);
	if(!p_jpeg_frame_encode_ctx) {
		PRINTF("%s:get jpeg frame encode context error\n", __func__);
		return -1;
	}

	img_tranc_jpeg_frm_obj = p_jpeg_frame_encode_ctx->frame_enc_create_obj();
	if(!img_tranc_jpeg_frm_obj) {
		PRINTF("%s:create h264 objection error\n", __func__);
		return -1;
	}
	frm_config.input_width = 1280;
	frm_config.input_height = 720;
	frm_config.output_width = 1280;
	frm_config.output_height = 720;
	ion_size = frm_config.input_width*frm_config.input_height + frm_config.input_width*frm_config.input_height/2;

	p_jpeg_frame_encode_ctx->frame_enc_config_obj(img_tranc_jpeg_frm_obj, &frm_config);

	ion_mem = ion_alloc(ion_size);
	if(!ion_mem) {
		PRINTF("%s:alloc ion error\n", __func__);
		return -1;
	}



		read(src_fd, ion_mem->virt_addr, ion_size);
		
		if(p_jpeg_frame_encode_ctx->frame_enc_input_a_frame_by_phy) {
			if(p_jpeg_frame_encode_ctx->frame_enc_input_a_frame_by_phy(img_tranc_jpeg_frm_obj, ion_mem->phy_addr))
				PRINTF("jpeg_encode_input_a_frame error\n");
		}
		
		if(p_jpeg_frame_encode_ctx->frame_enc_get_a_frame(img_tranc_jpeg_frm_obj, &frame_enc_outinfo)) {
			PRINTF("jpeg_encode_get_one_bitstream_frame error\n");
		//	goto out_frm_enc1;
		}

		write(out_fd, frame_enc_outinfo.p_data0, frame_enc_outinfo.size0);
		if(frame_enc_outinfo.size1) {
			write(out_fd, frame_enc_outinfo.p_data1, frame_enc_outinfo.size1);
		}

		if(p_jpeg_frame_encode_ctx->frame_enc_put_a_frame(img_tranc_jpeg_frm_obj, &frame_enc_outinfo)) {
			PRINTF("jpeg_encode_get_one_bitstream_frame error\n");
		//	goto out_frm_enc1;
		}


	ion_free(ion_mem);

	p_jpeg_frame_encode_ctx->frame_enc_destroy_obj(img_tranc_jpeg_frm_obj);
	frame_encode_put_context(p_jpeg_frame_encode_ctx);
	p_jpeg_frame_encode_ctx = NULL;

	frame_encode_uninit();

	return 0;
}

#endif

