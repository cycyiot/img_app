#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>
#include <cedarx/frame_encode.h>
#include <cedarx/frame_encode_h264.h>
#include <cedarx/frame_encode_jpeg.h>
#include <platform_cfg.h>

static struct frame_encode_context_st *g_pt_frame_encode_context_list_head;
static pthread_mutex_t frame_encode_context_list_lock = PTHREAD_MUTEX_INITIALIZER;


int frame_encode_context_register(struct frame_encode_context_st *p_frame_encode_context)
{
	struct frame_encode_context_st *pt_tmp;

	if(!p_frame_encode_context) {
		PRINTF("%s:invalid pointer\n", __func__);
		return -1;
	}

	pthread_mutex_lock(&frame_encode_context_list_lock);
	if (!g_pt_frame_encode_context_list_head)
	{
		g_pt_frame_encode_context_list_head  = p_frame_encode_context;
		p_frame_encode_context->next = NULL;
	}
	else
	{
		pt_tmp = g_pt_frame_encode_context_list_head;
		while (pt_tmp->next)
		{
			pt_tmp = pt_tmp->next;
		}
		pt_tmp->next	  = p_frame_encode_context;
		p_frame_encode_context->next = NULL;
	}
	pthread_mutex_unlock(&frame_encode_context_list_lock);

	return 0;

}


int frame_encode_context_unregister(struct frame_encode_context_st *p_frame_encode_context)
{
	struct frame_encode_context_st *pt_tmp, *pt_prev;

	pthread_mutex_lock(&frame_encode_context_list_lock);
	for(pt_tmp = g_pt_frame_encode_context_list_head, pt_prev = g_pt_frame_encode_context_list_head; pt_tmp; pt_tmp = pt_tmp->next) {
		if(p_frame_encode_context == pt_tmp) {
			if(g_pt_frame_encode_context_list_head == pt_tmp) {
				g_pt_frame_encode_context_list_head = NULL;
			} else {
				pt_prev->next = pt_tmp->next;
			}
			break;
		}
		pt_prev = pt_tmp;
	}
	pthread_mutex_unlock(&frame_encode_context_list_lock);

	return 0;

}

FRM_OBJ *frame_encode_create_obj(struct frame_encode_context_st *p_frame_encode_context)
{
	if(p_frame_encode_context && p_frame_encode_context->frame_enc_create_obj)
		return p_frame_encode_context->frame_enc_create_obj();
	return NULL;
}

int frame_encode_destroy_obj(struct frame_encode_context_st *p_frame_encode_context, FRM_OBJ *p_obj)
{
	if(p_frame_encode_context && p_frame_encode_context->frame_enc_destroy_obj)
		return p_frame_encode_context->frame_enc_destroy_obj(p_obj);

	return -1;
}

int frame_encode_get_head_info(struct frame_encode_context_st *p_frame_encode_context, FRM_OBJ *p_obj, struct frame_enc_head_st *p_head_info)
{
	if(p_frame_encode_context && p_frame_encode_context->frame_enc_get_head_info)
		return p_frame_encode_context->frame_enc_get_head_info(p_obj, p_head_info);

	return -1;
}

int frame_encode_config_obj(struct frame_encode_context_st *p_frame_encode_context, FRM_OBJ *p_obj, frame_enc_config_st *p_cfg)
{
	if(p_frame_encode_context && p_frame_encode_context->frame_enc_config_obj)
		return p_frame_encode_context->frame_enc_config_obj(p_obj, p_cfg);
	return -1;
}

int frame_encode_input_a_frame_by_phy(struct frame_encode_context_st *p_frame_encode_context, FRM_OBJ *p_obj, int phy_addr)
{
	if(p_frame_encode_context && p_frame_encode_context->frame_enc_input_a_frame_by_phy)
		return p_frame_encode_context->frame_enc_input_a_frame_by_phy(p_obj, phy_addr);

	return -1;
}

int frame_encode_input_a_frame_by_vrt(struct frame_encode_context_st *p_frame_encode_context, FRM_OBJ *p_obj, int vrt_addr)
{
	if(p_frame_encode_context && p_frame_encode_context->frame_enc_input_a_frame_by_vrt)
		return p_frame_encode_context->frame_enc_input_a_frame_by_vrt(p_obj, vrt_addr);

	return -1;
}
int frame_encode_get_thumbnai_a_frame(struct frame_encode_context_st *p_frame_encode_context, FRM_OBJ *p_obj, frame_enc_outinfo_st*p_out_info)
{
	if(p_frame_encode_context && p_frame_encode_context->frame_enc_get_thumbnail)
		return p_frame_encode_context->frame_enc_get_thumbnail(p_obj, p_out_info);

	return -1;
}	
int frame_encode_get_a_frame(struct frame_encode_context_st *p_frame_encode_context, FRM_OBJ *p_obj, frame_enc_outinfo_st*p_out_info)
{
	if(p_frame_encode_context && p_frame_encode_context->frame_enc_get_a_frame)
		return p_frame_encode_context->frame_enc_get_a_frame(p_obj, p_out_info);

	return -1;

}

int frame_encode_put_a_frame(struct frame_encode_context_st *p_frame_encode_context, FRM_OBJ *p_obj, frame_enc_outinfo_st *p_out_info)
{
	if(p_frame_encode_context && p_frame_encode_context->frame_enc_put_a_frame)
		return p_frame_encode_context->frame_enc_put_a_frame(p_obj, p_out_info);

	return -1;
}

int frame_encode_set_params_h264(struct frame_encode_context_st *p_frame_encode_context, FRM_OBJ *p_obj, h264_spec_param_st *param)
{
	if(p_frame_encode_context && p_frame_encode_context->frame_enc_set_params_h264)
		return p_frame_encode_context->frame_enc_set_params_h264(p_obj,param);

	return -1;
}
void frame_encode_show_context_list(void)
{
	int i = 0;
	struct frame_encode_context_st *pt_tmp = g_pt_frame_encode_context_list_head;

	while (pt_tmp)
	{
		PRINTF("%02d type:%d\n", i++, pt_tmp->type);
		pt_tmp = pt_tmp->next;
	}
}


struct frame_encode_context_st * frame_encode_get_context(FRM_ENCODE_TYPE type)
{
	struct frame_encode_context_st *pt_tmp = g_pt_frame_encode_context_list_head;
	
	while (pt_tmp)
	{
		if(pt_tmp->type == type)
		{
			return pt_tmp;
		}
		pt_tmp = pt_tmp->next;
	}
	return NULL;

}

int frame_encode_put_context(struct frame_encode_context_st *p_frame_encode_ctx)
{
	return 0;
}


int frame_encode_init(void)
{
	frame_encode_h264_init();
	frame_encode_jpeg_init();
	return 0;
}


int frame_encode_uninit(void)
{
	frame_encode_h264_uninit();
	frame_encode_jpeg_uninit();	
	return 0;
}


static volatile FRM_ENCODE_STATE frm_encode_status = FRM_ENCODE_STATE_IDLE;


int frm_encode_get_state(FRM_ENCODE_STATE *status)
{
	int ret = 0;
	FRM_ENCODE_STATE state = FRM_ENCODE_STATE_IDLE;

	if(status == NULL){
		PRINTF("%s:param error\n", __func__);
		ret = -1;
		goto errHdl;
	}

	state = frm_encode_status;
	*status = state;
	
errHdl:
	return ret;
}

int frm_encode_set_state(FRM_ENCODE_STATE status)
{
	int ret = 0;
	FRM_ENCODE_STATE state = FRM_ENCODE_STATE_IDLE;
	
	if(status >= FRM_ENCODE_STATE_MAX){
		PRINTF("%s:param error\n", __func__);
		ret = -1;
		goto errHdl;
	}

	frm_encode_get_state(&state);
	if(status != state){
		frm_encode_status = status;
	}else{
		PRINTF("%s:state isn't updated\n", __func__);
	}
	
errHdl:
	return ret;
}


