#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>
#include <cedarx/frame_encode.h>
#include <cedarx/h264_encode.h>
#include <platform_cfg.h>

typedef struct ven_obj_context_st {
	struct ven_obj *obj;
	VencOutputBuffer output_buf;
	struct frame_enc_head_st head_info;
}ven_obj_context_st;

static FRM_OBJ *h264_create_obj(void)
{
	struct ven_obj *obj = NULL;
	struct ven_obj_context_st *p_context = NULL;

	p_context = (ven_obj_context_st *)malloc(sizeof(struct ven_obj_context_st));
	if(!p_context) {
		PRINTF("%s: malloc error\n", __func__);
		return NULL;
	}
	memset(p_context, 0, sizeof(struct ven_obj_context_st));
	
	obj = h264_encode_create();
	p_context->obj = obj;
	
	return (FRM_OBJ *)p_context;
}
static int h264_destroy_obj(FRM_OBJ *obj)
{
	struct ven_obj_context_st *p_context = (struct ven_obj_context_st *)obj;
	struct frame_enc_head_st *p_head_info = NULL;
	int ret = 0;

	if(!obj) {
		PRINTF("%s:invalid FRM_OBJ pointer\n", __func__);
		return -1;
	}
	
	ret = h264_encode_destroy(p_context->obj);
	if(ret) {
		PRINTF("%s:h264_encode_destroy error\n", __func__);
	}
	p_head_info = &p_context->head_info;
	free(p_head_info->head_buf);
	free(p_context);

	return 0;
}

static int h264_config_obj(FRM_OBJ *obj, frame_enc_config_st *cfg)
{
	struct ven_cfg v_cfg;
	VencBaseConfig *p_basecfg = NULL;
	struct ven_obj_context_st *p_context = (struct ven_obj_context_st *)obj;
	struct frame_enc_head_st *p_head_info = NULL;
	VencHeaderData sps_pps_data;
	struct h264_spec_param_st *p_h264_spec_param = NULL;
	int ret = 0;

	if(!obj) {
		PRINTF("%s:invalid FRM_OBJ pointer\n", __func__);
		return -1;
	}
	if(!cfg) {
		PRINTF("%s:frame encode config error\n", __func__);
		return -1;
	}

	memset((void*)&v_cfg, 0, sizeof(struct ven_cfg));	
	p_basecfg = &v_cfg.baseConfig;
	
	p_basecfg->nInputWidth  = cfg->input_width;
	p_basecfg->nInputHeight = cfg->input_height;
	p_basecfg->nDstWidth    = cfg->output_width;
	p_basecfg->nDstHeight   = cfg->output_height;
	p_basecfg->nStride      = cfg->input_width;
	p_basecfg->eInputFormat = VENC_PIXEL_YUV420P;

	p_h264_spec_param = &cfg->spec_param.h264_spec;
	v_cfg.bit_rate         = p_h264_spec_param->bit_rate;
	v_cfg.frame_rate       = p_h264_spec_param->frame_rate;
	v_cfg.max_key_interval = p_h264_spec_param->max_key_interval;
	v_cfg.vbv_size         = p_h264_spec_param->vbv_size;//1*1024*1024;
	v_cfg.virtual_iframe   = p_h264_spec_param->virtual_iframe;

	ret = h264_encode_config(p_context->obj, &v_cfg);
	if(ret != 0) {
		PRINTF("%s:h264 encode config error\n", __func__);
		return ret;
	}
	ret = h264_encode_get_sps_pps(p_context->obj, &sps_pps_data);
	if(ret) {
		PRINTF("%s:get head info error\n", __func__);
		return ret;
	}

	p_head_info = &p_context->head_info;
	p_head_info->head_buf = (void *)malloc(sps_pps_data.nLength);
	if(!p_head_info->head_buf) {
		p_head_info->head_size = 0;
	} else {
		memcpy(p_head_info->head_buf, sps_pps_data.pBuffer, sps_pps_data.nLength);
		p_head_info->head_size = sps_pps_data.nLength;
	}

	return ret;
}

int h264_get_vbvsize(FRM_OBJ *obj, int *p_vbvsize)
{

	struct ven_obj_context_st *p_context = (struct ven_obj_context_st *)obj;

	if(!obj) {
		PRINTF("%s:invalid FRM_OBJ pointer\n", __func__);
		return -1;
	}
	if(!p_vbvsize) {
		PRINTF("%s:invalid vbvsize pointer\n", __func__);
		return -1;
	}

	return h264_encode_get_vbv_info(p_context->obj, p_vbvsize);

}


static int h264_input_a_frame_by_phy_yc(FRM_OBJ *obj, int phy_addr_y, int phy_addr_c)
{
	struct ven_obj_context_st *p_context = (struct ven_obj_context_st *)obj;

	if(!obj) {
		PRINTF("%s:invalid FRM_OBJ pointer\n", __func__);
		return -1;
	}

	return h264_encode_input_a_frame_by_phy_yc(p_context->obj, phy_addr_y, phy_addr_c);
}
static int h264_input_a_frame_by_phy(FRM_OBJ *obj, int phy_addr)
{
	struct ven_obj_context_st *p_context = (struct ven_obj_context_st *)obj;

	if(!obj) {
		PRINTF("%s:invalid FRM_OBJ pointer\n", __func__);
		return -1;
	}

	return h264_encode_input_a_frame_by_phy(p_context->obj, phy_addr);
}

static int h264_get_a_frame(FRM_OBJ *obj, frame_enc_outinfo_st *out_info)
{
	struct ven_obj_context_st *p_context = (struct ven_obj_context_st *)obj;
	VencOutputBuffer *p_output_buf;
	int ret = 0;

	if(!obj) {
		PRINTF("%s:invalid FRM_OBJ pointer\n", __func__);
		return -1;
	}
	if(!out_info) {
		PRINTF("%s:frame encode out info error\n", __func__);
		return -1;
	}

	p_output_buf = &p_context->output_buf;
	ret = h264_encode_get_one_bitstream_frame(p_context->obj, p_output_buf);
	
    out_info->size0   = p_output_buf->nSize0;
    out_info->size1   = p_output_buf->nSize1;
    out_info->p_data0 = p_output_buf->pData0;
    out_info->p_data1 = p_output_buf->pData1;
    out_info->p_priv  = NULL;

	return ret;
}
static int h264_put_a_frame(FRM_OBJ *obj, frame_enc_outinfo_st *out_info)
{
	struct ven_obj_context_st *p_context = (struct ven_obj_context_st *)obj;
	VencOutputBuffer *p_output_buf;
	int ret = 0;

	if(!obj) {
		PRINTF("%s:invalid FRM_OBJ pointer\n", __func__);
		return -1;
	}
	if(!out_info) {
		PRINTF("%s:frame encode out info error\n", __func__);
		return -1;
	}

	p_output_buf = &p_context->output_buf;
	ret = h264_encode_put_one_bitstream_frame(p_context->obj, p_output_buf);

	return ret;
}

static int h264_get_head_info(FRM_OBJ *obj, struct frame_enc_head_st *p_head_info)
{
	struct ven_obj_context_st *p_context = (struct ven_obj_context_st *)obj;

	if(!obj) {
		PRINTF("%s:invalid FRM_OBJ pointer\n", __func__);
		return -1;
	}

	if(!p_head_info) {
		PRINTF("%s:head info pointer error\n", __func__);
		return -1;
	}

	if(p_context->head_info.head_size > 0) {
		p_head_info->head_size = p_context->head_info.head_size;
		p_head_info->head_buf = p_context->head_info.head_buf;
		return 0;
	} else {
		return -1;
	}
}


int h264_set_bit_rate(FRM_OBJ *obj, int bit_rate)
{
	struct ven_obj_context_st *p_context = (struct ven_obj_context_st *)obj;

	if(!obj) {
		PRINTF("%s:invalid FRM_OBJ pointer\n", __func__);
		return -1;
	}

	return h264_encode_set_bitrate(p_context->obj, bit_rate);

}

int h264_set_frame_rate(FRM_OBJ *obj, int frame_rate)
{
	struct ven_obj_context_st *p_context = (struct ven_obj_context_st *)obj;

	if(!obj) {
		PRINTF("%s:invalid FRM_OBJ pointer\n", __func__);
		return -1;
	}

	return h264_encode_set_frame_rate(p_context->obj, frame_rate);
}
int h264_set_params(FRM_OBJ *obj, h264_spec_param_st *params)
{
	struct ven_obj_context_st *p_context = (struct ven_obj_context_st *)obj;

	if(!obj) {
		PRINTF("%s:invalid FRM_OBJ pointer\n", __func__);
		return -1;
	}

	if(!params) {
		PRINTF("%s:params pointer error\n", __func__);
		return -1;
	}
	if(params->index & BIT_RATE_INDEX) {
		h264_encode_set_bitrate(p_context->obj, params->bit_rate);
	}
	if(params->index & FRM_RATE_INDEX) {
		h264_encode_set_frame_rate(p_context->obj, params->frame_rate);
	}
	if(params->index & MAX_KINT_INDEX) {
		h264_encode_set_max_frame_interval(p_context->obj, params->max_key_interval);
	}
	if(params->index & IQP_OSET_INDEX) {
		h264_encode_set_IQpOffset(p_context->obj, params->iqp_offset);
	}
	if(params->index & ENI_FMON_INDEX) {
		h264_encode_set_encode_Iframe(p_context->obj, params->encode_iframe_on);
	}
	
	return 0;
}

static struct frame_encode_context_st h264_context = {
	.type = FRM_ENCODE_H264,
	.next = NULL,
	.frame_enc_create_obj  = h264_create_obj,
	.frame_enc_destroy_obj = h264_destroy_obj,
	.frame_enc_get_head_info = h264_get_head_info,
	.frame_enc_config_obj = h264_config_obj,
	.frame_enc_get_vbvsize = h264_get_vbvsize,
	.frame_enc_input_a_frame_by_phy_yc = h264_input_a_frame_by_phy_yc,
	.frame_enc_input_a_frame_by_phy = h264_input_a_frame_by_phy,
	.frame_enc_input_a_frame_by_vrt = NULL,
	.frame_enc_get_a_frame = h264_get_a_frame,
	.frame_enc_put_a_frame = h264_put_a_frame,
	.frame_enc_set_params_h264 = h264_set_params,
};

int frame_encode_h264_init(void)
{
	return frame_encode_context_register(&h264_context);
}

int frame_encode_h264_uninit(void)
{
	return frame_encode_context_unregister(&h264_context);
}

