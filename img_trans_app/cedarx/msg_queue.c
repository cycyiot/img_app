#include <msg_queue.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/***************************************************************************************
*
* Functioin Define
****************************************************************************************/


MSG_QUEUE_T *msg_queue_create(INT32U msgNum, MSG_TYPE_T msgType)
{
	MSG_QUEUE_T *pMsgQ = NULL;
	
	pMsgQ = (MSG_QUEUE_T *)malloc(sizeof(MSG_QUEUE_T));
	if(pMsgQ == NULL){
		PRINTF("%s:create msg queue failed!!!\r\n", __func__);
		return NULL;
	}
	
	pMsgQ->r_idx = 0;
	pMsgQ->w_idx = 0;

	pMsgQ->msg_addr = (INT32U *)malloc((msgNum + 1)* 4);
	pMsgQ->msg_num = msgNum + 1;
	pMsgQ->msg_type = msgType;
	
	pthread_mutex_init(&pMsgQ->lock, NULL);

	sem_init(&pMsgQ->wait, 0, 0);
	
	return pMsgQ;
}

INT32S msg_queue_destory(MSG_QUEUE_T *pMsgQ)
{
	if(pMsgQ == NULL){
		PRINTF("%s:param error\r\n", __func__);
		return -1;
	}

	sem_destroy(&pMsgQ->wait);
	
	pthread_mutex_destroy(&pMsgQ->lock);

	free(pMsgQ->msg_addr);
	pMsgQ->msg_addr = NULL;
	
	free(pMsgQ);
	pMsgQ = NULL;

	return 0;
}

INT32S msg_queue_type_set(MSG_QUEUE_T *pMsgQ, MSG_TYPE_T msgType)
{
	if(pMsgQ == NULL){
		PRINTF("%s:param error\r\n", __func__);
		return -1;
	}

	pthread_mutex_lock(&pMsgQ->lock);
	pMsgQ->msg_type = msgType;
	pthread_mutex_unlock(&pMsgQ->lock);
	
	return 0;
}

INT32S msg_queue_is_empty(MSG_QUEUE_T *pMsgQ)
{
	if(pMsgQ == NULL){
		PRINTF("%s:param error\r\n", __func__);
		return -1;
	}
	
	return (pMsgQ->w_idx == pMsgQ->r_idx) ? 1 : 0;
}

INT32S msg_queue_is_full(MSG_QUEUE_T *pMsgQ)
{
	if(pMsgQ == NULL){
		PRINTF("%s:param error\r\n", __func__);
		return -1;
	}
	
	return (((pMsgQ->w_idx + 1) % pMsgQ->msg_num) == pMsgQ->r_idx) ? 1 : 0;
}

INT32S msg_queue_get_used_count(MSG_QUEUE_T *pMsgQ)
{
	INT32S cnt = 0;

	if(pMsgQ == NULL){
		PRINTF("%s:param error\r\n", __func__);
		return -1;
	}

	cnt = pMsgQ->w_idx - pMsgQ->r_idx;
	return (cnt < 0) ? (cnt + pMsgQ->msg_num): cnt;
}

INT32S msg_queue_put(MSG_QUEUE_T *pMsgQ, INT32U msgAddr)
{
	if(pMsgQ == NULL){
		PRINTF("%s:param error\r\n", __func__);
		return -1;
	}

	pthread_mutex_lock(&pMsgQ->lock);
	if(msg_queue_is_full(pMsgQ)){
		PRINTF("%s:msg queue is full!!!\r\n", __func__);
		pthread_mutex_unlock(&pMsgQ->lock);
		return -1;	
	}

	pMsgQ->msg_addr[pMsgQ->w_idx] = msgAddr;
	pMsgQ->w_idx = (pMsgQ->w_idx + 1) % pMsgQ->msg_num;

	pthread_mutex_unlock(&pMsgQ->lock);
	
	sem_post(&pMsgQ->wait);
	
	return 0;		
}

INT32U msg_queue_get(MSG_QUEUE_T *pMsgQ)
{
	INT32U msgAddr;

	if(pMsgQ == NULL){
		PRINTF("%s:param error\r\n", __func__);
		return 0;
	}

	if(pMsgQ->msg_type == MSG_TYPE_BLOCK){
		sem_wait(&pMsgQ->wait);
	}else{
		if(sem_trywait(&pMsgQ->wait) != 0){
			//PRINTF("%s:current sem is zero!!!\r\n", __func__);
			return 0;
		}
	}
	
	pthread_mutex_lock(&pMsgQ->lock);
	if(msg_queue_is_empty(pMsgQ)){
		//PRINTF("%s:msg queue is empty!!!\r\n", __func__);
		pthread_mutex_unlock(&pMsgQ->lock);
		return 0;	
	}
	
	msgAddr = pMsgQ->msg_addr[pMsgQ->r_idx];
	pMsgQ->r_idx = (pMsgQ->r_idx + 1) % pMsgQ->msg_num;
	pthread_mutex_unlock(&pMsgQ->lock);
	
	return msgAddr;
}

INT32S msg_queue_flush(MSG_QUEUE_T * pMsgQ)
{
	INT32U i = 0;
	
	if(pMsgQ == NULL){
		PRINTF("%s:param error\r\n", __func__);
		return 0;
	}

	pthread_mutex_lock(&pMsgQ->lock);
	for(i  = 0; i < pMsgQ->msg_num; i++){
		pMsgQ->msg_addr[i] = 0;
	}
	
	pMsgQ->r_idx = 0;
	pMsgQ->w_idx = 0;

	sem_init(&pMsgQ->wait, 0, 0);
	pthread_mutex_unlock(&pMsgQ->lock);

	return 0;
}



