#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <linux/input.h>
#include <key/key.h>


struct key_dev key;

static unsigned short key_code_get(struct input_event *input_event)
{
	unsigned short code = 0xffff;
	
	switch(input_event->code){
	case KEY_ENTER:
		 code = KEY_ENTER;
		 break;
	case KEY_UP:
		 code = KEY_UP;
		 break;
	case KEY_DOWN:
		 code = KEY_DOWN;
		 break;
	case KEY_CANCEL:
		 code = KEY_CANCEL;
		 break;
	case KEY_HOME:
		 code = KEY_HOME;
		 break;
	default:
		 break;
	}

	return code;
}

static KEY_TYPE key_type_get(struct input_event *input_event)
{
	KEY_TYPE type = KEY_TYPE_NONE;
	
	switch(input_event->value){
	case KEY_TYPE_NONE:
		 type = KEY_TYPE_NONE;
		 break;
	case KEY_TYPE_SHORT:
		 type = KEY_TYPE_SHORT;
		 break;
	case KEY_TYPE_LONG:
		 type = KEY_TYPE_LONG;
		 break;
	default:
		 break;
	}

	return type;
}

static void *key_scan(void *pvoid)
{
	int ret = 0;
	struct input_event input_event;
	fd_set readset;
	
	while(1) {
		/* zero fd set */
		FD_ZERO(&readset);
		FD_SET(key.fd_key, &readset);
		
		/* select fd set */
		ret = select(key.fd_key + 1, &readset, NULL, NULL, NULL);
		if(ret == -1) {
			fprintf(stderr, "select error!");
			continue;
		}

		/* get event */
		pthread_mutex_lock(&key.mutex);
		if(FD_ISSET(key.fd_key, &readset)) {
			ret = read(key.fd_key, &input_event, sizeof(struct input_event));
			if(ret < sizeof(struct input_event)) {
				fprintf(stderr, "get input event error!\n");
				pthread_mutex_unlock(&key.mutex);
				continue;
			}

			if(input_event.type == EV_KEY){
				printf("[%s,%d]:type %d,code %d, value %d\n", __func__, __LINE__, input_event.type, input_event.code, input_event.value);
				if(key_code_get(&input_event) == KEY_ENTER){
					switch(key_type_get(&input_event)){
					case KEY_TYPE_SHORT:
						break;
					case KEY_TYPE_LONG:
						break;
					default:
						break;
					}
				}
			}
		}
		pthread_mutex_unlock(&key.mutex);
	}

	return NULL;
}

void key_init(void)
{
	int err = 0;

	printf("%s %d", __func__, __LINE__);
	key.fd_key = open(KEY_PATH, O_RDONLY);
	pthread_mutex_init(&key.mutex, NULL);
	
	/* create pthread */
	err = pthread_create(&key.thread_id_key, NULL, key_scan, NULL);
	if(err != 0) {
		printf("create key scan pthread error!\r\n");
		return;
	}
}

void key_uninit(void)
{
	printf("%s %d", __func__, __LINE__);
	close(key.fd_key);
	pthread_mutex_destroy(&key.mutex);
	pthread_cancel(key.thread_id_key);
}
