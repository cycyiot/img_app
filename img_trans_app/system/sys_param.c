#include <stddef.h>
#include <system/sys_param.h>
#include <platform_cfg.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <battery.h>
#include <camera/camera.h>
#include <storage_mgr/ap_tf.h>
#include <storage_mgr/storage_mgr.h>
#include <net/wifi.h>
#include <system/event.h>

pthread_t state_detect_id;

void mtd_sync(void)
{
	sync();
	system("blockdev --flushbufs /dev/mtdblock4");
}

int system_date_time_set(struct tm *dt)
{
	struct tm *dtime = dt;
	struct timeval sdt;
	time_t timep;

	if(NULL == dtime){
		PRINTF("%s():%d-dt pointer is null\n",__func__,__LINE__);
		return -1;
	}
	timep = mktime(dtime);
	if(timep < 0){
		PRINTF("%s():%d-do mktime error\n",__func__,__LINE__);
		PRINTF("(Y-M-D_H-M-S)=(%d-%d-%d_%d-%d-%d)\n",
				dtime->tm_year,dtime->tm_mon,dtime->tm_mday,
				dtime->tm_hour,dtime->tm_min,dtime->tm_sec);
		return -1;
	}
	sdt.tv_sec  = timep;
	sdt.tv_usec = 0;
	if(settimeofday(&sdt,NULL) < 0){
		PRINTF("%s():%d-do settimeofday error\n",__func__,__LINE__);
		return -1;
	}
	return 0;
}


int usb_storage_adcard_on()
{
	if(!card_is_mounted())
		return -1;
	system("umount /mnt/");
	system("echo 0 > /sys/class/android_usb/android0/enable");
	system("echo 18d1 > /sys/class/android_usb/android0/idVendor");
	system("echo 0001 > /sys/class/android_usb/android0/idProduct");
	system("echo mass_storage > /sys/class/android_usb/android0/functions");
	//system("echo /dev/mtdblock6 > /sys/class/android_usb/android0/f_mass_storage/lun/file");
#if 1
	if(access(CARD_DEV_NODE2,F_OK)==0){
    	system("echo /dev/mmcblk0p1 > /sys/class/android_usb/android0/f_mass_storage/lun/file");
	}else if(access(CARD_DEV_NODE1,F_OK)==0){
        system("echo /dev/mmcblk0 > /sys/class/android_usb/android0/f_mass_storage/lun/file");
	}
#endif
	system("echo 1 > /sys/class/android_usb/android0/enable");
	return 0;

}
int usb_storage_adcard_off()
{
	system("echo 0 > /sys/class/android_usb/android0/enable");
	system("echo 18d1 > /sys/class/android_usb/android0/idVendor");
	system("echo d002 > /sys/class/android_usb/android0/idProduct");
    system("echo adb > /sys/class/android_usb/android0/functions");
	system("echo 1 > /sys/class/android_usb/android0/enable");
	if(access(CARD_DEV_NODE2,F_OK)==0){
		system("mount /dev/mmcblk0p1 /mnt/ ");
	}else if(access(CARD_DEV_NODE1,F_OK)==0){
		system("mount /dev/mmcblk0 /mnt/");
	}
	return 0;
}

static void *sys_info_notify(void *param)
{
	struct event_data event;
#ifdef BATTERY_DETECT
	struct battery_info battery;
	unsigned int voltage_last = 0;
	enum battery_charge_state battery_state;
	int cur_state, last_state = 0;
#endif
//	int cur_state, last_state = 0;

	card_s cardInfo;
	unsigned long long rsize = 0;
#if 0
    cur_state=1;
	if(cur_state!=last_state){
    	last_state=cur_state;
		if(cur_state==1){
        	usb_storage_adcard_on();
		}else{
         	usb_storage_adcard_off();
		}
	}

#endif	
	card_fs_capacity_get(&cardInfo);
	rsize = cardInfo.rsize;
	while(1)
	{
#ifdef BATTERY_DETECT
		if(battery_is_charging())
		{
			battery_info_get(&battery);
			if((battery.voltage > 99) && (battery.voltage > voltage_last))
			{
				battery_state_notify(&battery);
			}
			voltage_last = battery.voltage;
		}
		cur_state = (battery_state == NO_INSERT ? 0 : 1);
		if(cur_state != last_state)
		{
			last_state = cur_state;
			if(cur_state == 1){
#ifdef SYSTEM_LOW_POWER
				event.id = SYS_LOW_POWER;
				event_put(&event);
#endif
			}else{
#ifdef SYSTEM_LOW_POWER
				event.id = SYS_RESUME;
				event_put(&event);
#endif
			}
		}
#endif		
		
		if(!card_fs_capacity_get(&cardInfo))
		{
			if((cardInfo.rsize < TF_FREE_SPACE_MIN) && (cardInfo.rsize < rsize))
			{
				event.id = CARD_DISABLE;
				event.client = 0;
				event_put(&event);
				card_state_notify(&cardInfo);
				rsize = cardInfo.rsize;
			}
			else if((((rsize - cardInfo.rsize) * 100 / cardInfo.tsize)) > 0)
			{
				card_state_notify(&cardInfo);
				rsize = cardInfo.rsize;
			}
		}

		wifi_station_notify();
		
		sleep(2);
	}
	return NULL;
}

int sys_info_detect_init(void)
{
	int ret = 0;

#ifdef BATTERY_DETECT	
	ret = battery_init();
#endif
	ret |= pthread_create(&state_detect_id, NULL, sys_info_notify, NULL);
	ret |= sys_cmd_init();

	return ret;
}
