#include <stddef.h>
#include <system/sys_param.h>
#include <system/update.h>
#include <platform_cfg.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <battery.h>
#include <camera/camera.h>
#include <storage_mgr/ap_tf.h>
#include <storage_mgr/storage_mgr.h>
#include <net/cmd_handle.h>
#include <led/led.h>
#include <net/wifi.h>

int sys_date_time_cmd_handler(struct cmd_param *cmdParam)
{
	struct json_object *jsonSubParamObj;
	struct tm dateTime;
	int ret = -1;
	
	if(is_error(cmdParam->param))
		goto err_exit;
	
	jsonSubParamObj = json_object_object_get(cmdParam->param, "YEAR");
	if(json_object_get_type(jsonSubParamObj) != json_type_int)
		goto err_exit;
	dateTime.tm_year = json_object_get_int(jsonSubParamObj) - 1900;
	
	jsonSubParamObj = json_object_object_get(cmdParam->param, "MONTH");
	if(json_object_get_type(jsonSubParamObj) != json_type_int)
		goto err_exit;
	dateTime.tm_mon = json_object_get_int(jsonSubParamObj) - 1;

	jsonSubParamObj = json_object_object_get(cmdParam->param, "DAY");
	if(json_object_get_type(jsonSubParamObj) != json_type_int)
		goto err_exit;
	dateTime.tm_mday = json_object_get_int(jsonSubParamObj);
	
	jsonSubParamObj = json_object_object_get(cmdParam->param, "HOUR");
	if(json_object_get_type(jsonSubParamObj) != json_type_int)
		goto err_exit;
	dateTime.tm_hour = json_object_get_int(jsonSubParamObj);

	jsonSubParamObj = json_object_object_get(cmdParam->param, "MINUTE");
	if(json_object_get_type(jsonSubParamObj) != json_type_int)
		goto err_exit;
	dateTime.tm_min = json_object_get_int(jsonSubParamObj);

	jsonSubParamObj = json_object_object_get(cmdParam->param, "SECOND");
	if(json_object_get_type(jsonSubParamObj) != json_type_int)
		goto err_exit;
	dateTime.tm_sec = json_object_get_int(jsonSubParamObj);
	
	ret = system_date_time_set(&dateTime);

err_exit:
	cmd_response(ret, cmdParam);
	return ret;
}

int sys_param_get_cmd_handler(struct cmd_param *cmdParam)
{
	struct json_object *jsonSubParamObj = NULL;
	struct json_object *jsonTmpObj = NULL;
	struct json_object *jsonFiledCardCapParamObj = NULL;
	struct json_object *jsonWifiObj = NULL;
	char str_buf[33];
	card_s cardInfo;
	struct led_dev *leds;
	struct camera_data *p_camera_dev;
	int len;

	jsonSubParamObj = json_object_new_object();
	jsonTmpObj = json_object_new_object();
	json_object_object_add(jsonSubParamObj, "CMD", json_object_new_int((int)CMD_SYS_PARAM_GET));

#ifdef GESTURE_RECOGNITION
	int n;
	struct json_object *jsonEcpObj = NULL;
	if(!is_error(cmdParam->param)){	
		jsonEcpObj = json_object_object_get(cmdParam->param, "K");
		if(json_object_get_type(jsonEcpObj) == json_type_int){
			n = json_object_get_int(jsonEcpObj);
			json_object_object_add(jsonTmpObj, "K", json_object_new_int(ecp(n)));
			json_object_new_int(ecp(n));
		}
	}
#endif	
#ifdef BATTERY_DETECT
	json_object_object_add(jsonTmpObj, "M_BATTERY", json_object_new_int(battery_is_charging()));
#endif
	leds = led_get_handler();
	if(leds != NULL)
		json_object_object_add(jsonTmpObj, "M_LED_MODE", json_object_new_int(leds->mode));

	p_camera_dev = camera_get_video_device(0);
	if(p_camera_dev != NULL){
		json_object_object_add(jsonTmpObj, "M_AWB", json_object_new_int(p_camera_dev->sys_param.awb));
		json_object_object_add(jsonTmpObj, "M_AE", json_object_new_int(p_camera_dev->sys_param.ae));
		json_object_object_add(jsonTmpObj, "M_CTS", json_object_new_int(p_camera_dev->sys_param.contrast));
		json_object_object_add(jsonTmpObj, "M_BHT", json_object_new_int(p_camera_dev->sys_param.brightness));
	}
	
	jsonWifiObj=json_object_new_object();
	memset(str_buf, 0, sizeof(str_buf));
	if (!wifiParmRead("ssid",str_buf)){
	  json_object_object_add(jsonWifiObj, "ssid", json_object_new_string(str_buf));
	}
	memset(str_buf, 0, sizeof(str_buf));
	if (!wifiParmRead("wpa_passphrase",str_buf)){
	  json_object_object_add(jsonWifiObj, "pass_phrase", json_object_new_string(str_buf));
	}
	json_object_object_add(jsonTmpObj, "Wifi_Param", jsonWifiObj);
	
	jsonFiledCardCapParamObj = json_object_new_object();
	if(card_status_get(&cardInfo) != 0){
		cardInfo.status = UNMOUNT;
	}
	json_object_object_add(jsonFiledCardCapParamObj, "online", json_object_new_int(1/*cardInfo.status*/));
	if(/*cardInfo.status == MOUNTED && !card_fs_capacity_get(&cardInfo)*/1){
		json_object_object_add(jsonFiledCardCapParamObj, "freeSpace", json_object_new_int(/*cardInfo.rsize/(1024*1024)*/6000));
		json_object_object_add(jsonFiledCardCapParamObj, "usedSpace", json_object_new_int(/*(cardInfo.tsize-cardInfo.rsize)/(1024*1024)*/2000));
		json_object_object_add(jsonFiledCardCapParamObj, "totalSpace", json_object_new_int(/*cardInfo.tsize/(1024*1024)*/8000));
	}
	json_object_object_add(jsonTmpObj, "M_CARD", jsonFiledCardCapParamObj);

	json_object_object_add(jsonTmpObj, "FirmWare", json_object_new_string(VERSION));

	json_object_object_add(jsonSubParamObj, "PARAM", jsonTmpObj);
	json_object_object_add(jsonSubParamObj, "RESULT", json_object_new_int(0));

	len = strlen(json_object_to_json_string(jsonSubParamObj))+1;
	TcpClientSend(cmdParam->client, json_object_to_json_string(jsonSubParamObj), len);
	PRINTF("%s: %s\n", __func__, json_object_to_json_string(jsonSubParamObj));

	json_object_put(jsonSubParamObj);
	return 0;
}

int ota_cmd_handler(struct cmd_param *cmdParam)
{
	struct firmware fw;
	struct json_object *jsonSubParamObj;
	//int upload = 0, 
	int	ret = -1;

	if(is_error(cmdParam->param))
			goto err_exit;
#if 0
	jsonSubParamObj = json_object_object_get(cmdParam->param, "upload");
	if(json_object_get_type(jsonSubParamObj) != json_type_int)
		goto err_exit;
	upload = json_object_get_int(jsonSubParamObj);
	if(upload == 0){
		system("killall -q pilot flow");					//kill the pilot process for the ram space to update system  
#if 0		
		ret = system("ftpd -f /etc/ftpd.conf");		//start ftpd service for client to upload package
		if((ret == -1) || (!WIFEXITED(ret)) || WEXITSTATUS(ret)){
			printf("%s failed!\n", __func__);
		}
#endif
	}else{
#endif
		jsonSubParamObj = json_object_object_get(cmdParam->param, "ver");
		if(json_object_get_type(jsonSubParamObj) != json_type_string)
			goto err_exit;
		fw.version = (char *)json_object_get_string(jsonSubParamObj);
		if(fw.version == NULL)
			goto err_exit;
		jsonSubParamObj = json_object_object_get(cmdParam->param, "md5");
		if(json_object_get_type(jsonSubParamObj) != json_type_string)
			goto err_exit;
		fw.md5 = (char *)json_object_get_string(jsonSubParamObj);
		if(fw.md5 == NULL)
			goto err_exit;

		led_blink(0xff);
		ret = firmware_update(&fw);
		led_blink(0x00);
		
		if(!ret){
			led_switch(0);
			cmd_response(ret, cmdParam);
			//system("reboot");
			return ret;
		}else{

        system("rm /tmp/*.zip");
		}
//	}
err_exit:
	cmd_response(ret, cmdParam);
	return ret;
}

struct cmd sys_cmd[] = {
	{CMD_DATE_TIME_SET, sys_date_time_cmd_handler},
	{CMD_SYS_PARAM_GET, sys_param_get_cmd_handler},
	{CMD_FIRMWARE_UPDATE_S, ota_cmd_handler},
};

int sys_cmd_init(void)
{
	return cmd_register(sys_cmd, ARRAY_SIZE(sys_cmd));
}
