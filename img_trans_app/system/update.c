#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <netdb.h>
#include <time.h>
#include <sys/stat.h> 
#include <string.h>
#include <unistd.h>
#include <platform_cfg.h>
#include <system/sys_param.h>
#include <system/update.h>
#include "bbc_sdk.h"

#define OTA_PATH    "/mnt/ota_update_allwinnertech"
#define BIN_PATH    "/mnt"

#define KER_IMG     "uImage"
#define ROOTFS_IMG  "rootfs.squashfs"
#define CONF_PATH  "/netPrivate/config.dat"

#define KERNEL_PATH "/dev/mtd1"
#define ROOTFS_PATH "/dev/mtd2"

#define KERNEL_BACKUP_PATH "/dev/mtd6"
#define ROOTFS_BACKUP_PATH "/dev/mtd7"

#define BUF_SIZE	(1024*512)
#define RETRY_CNT   3

extern unsigned char *SHA1(const unsigned char *d, size_t n, unsigned char *md);


//设备信息，根据设备具体信息赋值
Device device = {
		.deviceId = "20160715",
		.name = "c600",
		.mac = "cda03200ab",
		.vendor = "allwinnerotatest1",
		.firmwareVersion = "0.0.10",
		.romType = "DEV",
		.sdkVersion = SDK_VERSION,
		.deviceClass = {
			.name = "c600"
		}
};

//设备升级失败的上报信息
OtaFailedInfo failedInfo = {
		.curVersion = "0.0.1",
		.errorCode = 101
};

//需要存储的一些设置项，目前有"register"(是否注册），"timestamp" (上次接收命令的时间戳），"version"(旧版本信息）
//cJSON* config = NULL;

void get_firm_version(char *dst)
{
	FILE *fp;
	int i;
	char buf[32];
	
	if((fp=fopen(CONF_PATH,"r"))!=NULL)
	{
		for(i=1;i<15;i++)
		{
		    memset(buf,0x0,sizeof(buf));
			fgets(buf,sizeof(buf),fp);
		}

        fgets(dst, 32, fp);

		for(i=0;i<32;i++)
		{
		    if('\n' == dst[i])
			{
				dst[i] = '\0';
				break;
			}
		}
		        
		fclose(fp);
	}

}

#if 0
static int set_firm_version(const char *src)
{
    char cmd[64];

	memset(cmd, 0, sizeof(cmd));
    sprintf(cmd ,"/netPrivate/save.sh 9 %s", src);    
    system(cmd);

	return 0;
}

static int do_firmware_burn(const char *src_path, const char *dst_path)
{
	int ret = -1;
	char cmd[64];
	void *fSrcBuf = NULL;
	FILE *fSrcP = NULL;
	FILE *fDstP = NULL;
	size_t len;

	fSrcP = fopen(src_path, "rb");
	if(fSrcP == NULL){
		PRINTF("open %s err!\n", src_path);
		ret = -1;
		goto errHdl5;
	}

	fDstP = fopen(dst_path, "wb+");
	if(fDstP == NULL){
		PRINTF("open %s err!\n", dst_path);
		ret = -1;
		goto errHdl4;
	}

	fSrcBuf = malloc(BUF_SIZE);
	if(fSrcBuf == NULL){
		PRINTF("[%s,%d]:malloc buf failed!\n", __func__, __LINE__);
		ret = -1;
		goto errHdl3;
	}
	
	memset(cmd, 0, sizeof(cmd));
	snprintf(cmd, sizeof(cmd), "flash_eraseall %s", dst_path);
	ret = system(cmd); 
	if((ret == -1) || (!WIFEXITED(ret)) || WEXITSTATUS(ret)){
		PRINTF("%s: flash_eraseall %s failed!\n", __func__, dst_path);
		ret = -1;
		goto errHdl2;
	}

	while(1){
		len = fread(fSrcBuf, 1, BUF_SIZE, fSrcP);
		if(len > 0){
			ret = fwrite(fSrcBuf, 1, len, fDstP);
			if(ret != len){
				PRINTF("[%s,%d]:write data failed!\r\n", __func__, __LINE__);
				ret = -1;
				goto errHdl2;
			}
		}else{
			break;
		}
	}
	PRINTF("fwrite = %d\n", ret);
errHdl2:
	free(fSrcBuf);
	fSrcBuf = NULL;
errHdl3:
	fclose(fDstP); 
errHdl4:
	fclose(fSrcP); 
errHdl5:
	return ret;
}


static int do_firmware_burn(const char *src_path, const char *dst_path)
{
	int ret;
	char cmd[128];
	
	memset(cmd, 0, sizeof(cmd));
	snprintf(cmd, sizeof(cmd), "flash_erase %s 0 0", dst_path);
	ret = system(cmd);
	if((ret == -1) || (!WIFEXITED(ret)) || WEXITSTATUS(ret)){
		PRINTF("%s: flash_erase %s failed!\n", __func__, dst_path);
		return -1;
	}

	memset(cmd, 0, sizeof(cmd));
	snprintf(cmd, sizeof(cmd), "dd if=%s of=%s", src_path, dst_path);
	ret = system(cmd);
	if((ret == -1) || (!WIFEXITED(ret)) || WEXITSTATUS(ret)){
		PRINTF("%s: burn %s failed!\n", __func__, dst_path);
		return -1;
	}
	
	return 0;
}
#endif

int firmware_update(struct firmware *fw)
{
    char cmd[64];
	int ret = -1;

	memset(cmd, 0, sizeof(cmd));
	snprintf(cmd, sizeof(cmd), "nice --19 /etc/ota.sh %s %s", fw->version, fw->md5);
	ret = system(cmd);
	if((ret == -1) || (!WIFEXITED(ret)) || WEXITSTATUS(ret)){
		PRINTF("%s failed!\n", __func__);
		return -1;
	}

	mtd_sync();
	
	return ret;    
}
