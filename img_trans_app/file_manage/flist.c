#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <file_manage/flist.h>
	
void flist_init(linklist *head)
{
	*head = NULL;
}
void flist_destroy(linklist *head)
{
	struct node *tail, *del;
	
	if (IS_ERROR(*head))
		return;
	tail = (*head)->prev;
	while (tail != *head) {
		del = tail;
		tail = tail->prev;
		free(del);
	}
	free(*head);
	*head = NULL;
}	
static int copy_to_node(struct node *pnode, struct file_s *file)
{
	assert_f(pnode);
	assert_f(file);
	memcpy((void*)&pnode->file,(void*)file, sizeof(struct file_s));

	return 0;
}

int flist_push(linklist *head, struct file_s *file)
{
	struct node *new,*tail;

	assert_f(file);
	new = (struct node*)malloc(sizeof(struct node));
	if (IS_ERROR(new)) {
		PRINT_ERR("malloc memory fail \n");
		return -1;
	}
	copy_to_node(new,file);
	new->prev = NULL;
	new->next = NULL;
	if (NULL == *head) {	/* first element */
		*head = new;
		(*head)->prev = (*head)->next = *head;
	} else {
		tail = (*head)->prev;
	 	if (new->file.ctime > (*head)->file.ctime) {
			new->prev = (*head)->prev;			
			(*head)->prev->next = new;
			new->next = *head;
			(*head)->prev = new;
			*head = new;
		} else {
			while (new->file.ctime > tail->file.ctime) {
				tail = tail->prev;
			}			
			new->prev = tail;
			new->next = tail->next;
			tail->next->prev = new;
			tail->next = new;
		}
	}

	return 0;
}
int flist_pop(linklist *head, struct file_s *file)
{
	struct node *del, *tail = (*head)->prev;
	
	assert_f(*head);
	assert_f(file);
	if (!strcmp((const char*)file->name, (const char*)(*head)->file.name)) {
		if (((*head)->prev == (*head)->next) && ((*head)->next == *head)) { /* only one element */
			free(*head);
			*head = NULL;
			return 0;
		}
		del = *head;
		(*head)->next->prev = (*head)->prev;
		(*head)->prev->next = (*head)->next;
		*head = (*head)->next;
		free(del);	
	} else {
		if (((*head) == tail) && !strcmp((const char*)file->name, (const char*)tail->file.name)) {
			free(tail);
			*head = NULL;
			return 0;
		}
		while (((*head) != tail) && strcmp((const char*)file->name, (const char*)tail->file.name)) {
			tail = tail->prev;	
		}		
		if (*head == tail) {
			PRINT_DBG("Not find node (%s%s) \n", file->dir, file->name);
			return -1;
		}	
		tail->prev->next = tail->next;
		tail->next->prev = tail->prev;
		free(tail);
	} 

	return 0;
}
unsigned int flist_count(linklist *head)
{
	unsigned int count = 0;
	struct node *tail;
	
	if (NULL == *head)	
		return count;
	tail = (*head)->prev;
	while (tail != *head) {
		count++;
		tail = tail->prev;	
	}
	count++;	/* head count */
	
	return count;
}
struct node *flist_find_node(linklist *head, struct file_s *file)
{
	struct node *tail;

	if (IS_ERROR(*head) || IS_ERROR(file))
		return NULL;
	tail = (*head)->prev;
	while ((*head) != tail && strcmp((const char*)file->name, (const char*)tail->file.name)) {
		tail = tail->prev;
	}
	if ((*head == tail) && strcmp((const char*)file->name, (const char*)tail->file.name)) {
		return NULL;
	}
	return tail;
}
struct node *flist_next(linklist *head)
{
	if (IS_ERROR(*head))
		return NULL;
	return (*head)->next;
}
struct node *flist_prev(linklist *head)
{
	if (IS_ERROR(*head))
		return NULL;
	return (*head)->prev;
}	
void flist_traverse(linklist *head, void (*func)(struct file_s *file))
{
	struct node *tail;
	
	if (IS_ERROR(*head) || IS_ERROR(func))
		return;
	tail = (*head)->prev;
	while (*head != tail) {
		(*func)(&tail->file);
		tail = tail->prev;
	}	
	(*func)(&tail->file);
}
void flist_print_node(struct file_s *file)
{
	printf("node:(name,dir,status,lock,bsize,ctime) = (%s,%s,%d,%d,%d,%llu) \n",
		file->name,file->dir,file->status,file->lock,file->bsize,file->ctime);
}
//#define	__LIST_TEST
#ifdef	__LIST_TEST
int main(int argc, char *argv[])
{
	linklist head; 
	struct node *pnode;
	struct file_s file = {.name = "9.jpg"};
	struct file_s file_tb[] = {
	{"0.jpg","/mnt/photo/",0,0,0,0},
	{"1.jpg","/mnt/photo/",0,0,0,1},
	{"2.jpg","/mnt/photo/",0,0,0,2},
	{"3.jpg","/mnt/photo/",0,0,0,3},
	{"4.jpg","/mnt/photo/",0,0,0,4},
	{"5.jpg","/mnt/photo/",0,0,0,5},
	{"6.jpg","/mnt/photo/",0,0,0,6},
	{"7.jpg","/mnt/photo/",0,0,0,7},
	{"8.jpg","/mnt/photo/",0,0,0,8},
	{"9.jpg","/mnt/photo/",0,0,0,9},
	{"10.jpg","/mnt/photo/",0,0,0,10},
	{"11.jpg","/mnt/photo/",0,0,0,11},
	{"12.jpg","/mnt/photo/",0,0,0,12},
	{"13.jpg","/mnt/photo/",0,0,0,13},
	{"14.jpg","/mnt/photo/",0,0,0,14},
	{"15.jpg","/mnt/photo/",0,0,0,15},
	{"16.jpg","/mnt/photo/",0,0,0,16},
	{"17.jpg","/mnt/photo/",0,0,0,17},
	{"18.jpg","/mnt/photo/",0,0,0,18},
	{"19.jpg","/mnt/photo/",0,0,0,19}
	};

	printf("file list test start ... \n");
	flist_init(&head);
	printf(">>>>>>>>>>>push test<<<<<<<<<<<\n");	
	flist_push(&head,&file_tb[3]);printf("push => ");flist_print_node(&file_tb[3]);	
	flist_push(&head,&file_tb[8]);printf("push => ");flist_print_node(&file_tb[8]);	
	flist_push(&head,&file_tb[1]);printf("push => ");flist_print_node(&file_tb[1]);	
	flist_push(&head,&file_tb[2]);printf("push => ");flist_print_node(&file_tb[2]);	
	flist_push(&head,&file_tb[4]);printf("push => ");flist_print_node(&file_tb[4]);	
	flist_push(&head,&file_tb[9]);printf("push => ");flist_print_node(&file_tb[9]);	
	flist_push(&head,&file_tb[5]);printf("push => ");flist_print_node(&file_tb[5]);	
	flist_push(&head,&file_tb[7]);printf("push => ");flist_print_node(&file_tb[7]);	
	flist_push(&head,&file_tb[6]);printf("push => ");flist_print_node(&file_tb[6]);	
	flist_push(&head,&file_tb[18]);printf("push => ");flist_print_node(&file_tb[18]);	
	flist_push(&head,&file_tb[10]);printf("push => ");flist_print_node(&file_tb[10]);	
	flist_push(&head,&file_tb[12]);printf("push => ");flist_print_node(&file_tb[12]);	
	flist_push(&head,&file_tb[11]);printf("push => ");flist_print_node(&file_tb[11]);	
	flist_push(&head,&file_tb[16]);printf("push => ");flist_print_node(&file_tb[16]);	
	flist_push(&head,&file_tb[0]);printf("push => ");flist_print_node(&file_tb[0]);	
	printf("-----[print list start]-----\n");
	printf("     Total %d node      \n",flist_count(&head));	
	flist_traverse(&head,flist_print_node);
	printf("------[print list end]------\n");

	printf("\n>>>>>>>>>>>pop test<<<<<<<<<<<<\n");
	flist_pop(&head,&file_tb[0]);printf("pop => ");flist_print_node(&file_tb[0]);	
	flist_pop(&head,&file_tb[4]);printf("pop => ");flist_print_node(&file_tb[4]);		
	flist_pop(&head,&file_tb[3]);printf("pop => ");flist_print_node(&file_tb[3]);		
	flist_pop(&head,&file_tb[7]);printf("pop => ");flist_print_node(&file_tb[7]);		
	flist_pop(&head,&file_tb[5]);printf("pop => ");flist_print_node(&file_tb[5]);		
	printf("-----[print list start]-----\n");
	printf("     Total %d node      \n",flist_count(&head));	
	flist_traverse(&head,flist_print_node);
	printf("------[print list end]------\n");

	printf("\n>>>>>>>>>>prev and next test<<<<<<<<<<\n");
	pnode = flist_prev(&head);
	printf("head prev node => ");flist_print_node(&pnode->file);
	pnode = flist_next(&head);
	printf("head next node => ");flist_print_node(&pnode->file);

	printf("\n>>>>>>>>>>find node test<<<<<<<<<\n");
	pnode = flist_find_node(&head,&file);	 
	if (IS_ERROR(pnode))	printf("Not found file node (%s) \n",file.name);
	printf("find node => ");flist_print_node(&pnode->file);

	printf("\n>>>>>>>>>>>destroy test<<<<<<<<<<\n");
	flist_destroy(&head);
	printf("-----[print list start]-----\n");
	printf("     Total %d node      \n",flist_count(&head));	
	flist_traverse(&head,flist_print_node);
	printf("------[print list end]------\n");

	exit(0);
	
}
#endif
