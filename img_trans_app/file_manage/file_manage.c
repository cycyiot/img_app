#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <ctype.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <file_manage/file_manage.h>

#define	_RUN_TIME_TEST	0
#if	_RUN_TIME_TEST == 1
#include <sys/time.h>
#endif

struct folder_s{
	char			dir[MAX_NAME_LENGTH];
	unsigned int 	count;
	int				nameid;
	ufile_type		type;
	linklist		head;
};  

struct folder_s *user_folder[MAX_FOLDER_NUM] = {NULL};		

static int dec_to_str(unsigned int dec,char *buf)
{
	int ge,si,ba,qi;
	char str[8];
	
	assert_f(buf);
	memset(str, 0, sizeof(str));
	qi = dec / 1000;
	ba = (dec - qi * 1000) / 100;
	si = (dec - qi * 1000 - ba *100) / 10;
	ge = dec % 10;
	sprintf((char*)str, "%c%c%c%c", toascii(qi+'0'),toascii(ba+'0'),toascii(si+'0'),toascii(ge+'0'));
	strcpy(buf,(const char*)str);	
	
	return 0;	
}
static int user_get_local_dt(struct tm *buf)
{
	time_t stime;
	struct tm *ptm;
	
	assert_f(buf);
	if(((time_t)-1) == time(&stime))
		return -1;
	if(NULL == (ptm = localtime(&stime)))
		return -1;
	memcpy((void*)buf,(const void*)ptm,sizeof(struct tm));
	
	return 0;
}
static int get_local_date_str(char *buf)
{
	char yr[8],mn[8],dy[8],hr[8],mi[8],sc[8];
	struct tm dt={0};
	char *date = buf;

	assert_f(buf);
  	if(user_get_local_dt(&dt))
		return -1;

	dt.tm_year += 1900;
	dt.tm_mon  += 1;

	dec_to_str(dt.tm_year,(char*)&yr);
	dec_to_str(dt.tm_mon,(char*)&mn);
	dec_to_str(dt.tm_mday,(char*)&dy);

	dec_to_str(dt.tm_hour,(char*)&hr);
	dec_to_str(dt.tm_min, (char*)&mi);
	dec_to_str(dt.tm_sec, (char*)&sc);

	sprintf(date,"%s%s%s%s%s%s",&yr[2],&mn[2],&dy[2],&hr[2],&mi[2],&sc[2]);

	return 0;
}
static int _file_type_get(const char *fname)
{
	int ty;
	char *tp = (char*)fname;
	
	assert_f(fname);
	while(('\0' != *tp) && ('.' != *tp)){
		tp++;
	}
	if('\0' == *tp)
		return -1;
	tp++;
	if(0 == strcmp((const char*)tp,(const char*)PHOTO_FILE_SUFF))
		ty = PHOTO_FILE_TYPE;		
	else if(0 == strcmp((const char*)tp,(const char*)VIDEO_FILE_SUFF))
		ty = VIDEO_FILE_TYPE;		
	else
		ty = UNKNOW_FILE_TYPE;	
	
	return ty;
}
static unsigned long long _file_creat_time_get(const char *fname)
{
	char *tp;
	char ctime[32];
	char *name = (char*)fname;
	
	assert_f(name);
	if(strlen(name) > (MAX_NAME_LENGTH-1)){
		PRINT_ERR("file name %s is too long\n",name);
		return -1;
	}
	memset(ctime, 0, sizeof(ctime));
	tp = (char*)&ctime;
	while(('.' != *name) && ('\0' != *name)){
		if(*name < '0' || *name > '9'){
			name++;
			continue;
		}
		*tp = *name;
		tp++;
		name++;
	}
	if('\0' == *name){
		PRINT_ERR("file name %s is not correct\n",name);
		return -1;
	}
	*tp = '\0';
	
	return (unsigned long long)atoll((const char*)ctime);
}
static int _file_name_id_get(const char *name,int *fid)
{
	char *pt = NULL;
	char id[8] = {'\0'};
	int cn = 0;
	
	*fid = -1;
	assert_f(name);
	while('\0' != *name && '_' != *name)	
		name++;
	if('_' == *name){
		name++;
		pt = (char*)name;
		while('\0' != *name && '.' != *pt){
			pt++;
			cn++;
		} 
		if('.' == *pt){
			memcpy((void*)id,(const void*)name,cn);	
			*fid = atoi(id);
			return 0;
		}
	}
	return -1;	
}
static int _file_name_prefix_get_str(const char *name,char *str)
{
	int n;

	assert_f(name);
	assert_f(str);
	while('\0' != *name && '_' != *name)	
		name++;
	if('_' == *name){
		name -= 12;	/*file name pattern is 161029211300_001.mp4,so offset -12 */
		for(n = 0;n < 12;n++){
			*str++ = *name++;
		}	
		*str = '\0';
		return 0;
	}
	return -1;
}
int user_file_name_suffix_chang(char *name, const char *suffix)
{
	char *newname = name;
		
	assert_f(name);
	assert_f(suffix);
	while('\0' != *newname && '.' != *newname)	
		newname++;
	if('.' == *newname){
		newname++;
		memset(newname, '\0', strlen(newname));
		memcpy(newname, suffix, strlen(suffix));
		return 0;
	}

	return -1;
}
int _file_name_split(const char *full_name,char *dirname,char *basename)
{
	char *cp;
	int len,point = 0;

	assert_f(full_name);
	len = strlen(full_name);
	cp = (char*)full_name + len - 1;	
	while('/' != *cp--)	point++;
	if(point > len-1)
		return -1;
	point = len - point;
	if(dirname){
		strncpy(dirname, full_name, point);
		dirname[point] = '\0';	
	}
	if(basename){
		strcpy(basename, (const char*)(full_name+point));
	}
	return 0;	
}
static int _file_name_curdate_is_changed(const char *latest_file)
{
	char fname_date[16] = {'\0'};
	char local_date[16] = {'\0'};
	
	if(_file_name_prefix_get_str(latest_file,(char*)fname_date)){
		PRINT_ERR("get date from file name fail \n");
		return -1;
	}
	if(get_local_date_str((char*)local_date)){
		PRINT_ERR("get local date fail\n");
		return -1;
	}
	//PRINT_DBG("\n===file name date(%s),local date(%s)===\n",fname_date,local_date);
	if(strncmp((const char*)fname_date,(const char*)local_date,16)){
		PRINT_DBG("file name prefixion is need to update \n");
		return 1;
	}
	
	return 0;
}
static int _user_folder_refresh(struct folder_s *pfolder,int file_type)
{
	DIR *dr;
	struct dirent *dm;
	struct file_s fe;
	char cdir[32] = {'\0'};
	struct folder_s *pfd = pfolder;
	unsigned int cnt = 0;
	unsigned long long ctm = 0;
	
#if	_RUN_TIME_TEST == 1	
	struct timeval stv,etv;
#endif
	if(NULL == (dr = opendir((const char*)pfd->dir))){
		PRINT_ERR("open %s dir fail\n",(const char*)pfd->dir);
		perror("open dir");
		return -1;
	}
#if	_RUN_TIME_TEST == 1		
	gettimeofday(&stv,NULL);
#endif
	getcwd(cdir,sizeof(cdir));
	chdir((const char*)pfd->dir);
	flist_init(&pfd->head);
	if(file_type == PHOTO_THUMB_FILE_TYPE || file_type == VIDEO_THUMB_FILE_TYPE){
		file_type = PHOTO_FILE_TYPE;
	}
	while(NULL != (dm = readdir(dr))){		
		if(!strcmp((const char*)".", (const char*)dm->d_name) ||
		   !strcmp((const char*)"..", (const char*)dm->d_name)){
			continue;
		}		
		if((file_type != _file_type_get((const char*)dm->d_name)) || 
			((ctm = _file_creat_time_get((const char*)dm->d_name)) < 0)){
			continue;
		}
		strcpy((char*)&fe.dir, (const char*)pfd->dir);
		strcpy((char*)&fe.name,(const char*)dm->d_name);
		//sprintf((char*)&fe.ne,"%s%s",(char*)pfd->ne,dm->d_name);
		fe.status 	= 0;
		fe.lock 	= 0;
		fe.bsize 	= 0;
		fe.ctime 	= ctm;
		if(flist_push(&pfd->head,&fe)){
			PRINT_ERR("file %s push fail\n",fe.name);
			continue;
		}
		cnt++;
	}
#if	_RUN_TIME_TEST == 1	
	gettimeofday(&etv,NULL);
#endif
	chdir(cdir);
#if	_RUN_TIME_TEST == 1
	printf("\n\n===list generate time(%lu ms)===\n",(etv.tv_sec * 1000 + etv.tv_usec/1000)-(stv.tv_sec * 1000 + stv.tv_usec/1000));
#endif
	if(closedir(dr)){
		PRINT_ERR("close %s folder fail\n",pfd->dir);
		return -1;
	}
	//pfd->type 	= file_type;
	pfd->count 	= cnt;
	
	return 0;
}
int user_folder_refresh(void)
{
	int id;
	unsigned int n;
	
	for(n = 0; n < MAX_FOLDER_NUM; n++){
		if(NULL != user_folder[n]){
			PRINT_DBG("refresh %s folder\n",user_folder[n]->dir);
			if(access((const char*)user_folder[n]->dir,F_OK)){
				PRINT_DBG("%s folder is not exist,it will be create...\n",user_folder[n]->dir);
                                if(mkdir((const char*)user_folder[n]->dir,S_IRUSR|S_IWUSR)){
					PRINT_ERR("%s folder creat fail\n",user_folder[n]->dir);
					//return -1;
				}
				user_folder[n]->nameid = 0;
			}else{
				if(_user_folder_refresh(user_folder[n],user_folder[n]->type)){
					PRINT_ERR("refresh %s folder fail\n",user_folder[n]->dir);
					flist_destroy(&user_folder[n]->head);
					return -1;
				}
				if(user_folder[n]->head) 
					_file_name_id_get((const char*)&user_folder[n]->head->file.name, &id);
				else
					id = 0;
				user_folder[n]->nameid = id;	
			}
		}
	}

	return 0;
}
void user_folder_file_list_free(void)
{
	int n;

	for(n = 0; n < MAX_FOLDER_NUM; n++){
		if((NULL != user_folder[n]) && (NULL != user_folder[n]->head)){
			flist_destroy(&user_folder[n]->head);
			user_folder[n]->head 	= NULL;
			user_folder[n]->count 	= 0;
		}
	}
}
int user_folder_init(const char *dir,enum file_type type)
{
	int n;
	struct folder_s *ufd;

	assert_f(dir);
	ufd = (struct folder_s*)malloc(sizeof(struct folder_s));
	if(IS_ERROR(ufd)){
		PRINT_ERR("malloc memory fail\n");
		return -1;
	}
	memset((void*)ufd, 0 , sizeof(struct folder_s));
	strcpy((char*)ufd->dir, dir);
	ufd->type 	= type;
	ufd->head 	= NULL;
	for(n = 0; n < MAX_FOLDER_NUM; n++){
		if(NULL == user_folder[n]){
			user_folder[n] = ufd;
			break;
		}
	}
	if(n >= MAX_FOLDER_NUM){
		PRINT_ERR("folder %s register fail,supported folder number max is %d \n",ufd->dir,MAX_FOLDER_NUM);
		free(ufd);
		return -1;
	}
	return 0;
}
void user_folder_exit(void)
{
	int n;

	for(n = 0; n < MAX_FOLDER_NUM; n++){
		if(NULL != user_folder[n]){
			flist_destroy(&user_folder[n]->head);
			free(user_folder[n]);
			user_folder[n] = NULL;
		}
	}
}
static enum file_type user_folder_get_file_type(const char *folder)
{
	enum file_type file_type;
	
	if(!strcmp(folder,PHOTO_FOLDER))
		file_type = PHOTO_FILE_TYPE;
	else if(!strcmp(folder,VIDEO_FOLDER))
		file_type = VIDEO_FILE_TYPE;
	else if(!strcmp(folder,VIDEO_THUMB_FOLDER))
		file_type = VIDEO_THUMB_FILE_TYPE;
	else if(!strcmp(folder,PHOTO_THUMB_FOLDER))
		file_type = PHOTO_THUMB_FILE_TYPE;
	else
		file_type = UNKNOW_FILE_TYPE;

	return file_type;
}
static int user_folder_get_id(enum file_type type, int *id)
{
	int n;

	for(n = 0; n < MAX_FOLDER_NUM; n++){
		if(user_folder[n] && (type == user_folder[n]->type)){
			*id = n;
			break;
		}
	}
	if(n >= MAX_FOLDER_NUM)
		return -1;

	return 0;
}

int user_folder_file_number_get(const char *folder_name)
{
	int id;
	enum file_type file_type;

	assert_f(folder_name);
	file_type = user_folder_get_file_type(folder_name);	
	if(user_folder_get_id(file_type,&id)){
		PRINT_ERR("folder %s is not exist or not init\n",folder_name);
		return -1;
	}
	return user_folder[id]->count;
}
static int _user_file_name_id_is_reset(const int file_type)
{
	int n;

	if(file_type != PHOTO_FILE_TYPE && file_type != VIDEO_FILE_TYPE){
		PRINT_ERR("file type (%d) error\n", file_type);
		return -1;
	} 
	for(n = 0; n < MAX_FOLDER_NUM; n++){
		if(NULL == user_folder[n])
			continue;
		if(file_type == user_folder[n]->type){
			if(!user_folder[n]->count)	/* no file, so reset id to zero */
				return 1;
			if(_file_name_curdate_is_changed((const char*)user_folder[n]->head->file.name) > 0)
				return 1;
		}
	}

	return 0;	
}
static int _user_file_name_generate(const int file_type, char name[])
{
	int fdid; 
	char *sf;
	char id[8],date[64];

	if(user_folder_get_id(file_type,&fdid)){
		PRINT_ERR("Not found folder which type is %d \n",file_type);
		return -1;
	}
	if(_user_file_name_id_is_reset(file_type) > 0){
		user_folder[fdid]->nameid = 0;		
	}else{
		if(++user_folder[fdid]->nameid > MAX_FILE_ID)
			user_folder[fdid]->nameid = 0;		
	}
	if(file_type == PHOTO_FILE_TYPE)		sf = PHOTO_FILE_SUFF;
	else if(file_type == VIDEO_FILE_TYPE)	sf = VIDEO_FILE_SUFF;
	else									sf = NULL;
	if(IS_ERROR(sf))	
		return -1;
	dec_to_str(user_folder[fdid]->nameid, (char*)&id);
	if(get_local_date_str((char*)date))
		return -1;
	sprintf(name, "%s_%s.%s", date,&id[2],sf);
			
	return 0;
}
static int user_file_list_node_delete(const char *full_name)
{
	int n;
	struct file_s file;
	char dir[32],name[32];
	
	assert_f(full_name);
	memset(dir, '\0', sizeof(dir));
	memset(name,'\0', sizeof(name));
	_file_name_split(full_name, dir, name);
	for(n = 0 ;n < MAX_FOLDER_NUM; n++){
		if((NULL == user_folder[n]) || (NULL == user_folder[n]->head))
			continue;
		if(!strcmp(dir,user_folder[n]->dir)){
			memset(&file, 0, sizeof(file));
			strncpy(file.name, name, strlen(name));
			if(flist_pop(&user_folder[n]->head, &file)){
				PRINT_ERR("pop file node fail,please update %s folder file list...\n",user_folder[n]->dir);
				return -1;
			}			
			user_folder[n]->count--;
			break;
		}
	}
	if(n >= MAX_FOLDER_NUM){
		PRINT_ERR("not find file node,please update file list...\n");
		return -1;
	}
	return 0;
}

static int _file_info_get(const char *full_name, struct file_s *buf)
{
	char dirname[32],basename[32];
	unsigned long long ctm;
	struct stat info;
		
	assert_f(full_name);
	assert_f(buf);
	if(stat(full_name,&info)){
		PRINT_ERR("file %s do stat fail\n",full_name);
		return -1;
	}
	_file_name_split(full_name,dirname,basename);
	strcpy((char*)&buf->dir, dirname);
	strcpy((char*)&buf->name, basename);
	buf->status = 0;
	if(!(info.st_mode & S_IWUSR))	
		buf->lock = 1;
	else	
		buf->lock = 0;
	buf->bsize = info.st_size;
	if((ctm = _file_creat_time_get(basename)) < 0){
		PRINT_ERR("get file creat time fail\n");
		return -1;
	}
	buf->ctime = ctm;

	return 0;
}

int user_file_name_generate(enum file_type type, char name[])
{
	int id;
	char buf[64];
	
	assert_f(name);
	memset(buf, 0, sizeof(buf));
	if(user_folder_get_id(type, &id)){
		PRINT_ERR("not find folder which type is %d \n",type);
		return -1;
	}
	if(_user_file_name_generate(type,buf)){
		PRINT_ERR("file name generate fail %s \n",buf);
		return -1;
	}
	sprintf(name,"%s%s", user_folder[id]->dir,buf);
	return 0;
}
int user_file_list_update(const char *full_name)
{
	int id;
	char basename[32],dirname[32];
	enum file_type ftype;
	struct file_s buf;

	assert_f(full_name);
	_file_name_split(full_name,dirname,basename);
	ftype = user_folder_get_file_type(dirname);
	if(user_folder_get_id(ftype,&id)){
		PRINT_ERR("get %s id fail \n",dirname);
		return -1;
	}
	if(_file_info_get(full_name, &buf)){
		PRINT_ERR("get file %s infomation fail\n",full_name);
		return -1;
	}
	if(flist_push(&user_folder[id]->head,&buf)){
		PRINT_ERR("push file %s to list fail\n",full_name);
		return -1;
	}
	user_folder[id]->count++;	
	
	return 0;
}
FILE *user_file_open(const char *full_name, const char *mode)
{
	if(IS_ERROR(full_name) || IS_ERROR(mode)){
		PRINT_ERR("parameter is error\n");
          	return NULL;
	}
	return fopen(full_name,mode);	
}
int user_file_close(const char *full_name, FILE *fp)
{
	assert_f(fp);
	if(fclose(fp)){
		PRINT_ERR("close file fail\n");
		return -1;
	}
	if(NULL == full_name)
		return 1;
	if(user_file_list_update(full_name)){
		PRINT_ERR("update file (%s) list fail\n",full_name);
		return 1;
	}
	return 0; 	
}
size_t user_file_write(const void *buffer, size_t size, size_t nsize, FILE *fp)
{
	assert_f(buffer);
	assert_f(fp);
	return fwrite(buffer,size,nsize,fp);	
}
size_t user_file_read(char *buffer, size_t size, size_t nsize, FILE *fp)
{
	assert_f(buffer);
	assert_f(fp);
	return fread(buffer,size,nsize,fp);
}
int user_file_save(const char *buf, const unsigned int size, const char *full_name)
{
	FILE *fd;
	int res;
	
	assert_f(buf);
	assert_f(full_name);
	fd = user_file_open(full_name,"wb");
	if(IS_ERROR(fd)){
		PRINT_ERR("open %s file fail\n",full_name);
		return -1;
	} 
	user_file_write(buf,size,1,fd);
	res = user_file_close(NULL,fd);
	
	return res;
}
int user_file_data_get(char *full_name, unsigned int *size, char *buf)
{
    FILE *fp = NULL;
    int ret = 0;
    char *data = buf;

	if(!full_name || !size || !data)
		return -1;
	if(access(full_name,F_OK) < 0){
		PRINT_ERR(" %s file is not exist \n",full_name);
		return -1;
	}
    fp = fopen(full_name, "rb");
    if(fp == NULL){
        PRINT_ERR("read jpeg file error");
        return -1;
    }
    fseek(fp,0,SEEK_END);
    *size = ftell(fp);
    rewind(fp);
	ret = fread(data,1,*size,fp);
    if (ret != *size)
    {
        PRINT_ERR("read file fail");
        fclose(fp);
        return -1;
    }
    fclose(fp);

    return 0;
}

int user_file_delete(const char *full_name)
{
	int ret = -1;
	
	assert_f(full_name);
	if(access(full_name,F_OK) < 0){
		PRINT_ERR(" %s file is not exist \n",full_name);
		return 0;
	}
	if(unlink(full_name)){
		PRINT_ERR("unlink %s file fail\n",full_name);
		return -1;
	}
	ret = user_file_list_node_delete(full_name);

	return ret;
}

int user_folder_file_list_get(const char *folder, unsigned int offset, unsigned int filenum, struct file_s *buf)
{
	int fid = -1;
	int cnt,n;
	linklist th;
	char full_name[128];
	enum file_type ftype;
	
	assert_f(folder);
	assert_f(buf);
	ftype = user_folder_get_file_type(folder);
	if(user_folder_get_id(ftype, &fid))
		return -1;
	if(offset > user_folder[fid]->count){
		return -1;
	}
	th = user_folder[fid]->head;
	for(n = 0; n < offset; n++){
		th = flist_next(&th);
	}
	cnt = 0;
	memset(full_name, 0, sizeof(full_name));
	while((NULL != th) && (cnt < filenum)){
		snprintf(full_name,strlen(th->file.dir) + strlen(th->file.name) + 1,"%s%s",th->file.dir,th->file.name);
		//printf("copy full name %s\n",full_name);
		_file_info_get(full_name,&th->file);
		memcpy((void*)buf,(const void*)&th->file,sizeof(struct file_s));
		buf++;
		cnt++;
		th = flist_next(&th);
	}

	return cnt;
}

int user_folder_file_list_page_get(const char *folder, int pagenum, int *pagesize, struct file_s *buf)
{
	int ret;
	int filenum,page,offset,getlen;
	int pagelen = *pagesize;	

	assert_f(folder);
	assert_f(buf);
	*pagesize = 0;
	filenum = user_folder_file_number_get(folder);	
	if(filenum <= 0){
		PRINT_ERR("%s folder have not file\n",folder);
		return -1;
	}
	page	=	filenum / pagelen;
	offset	=	filenum	% pagelen;
	if(offset)	page++;
	if(pagenum > page){
		PRINT_ERR("request pagenum(%d) is large than real pagenum(%d)\n", pagenum,page);
		return -1;
	}else{
		getlen = pagelen;
		if(pagenum == page && offset)
			getlen = offset;
		ret = user_folder_file_list_get(folder,(pagenum - 1) * pagelen,getlen,buf);
		if(ret != getlen){
			PRINT_ERR("file list get error, want get len(%d),return ret(%d)\n", getlen, ret);
			user_folder_list_printf();
			return -1;
		}
		*pagesize = getlen;
	}

	return 0;
}


void user_folder_list_printf(void)
{
	int n;
	for(n = 0; n < MAX_FOLDER_NUM; n++){
		if(NULL != user_folder[n]){
			printf("\n=============[PRINT ALL FILE]==================\n");
			printf("******%s folder have %d file *******\n",
			user_folder[n]->dir,
			user_folder_file_number_get(user_folder[n]->dir));
			flist_traverse(&user_folder[n]->head,flist_print_node);
			printf("===================[PRINT END]===================\n\n");
		}
	}
}

//#define	__FILE_MANAGE_TEST__
#ifdef	__FILE_MANAGE_TEST__
int main(int argc, char *argv[])
{

	printf("file manage test \n");

	exit(0);
}
#endif 
