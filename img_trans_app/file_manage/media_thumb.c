#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <file_manage/file_manage.h>
#include <file_manage/media_thumb.h>

#define	MEDIA_THUMB_FNAME_LIST_NUM_MAX	(9999)

static int media_thumb_get_all_name_list(struct app_thumb_st *thumb)
{
	int filenum,ret;
	
	if(!thumb){
		PRINT_ERR("invalid pointer\n");
		return -1;
	}
	filenum = user_folder_file_number_get(thumb->folder_thum);
	if(filenum < 0){
		PRINT_ERR("get user folder %s file number fail\n",thumb->folder_thum);
		return -1;
	}
	if(filenum > MEDIA_THUMB_FNAME_LIST_NUM_MAX) {
		PRINT_ERR("get %s folder file number is too larger (%d)\n",thumb->folder_thum,filenum);
		filenum = MEDIA_THUMB_FNAME_LIST_NUM_MAX;
	}
	thumb->list_type = 0;
	thumb->list_size = filenum;
	thumb->list_data = malloc(filenum * sizeof(struct file_s));
	if(!thumb->list_data){
		PRINT_ERR("malloc list buf fail \n");
		return -1;
	}
	ret = user_folder_file_list_get(thumb->folder_thum, 0, filenum, (struct file_s*)thumb->list_data);
	if(ret != filenum){
		PRINT_ERR("get folder %s file list fail\n", thumb->folder_thum);
		return -1;
	}
	thumb->page_size = thumb->list_size;
	
	return 0;
}
int media_thumb_get(struct app_thumb_st *thumb)
{
	int n,ret;
	unsigned int fsize = 0;
	char *offset;
	char full_name[64];
	struct file_s *flist;
	
	if(!thumb){
		PRINT_ERR("invalid pointer\n");
		return -1;
	}
	if(thumb->file_type == VIDEO_FILE_TYPE) {
		thumb->folder_name = (char*)MEDIA_VIDEO_REAL_DIR;
		thumb->folder_thum = (char*)VIDEO_THUMB_FOLDER;
		thumb->thum_suffix = (char*)VIDEO_THUMB_FILE_SUFF;		
	}else if(thumb->file_type == PHOTO_FILE_TYPE) {
		thumb->folder_name = (char*)MEDIA_PHOTO_REAL_DIR;
		thumb->folder_thum = (char*)PHOTO_THUMB_FOLDER;
		thumb->thum_suffix = (char*)PHOTO_THUMB_FILE_SUFF;
	}else {
		PRINT_ERR("media thumbnail file type(%d) error\n",thumb->file_type);
		return -1;	
	}
	/* get all file name list of the folder */
	if(thumb->page_size == 0 && thumb->page_numb == 0 && thumb->list_type == MEDIA_THUMB_NAME_LIST){
		media_thumb_get_all_name_list(thumb);
		goto out;
	}
	else if(thumb->page_size > MEDIA_THUMB_PAGENUM_MAX) {
		PRINT_ERR("media thumbnail page size is too large(%d)\n", thumb->page_size);
		thumb->page_size = MEDIA_THUMB_PAGENUM_MAX;
	}
	
	flist = malloc(sizeof(struct file_s) * thumb->page_size);
	if(!flist){
		PRINT_ERR("%s:malloc file_s buf fail\n",__func__);
		return -1;
	}
	ret = user_folder_file_list_page_get((const char*)thumb->folder_thum,	thumb->page_numb, &thumb->page_size, flist);
	if(ret) {
		PRINT_ERR("get user file list fail\n");
		free(flist);
		return -1;
	}
	if(thumb->list_type == 0) {		
		for(n = 0; n < thumb->page_size; n++) 
			user_file_name_suffix_chang(flist[n].name, (const char*)thumb->thum_suffix);
		thumb->list_data = (void*)flist;
	}else if(thumb->list_type == 1) {
		thumb->list_size = 0;
		for(n = 0; n < thumb->page_size; n++) {
			thumb->list_size += flist[n].bsize;	
		}
		PRINT_ERR("thumbnail file total size(%d KB)\n", thumb->list_size/1024);					
		thumb->list_data = (char*)malloc(thumb->list_size);
		if(!thumb->list_data) {
			PRINT_ERR("malloc thumbnail data buf(%d KB) fail\n", thumb->list_size/1024);					
			free(flist);
			thumb->list_data = NULL;
			return -1;			
		}
		offset = thumb->list_data;
		for(n = 0; n < thumb->page_size; n++) {
			memset(full_name, 0, sizeof(full_name));
			snprintf(full_name,strlen(flist[n].dir) + strlen(flist[n].name) + 1,"%s%s",thumb->folder_thum,flist[n].name);
			user_file_data_get(full_name,&fsize,offset);
			offset += flist[n].bsize;
		}
		free(flist);
	}else {
		PRINT_ERR("media thumbnail list type(%d) error\n",thumb->list_type);
		free(thumb->list_data);
		thumb->list_data = NULL;
		return -1;
	}
out:	
	return 0;
}

int media_thumb_put(struct app_thumb_st *thumb)
{
	if(!thumb)
		return -1;
	if(thumb->list_data)
		free(thumb->list_data);

	return 0;
}

int media_thumb_file_delete(struct app_thumb_st *thumb)
{
	int n;
	char full_name[128];
	
	if(!thumb || !thumb->list_data){
		PRINT_ERR("invalid pointer 0x%x\n",(int)thumb);
		return -1;
	}
	if(!strcmp(thumb->folder_name, MEDIA_VIDEO_REAL_DIR)){
		for(n = 0; n < thumb->list_size; n++) {
			memset(full_name, 0, sizeof(full_name));	
			memcpy(full_name, VIDEO_THUMB_FOLDER, strlen((char*)VIDEO_THUMB_FOLDER));
			strncat(full_name, thumb->list_data + n * MAX_NAME_LENGTH, MAX_NAME_LENGTH);
			user_file_name_suffix_chang((char*)full_name, VIDEO_THUMB_FILE_SUFF);
			PRINT_ERR("delete %s file ... \n",full_name);
			user_file_delete(full_name);
		}
		for(n = 0; n < thumb->list_size; n++) {
			memset(full_name, 0, sizeof(full_name));	
			memcpy(full_name, VIDEO_FOLDER, strlen((char*)VIDEO_FOLDER));
			strncat(full_name, thumb->list_data + n * MAX_NAME_LENGTH, MAX_NAME_LENGTH);
			user_file_name_suffix_chang((char*)full_name, VIDEO_FILE_SUFF);
			PRINT_ERR("delete %s file ... \n",full_name);
			user_file_delete(full_name);		
		}
	}else if(!strcmp(thumb->folder_name, MEDIA_PHOTO_REAL_DIR)){
		for(n = 0; n < thumb->list_size; n++) {
			memset(full_name, 0, sizeof(full_name));	
			memcpy(full_name, PHOTO_THUMB_FOLDER, strlen((char*)PHOTO_THUMB_FOLDER));
			strncat(full_name, thumb->list_data + n * MAX_NAME_LENGTH, MAX_NAME_LENGTH);
			PRINT_ERR("delete %s file ... \n",full_name);
			user_file_delete(full_name);		
		}		
		for(n = 0; n < thumb->list_size; n++) {
			memset(full_name, 0, sizeof(full_name));	
			memcpy(full_name, PHOTO_FOLDER, strlen((char*)PHOTO_FOLDER));
			strncat(full_name, thumb->list_data + n * MAX_NAME_LENGTH, MAX_NAME_LENGTH);
			PRINT_ERR("delete %s file ... \n",full_name);
			user_file_delete(full_name);		
		}

		return 0;
	}else {
		PRINT_ERR("%s folder is not exist !\n",thumb->folder_name);
		return -1;
	}

	return 0;
}




