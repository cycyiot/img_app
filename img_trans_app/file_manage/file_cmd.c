#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <ctype.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <file_manage/file_manage.h>
#include <net/cmd_handle.h>
#include <platform_cfg.h>
#include <file_manage/media_thumb.h>

int user_file_cmd_handler_file_delete(struct cmd_param *cmdParam)
{
	int ret = -1, len, n;
	struct app_thumb_st app_thumb;
	struct json_object *jsonSubParamObj;
	struct json_object *jsonSubArryObj;
	const char *cmdString;

	if(is_error(cmdParam->param))
		goto err_exit;
	memset(&app_thumb, 0, sizeof(struct app_thumb_st));
	jsonSubParamObj = json_object_object_get(cmdParam->param, "folder");
	if(json_object_get_type(jsonSubParamObj) == json_type_string){
		app_thumb.folder_name = (char*)json_object_get_string(jsonSubParamObj);
	}
	jsonSubParamObj = json_object_object_get(cmdParam->param, "filelist");
	if(json_object_get_type(jsonSubParamObj) == json_type_array){
		len =  json_object_array_length(jsonSubParamObj);
		app_thumb.list_type = 0;
		app_thumb.list_size = len;
		app_thumb.list_data = malloc(MAX_NAME_LENGTH * app_thumb.list_size);
		if(!app_thumb.list_data) {
			goto err_exit;
		}
		memset(app_thumb.list_data, 0 ,sizeof(MAX_NAME_LENGTH * app_thumb.list_size)); 
		for(n = 0; n < len; n++){
			jsonSubArryObj = json_object_array_get_idx(jsonSubParamObj,n);
			cmdString = json_object_get_string(jsonSubArryObj);
			printf("get json array[%d]=%s\n",n,cmdString);
			memcpy((void*)((char*)app_thumb.list_data + n * MAX_NAME_LENGTH), cmdString, MAX_NAME_LENGTH);
		}
		media_thumb_file_delete(&app_thumb);
		free(app_thumb.list_data);
	}
	ret = 0;
err_exit:
	cmd_response(ret, cmdParam);
	return ret;
}

void thumbnail_info_notify(struct app_thumb_st *apk_thumb, struct cmd_param *cmdParam, int result)
{
	struct json_object *jsonPacketObj = NULL;
	struct json_object *jsonParamObj = NULL;
	struct json_object *jsonArryObj = NULL;
	struct json_object *jsonSubObj = NULL;
	struct file_s *flist = (struct file_s*)apk_thumb->list_data;
	unsigned int len;
	int n;

	jsonPacketObj = json_object_new_object();
	json_object_object_add(jsonPacketObj, "CMD", json_object_new_int(cmdParam->id));
	if(result != 0) {
		json_object_object_add(jsonPacketObj, "PARAM", json_object_new_int(-1));
		json_object_object_add(jsonPacketObj, "RESULT", json_object_new_int(result));
		goto Out;
	}
	jsonParamObj = json_object_new_object();
	json_object_object_add(jsonParamObj,"filetype",json_object_new_int(apk_thumb->file_type));
	json_object_object_add(jsonParamObj,"folder",json_object_new_string((const char*)apk_thumb->folder_name));
	json_object_object_add(jsonParamObj,"pagenum",json_object_new_int(apk_thumb->page_numb));
	json_object_object_add(jsonParamObj,"pagesize",json_object_new_int(apk_thumb->page_size));
	jsonArryObj = json_object_new_array();
	if (apk_thumb->page_size > 0 && flist){
		for(n = 0; n < apk_thumb->page_size; n++) {
			jsonSubObj	= json_object_new_object();
			json_object_object_add(jsonSubObj,"name",json_object_new_string((const char*)flist[n].name));
			json_object_object_add(jsonSubObj,"size",json_object_new_int(flist[n].bsize));
			json_object_array_add(jsonArryObj,jsonSubObj);
		}
	}
	json_object_object_add(jsonParamObj,"filelist",jsonArryObj);
	json_object_object_add(jsonPacketObj, "PARAM", jsonParamObj);
	json_object_object_add(jsonPacketObj, "RESULT", json_object_new_int(result));
Out:
	len = strlen(json_object_to_json_string(jsonPacketObj)) + 1;
	TcpClientSend(cmdParam->client, json_object_to_json_string(jsonPacketObj), len);
	PRINT_DBG("%s: %s\n", __func__, json_object_to_json_string(jsonPacketObj));
	json_object_put(jsonPacketObj);
}

int user_file_cmd_handler_list_get(struct cmd_param *cmdParam)
{
	int ret = -1;
	struct app_thumb_st app_thumb;
	struct json_object *jsonSubParamObj;
	
	if(is_error(cmdParam->param))
		goto err_exit;
	memset(&app_thumb, 0, sizeof(struct app_thumb_st));
	jsonSubParamObj = json_object_object_get(cmdParam->param, "filetype");
	if(json_object_get_type(jsonSubParamObj) == json_type_int){
		app_thumb.file_type = json_object_get_int(jsonSubParamObj);
	}
	jsonSubParamObj = json_object_object_get(cmdParam->param, "listtype");
	if(json_object_get_type(jsonSubParamObj) == json_type_int){
		app_thumb.list_type = json_object_get_int(jsonSubParamObj);
	}
	jsonSubParamObj = json_object_object_get(cmdParam->param, "pagenum");
	if(json_object_get_type(jsonSubParamObj) == json_type_int){
		app_thumb.page_numb = json_object_get_int(jsonSubParamObj);
	}
	jsonSubParamObj = json_object_object_get(cmdParam->param, "pagesize");
	if(json_object_get_type(jsonSubParamObj) == json_type_int){
		app_thumb.page_size = json_object_get_int(jsonSubParamObj);
	}
	ret = media_thumb_get(&app_thumb);
	if(ret) {
		thumbnail_info_notify(&app_thumb,cmdParam,0);
	}else if(app_thumb.list_type == MEDIA_THUMB_NAME_LIST) {
		thumbnail_info_notify(&app_thumb,cmdParam,0);
	}else if(app_thumb.list_type == MEDIA_THUMB_DATA_LIST) {
		TcpClientSend(cmdParam->client, (const char*)app_thumb.list_data, app_thumb.list_size);
	}
	media_thumb_put(&app_thumb);
err_exit:
	return ret;
}
struct cmd user_file_cmd[] = {
	{CMD_GET_DISK_FILE_LIST, user_file_cmd_handler_list_get},
	{CMD_DELETE_DISK_FILE, user_file_cmd_handler_file_delete},
};

int file_cmd_init(void)
{
	return cmd_register(user_file_cmd, ARRAY_SIZE(user_file_cmd));
}
