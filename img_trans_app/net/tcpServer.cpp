#include "GroupsockHelper.hh"
#include "BasicUsageEnvironment.hh"
#include "tcpServer.hh"
#include <pthread.h>
#include "TcpClientConnection.hh"
#include "stdio.h"
#include <netinet/tcp.h>
#include <net/cmd_handle.h>

typedef struct {
	int fServerSoket;
	pthread_t tcpServerId;
	pthread_attr_t tcpServerThreadAttr; 
}tcpServerContex;

static tcpServerContex gPrivteData;
UsageEnvironment* GtcpServerEnv;

int tcpServerCreate(void)
{
	int ret;

	memset(&gPrivteData,0,sizeof (tcpServerContex));
	ret = pthread_create(&gPrivteData.tcpServerId, NULL, tcpServerThread, NULL);
	if(ret != 0)
		goto err;
	ret=TcpClientInt();
err:
	return ret;
}

void tcpServerDestroy(void)
{
	pthread_cancel(gPrivteData.tcpServerId);
}

int tcpServerAppSetUp(int fServerSoket)
{
	 int keepalive = 1; // keepalive属性
	 int keepidle = 15; // 如该连接在15秒内没有任何数据往来,则进行探测
	 int keepinterval = 1; // 探测时发包的时间间隔为1 秒
	 int keepcount = 15; // 探测尝试的次数.如果第1次探测包就收到响应了,则后2次的不再发.

	 setsockopt(fServerSoket, SOL_SOCKET, SO_KEEPALIVE, (void *)&keepalive , sizeof(keepalive ));
	 setsockopt(fServerSoket, SOL_TCP, TCP_KEEPIDLE, (void*)&keepidle , sizeof(keepidle ));
	 setsockopt(fServerSoket, SOL_TCP, TCP_KEEPINTVL, (void *)&keepinterval , sizeof(keepinterval ));
	 setsockopt(fServerSoket, SOL_TCP, TCP_KEEPCNT, (void *)&keepcount , sizeof(keepcount ));
	 
	 return 0;
}

void tcpServerAppDown(void)
{
	GtcpServerEnv->taskScheduler().turnOffBackgroundReadHandling(gPrivteData.fServerSoket);
	close(gPrivteData.fServerSoket);
}

void *tcpServerThread(void *data)
{
//	 struct sockaddr_in var;
	 TaskScheduler* scheduler = BasicTaskScheduler::createNew();
  	 GtcpServerEnv = BasicUsageEnvironment::createNew(*scheduler);
	 
	 PRINTF("enter tcp Server\n");
	 gPrivteData.fServerSoket = setupStreamSocket(*GtcpServerEnv, 4646);
	 if(listen(gPrivteData.fServerSoket, 5) < 0) {	
		PRINTF("tcp server listen fail\n");
		return NULL;
     }
	 tcpServerAppSetUp(gPrivteData.fServerSoket);
	 GtcpServerEnv->taskScheduler().turnOnBackgroundReadHandling(gPrivteData.fServerSoket, connectionHandler, &gPrivteData);
	 // cmdProtocolInit();
	 GtcpServerEnv->taskScheduler().doEventLoop();
	 
	 return NULL;
}

void connectionHandler(void *data, int mas)
{
	struct sockaddr_in clientAddr;
	socklen_t  clientAddrLen = sizeof(clientAddr);
	
	int clientSocket = accept(gPrivteData.fServerSoket, (struct sockaddr*)&clientAddr, &clientAddrLen);
	if(clientSocket < 0){
		PRINTF("accept() failed\n");
		return;
	}
  	increaseSendBufferTo(*GtcpServerEnv, clientSocket, SERVER_SND_BUF_SIZE);
#ifdef DEBUG
 	*GtcpServerEnv<< "accept()ed connection from " << AddressString(clientAddr).val() << "\n";
#endif
	TcpClientOpen(GtcpServerEnv, clientSocket, &clientAddr);
}
