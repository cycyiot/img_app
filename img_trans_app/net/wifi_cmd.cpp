#ifdef __cplusplus
extern "C" {
#endif
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <json/json.h>
#include <json/bits.h>
#include <net/wifi.h>
#include <net/cmd_handle.h>
#include <system/sys_param.h>
#include <platform_cfg.h>
#include <sys/socket.h> 
#include <arpa/inet.h>

void wifi_station_notify(void)
{
	struct json_object *jsonCfmPacketObj = NULL;
	struct json_object *jsonParam;
	struct wifi_rssi wifi_rssi;
	int i, len;

	for(i=0; i<NET_CLIENT_NUM; i++)
	{
		if(wifi_rssi_get(i, &wifi_rssi) != -1){
			jsonCfmPacketObj = json_object_new_object();
			json_object_object_add(jsonCfmPacketObj, "REPORT", json_object_new_int(RE_WIFI));
			jsonParam = json_object_new_object();
			json_object_object_add(jsonParam, "rssi", json_object_new_int(wifi_rssi.rssi));
			json_object_object_add(jsonCfmPacketObj, "PARAM", jsonParam);
//			PRINTF("%s\n", json_object_to_json_string(jsonCfmPacketObj));
			len = strlen(json_object_to_json_string(jsonCfmPacketObj)) + 1;		
			TcpClientSend(wifi_rssi.client, json_object_to_json_string(jsonCfmPacketObj), len);
			json_object_put(jsonCfmPacketObj);
		}
	}
}

int wifi_set_param_cmd_handler(struct cmd_param *cmdParam)
{
	struct json_object *jsonSubParamObj;
	int ret = -1;
	
	if(is_error(cmdParam->param))
		goto err_exit;
	jsonSubParamObj = json_object_object_get(cmdParam->param, "ssid");
	if((!is_error(jsonSubParamObj)) && (json_object_get_type(jsonSubParamObj)==json_type_string)){
		ret = set_ssid(json_object_get_string(jsonSubParamObj));
		if(ret < 0)
			goto err_exit;
	}
	jsonSubParamObj = json_object_object_get(cmdParam->param, "phrase");
	if((!is_error(jsonSubParamObj)) && (json_object_get_type(jsonSubParamObj)==json_type_string)){
		ret = set_phrase(json_object_get_string(jsonSubParamObj));
	}
	if(!ret){
		cmd_response(0, cmdParam);
		wifi_stop();
		wifi_start();
		mtd_sync();
	}
err_exit:
	if(ret)
		cmd_response(ret, cmdParam);
	return ret;
}

int wifi_rssi_start_cmd_handler(struct cmd_param *cmdParam)
{
	return wifi_station_report_register(cmdParam->client);
}

int wifi_rssi_stop_cmd_handler(struct cmd_param *cmdParam)
{
	return wifi_station_report_unregister(cmdParam->client);
}

struct cmd wifi_cmd[] = {
	{CMD_NET_SSID, wifi_set_param_cmd_handler},
	{CMD_WIFI_RSSI_START, wifi_rssi_start_cmd_handler},
	{CMD_WIFI_RSSI_STOP, wifi_rssi_stop_cmd_handler},
};

int wifi_cmd_init(void)
{
	return cmd_register(wifi_cmd, ARRAY_SIZE(wifi_cmd));
}
#ifdef __cplusplus
}
#endif
