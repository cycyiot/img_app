#include "GroupsockHelper.hh"
#include "BasicUsageEnvironment.hh"
#include "tcpServer.hh"
#include "TcpClientConnection.hh"
#include "stdio.h"
#include "HashTable.hh"
#include <liver/liverDevice.h>
#ifdef __cplusplus
extern "C" {
#endif

#include <net/cmd_handle.h>
#include <system/event.h>
#include <net/wifi.h>
#include <led/led.h>
#ifdef __cplusplus
}
#endif

UsageEnvironment* gEnv = NULL;
static ClientLink *linkContex=NULL;

int TcpClientInt( void)
{
	//gCmdTable=HashTable::create(ONE_WORD_HASH_KEYS);
	linkContex=(ClientLink *)malloc(sizeof(ClientLink));
	if(!linkContex)
		return -1;
	memset(linkContex,0,sizeof(ClientLink));
	memset(linkContex->ourSockets, -1, sizeof(linkContex->ourSockets));
	linkContex->SocketTable=HashTable::create(ONE_WORD_HASH_KEYS);
	if(!linkContex->SocketTable)
	{
		PRINTF("creat table errer\n");
		return -1;
	}
	pthread_mutex_init(&linkContex->mutex, NULL);

	return 0;
}

int TcpClientOpen(void *env, int clientSocket, void *clientAddr)
{
	unsigned int i;
	ClientContex *socketData;
    led_blink(1);
	PRINTF("%s: clientSocket = %d, ClientCount = %d\n", __func__, clientSocket, linkContex->ClientCount);
	if (linkContex->ClientCount >= NET_CLIENT_NUM){
		close(clientSocket);
		PRINTF("only suport %d client\n", NET_CLIENT_NUM);
		return -1;
	}

	for(i=0; i<NET_CLIENT_NUM; i++)
	{
		if(linkContex->ourSockets[i] == -1)
		{
			linkContex->ourSockets[i] = clientSocket;
			break;
		}
	}
	if(i == NET_CLIENT_NUM){
		PRINTF("%s: socket is busy!!!\n", __func__);
		close(clientSocket);
		return -1;
	}
	socketData=(ClientContex*)malloc(sizeof(ClientContex));
	if(!socketData)
	{
		PRINTF("client malloc fail\n");
		close(clientSocket);
		return -3;
	}
	memset(socketData,0,sizeof (ClientContex));
	socketData->ourSocket=clientSocket;
	socketData->clientAddr=(*(struct sockaddr_in *)(clientAddr));
	linkContex->SocketTable->Add((char const *) clientSocket,socketData);
	linkContex->Env=(UsageEnvironment*)env;
	linkContex->Env->taskScheduler().setBackgroundHandling(clientSocket, SOCKET_READABLE|SOCKET_EXCEPTION, TcpClientReceive, &linkContex->ourSockets[i]);
	linkContex->ClientCount++;
	if(linkContex->ClientCount == 1){
		socketData->active=1;
	}else{
		socketData->active=0;
	}
	liverReset();
	return 0;
}

void TcpClientClose(int clientSocket)
{	
	ClientContex *socketData=NULL;
	int active, i;
	
	socketData=(ClientContex *)linkContex->SocketTable->Lookup((char const *) clientSocket);
	if(socketData){
		active = socketData->active;
		linkContex->SocketTable->Remove((char const *)clientSocket);
		free(socketData);
		linkContex->ClientCount--;
		linkContex->Env->taskScheduler().disableBackgroundHandling(clientSocket);
		for(i=0; i<NET_CLIENT_NUM; i++)
		{
			if(linkContex->ourSockets[i] == clientSocket)
			{
				linkContex->ourSockets[i] = -1;
				break;
			}
		}
		/* 如果还有客户端存在，转移控制权*/
		if(active && linkContex->ClientCount)
		{
			for(i=0; i<NET_CLIENT_NUM; i++)
			{
				if(linkContex->ourSockets[i] != -1)
				{
					socketData = (ClientContex *)linkContex->SocketTable->Lookup((char const *)linkContex->ourSockets[i]); 
					if(socketData)
						socketData->active = 1;
					break;
				}
			}
		}
	}
}

int TcpClientActiveCount(void)
{
	return linkContex->ClientCount;
}

void TcpClientReceive(void *Clientsocket, int)
{
	int size = 0;
	int ourSocket=(*(int *)Clientsocket);
	ClientContex *socketData=NULL;
	struct event_data event;

	socketData=(ClientContex *)linkContex->SocketTable->Lookup((char const *) ourSocket);
	if(socketData==NULL)
		return ;
	size = readSocket(*linkContex->Env, ourSocket, (unsigned char *)socketData->RequestBuffer, BUFFER_SIZE, socketData->clientAddr);
	if(size<0){
		if(0 == linkContex->ClientCount){
			event.id = NET_DISCONNECT;
			event.client = 0;
			event_put(&event);
		}
		wifi_station_report_unregister(ourSocket);
		TcpClientClose(ourSocket);
		PRINTF("[%s,%d]:socket 0x%x client disconnect! ClientCount = %d\r\n", __func__, __LINE__,ourSocket, linkContex->ClientCount);
		led_blink(0xff);
		return;
	}	
	PRINTF("received req packet %d bytes, cmd = %s\n", size, socketData->RequestBuffer);
	socketData->RequestBuffer[size] = '\0';
	cmd_handle(socketData);
}

int TcpClientSend(int socket ,const char *buf, unsigned int size)
{
	ClientContex *socketData=NULL;
	unsigned int i;
	int tmpSocket;
	int ret = -1;

	if(size == 0){
		PRINTF("%s:sent cfm packet param error!\r\n", __func__);
		goto nextHdl;
	}

	if(socket == 0)
	{
		for(i=0; i<NET_CLIENT_NUM; i++)
		{
			tmpSocket=linkContex->ourSockets[i];
			socketData = (ClientContex *)linkContex->SocketTable->Lookup((char const *) tmpSocket);
			if(socketData){
				pthread_mutex_lock(&linkContex->mutex);
				ret = writeSocket(*linkContex->Env, tmpSocket, socketData->clientAddr.sin_addr, (portNumBits)socketData->clientAddr.sin_port, (unsigned char *)buf, size);
				pthread_mutex_unlock(&linkContex->mutex);
				PRINTF("broadcast sucess \n");
			}
		}
	}
	else {
		socketData=(ClientContex *)linkContex->SocketTable->Lookup((char const *) socket);
		if(!socketData){
			PRINTF("socket invalue\n");
			goto nextHdl;
		}
		pthread_mutex_lock(&linkContex->mutex);
		writeSocket(*linkContex->Env, socket, socketData->clientAddr.sin_addr, (portNumBits)socketData->clientAddr.sin_port, (unsigned char *)buf, size);
		pthread_mutex_unlock(&linkContex->mutex);
	}
nextHdl:
	return ret;
}
