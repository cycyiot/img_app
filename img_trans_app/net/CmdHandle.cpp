#include <tcpServer.hh>
#ifdef __cplusplus
extern "C" {
#endif
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <system/sys_param.h>
#include <net/cmd_handle.h>

static struct cmd *g_CmdHead = NULL;
static pthread_mutex_t mutex;

void cmd_list(void)
{
	struct cmd *Tmp;
	int i = 0;
	
	Tmp = g_CmdHead;
	while(Tmp != NULL)
	{
		printf("%s: id(%d) = %d\n", __func__, i++, Tmp->id);
		Tmp = Tmp->ptNext;
	}
}

void cmd_response(int result, struct cmd_param *cmdParam)
{
	struct json_object *jsonCfmPacketObj = NULL;
	unsigned int len = 0;

	jsonCfmPacketObj = json_object_new_object();
	json_object_object_add(jsonCfmPacketObj, "CMD", json_object_new_int(cmdParam->id));
	json_object_object_add(jsonCfmPacketObj, "RESULT", json_object_new_int(result));
	len = strlen(json_object_to_json_string(jsonCfmPacketObj)) + 1;
	TcpClientSend(cmdParam->client, json_object_to_json_string(jsonCfmPacketObj), len);
	PRINTF("%s: %s\n", __func__, json_object_to_json_string(jsonCfmPacketObj));
	json_object_put(jsonCfmPacketObj);
}
void cmd_response_steam(int result, struct cmd_param *cmdParam,const char *name,const char *thumb_name)
{
	struct json_object *jsonCfmPacketObj = NULL;
	struct json_object *jsonParamObj =NULL;
	unsigned int len = 0;

	jsonCfmPacketObj = json_object_new_object();
	json_object_object_add(jsonCfmPacketObj, "CMD", json_object_new_int(CMD_IMG_LOAD));
    jsonParamObj=json_object_new_object();
	json_object_object_add(jsonParamObj,"photo",json_object_new_string(name));
	json_object_object_add(jsonParamObj,"thumb",json_object_new_string(thumb_name));
	
	json_object_object_add(jsonCfmPacketObj,"PARAM",jsonParamObj); 
	json_object_object_add(jsonCfmPacketObj, "RESULT", json_object_new_int(result));
	len = strlen(json_object_to_json_string(jsonCfmPacketObj)) + 1;
	TcpClientSend(cmdParam->client, json_object_to_json_string(jsonCfmPacketObj), len);
	PRINTF("%s: %s\n", __func__, json_object_to_json_string(jsonCfmPacketObj));
	json_object_put(jsonCfmPacketObj);
}

int cmd_init(void)
{
	pthread_mutex_init(&mutex, NULL);
	return 0;
}

int cmd_register(struct cmd cmd[], int num)
{
	struct cmd *Tmp;
	int i;

	for(i = 0; i < num; ++i)
	{
		cmd[i].ptNext = &cmd[i+1];
	}
	cmd[num-1].ptNext = NULL;
	pthread_mutex_lock(&mutex);
	if(g_CmdHead != NULL){
		Tmp = g_CmdHead;
		while(Tmp->ptNext)
		{
			Tmp = Tmp->ptNext;
		}
		Tmp->ptNext = &cmd[0];
	}else{
		g_CmdHead = &cmd[0];
	}
//	cmd_list();
	pthread_mutex_unlock(&mutex);
	
	return 0;
}

void cmd_handle(ClientContex *socketData)
{
	struct json_object *jsonReqPacketObj = NULL;
	struct json_object *jsonCmdObj = NULL;
	CMD cmd_id;
	struct cmd *cmd;
	struct cmd_param cmdParam;
	
	PRINTF("%s: cmd = %s\n", __func__, socketData->RequestBuffer);
	jsonReqPacketObj = json_tokener_parse(socketData->RequestBuffer);
	if(is_error(jsonReqPacketObj)){
		PRINTF("json parse err\n");
		return;
	}
	jsonCmdObj = json_object_object_get(jsonReqPacketObj, "CMD");
	if(is_error(jsonCmdObj)){
		PRINTF("%s:receive cmd err!\n", __func__);
		goto err_exit;
	}
	if(json_object_get_type(jsonCmdObj) == json_type_int){
		cmd_id = (CMD)json_object_get_int(jsonCmdObj);
		for(cmd = g_CmdHead; cmd != NULL; cmd = cmd->ptNext)
		{
			if(cmd->id == cmd_id){
				cmdParam.client = socketData->ourSocket;
				cmdParam.id = cmd_id;
				cmdParam.param = json_object_object_get(jsonReqPacketObj, "PARAM");
				cmd->handler(&cmdParam);
				break;
			}
		}
	}else{
		PRINTF("%s:receive cmd type error!\n", __func__);
		goto err_exit;
	}
//    cmd_response(ret, &cmdParam);
err_exit:
	json_object_put(jsonReqPacketObj);
}

#ifdef __cplusplus
}
#endif
