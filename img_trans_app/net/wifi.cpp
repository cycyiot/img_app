#ifdef __cplusplus
extern "C" {
#endif
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <platform_cfg.h>
#include <sys/socket.h> 
#include <arpa/inet.h>
#include <net/wifi.h>
#include <pthread.h>

#define WIFI_SSID "MR100"
#define WIFI_PSW "12345678"
#define WIFI_RSSI_PATH "/sys/kernel/debug/ieee80211/phy0/xradio/station_signal"
#define INVALID (-1)

static  char const ParamPath []="/netPrivate/hostapd.conf";
struct wifi_station{
		pthread_mutex_t mutex;
		int client;
};
static struct wifi_station wifi_station_table[NET_CLIENT_NUM];
static int wifi_station_init_flag = 0;

void wifi_start(void)
{
	system("hostapd_cli -i wlan0 enable");
}

void wifi_stop(void)
{
	system("hostapd_cli -i wlan0 disable");
}

int wifi_set_default_param(void)
{
	int ret;
	char restore[100];

	memset(restore, 0, sizeof(restore));
	sprintf(restore,"sed -i 's/^ssid.*$/ssid=%s/' /netPrivate/hostapd.conf", WIFI_SSID);
	ret = system(restore);

	memset(restore, 0, sizeof(restore));
	sprintf(restore,"sed -i 's/^wpa_passphrase.*$/wpa_passphrase=%s/' /netPrivate/hostapd.conf", WIFI_PSW);
	ret |= system(restore);
	
	return ret;
}

int set_ssid(const char *ssid)
{
	char cmd[100];
	
	if(strlen(ssid)>31){
		PRINTF("ssid too long\n");
		return -1;
	}
	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd,"sed -i 's/^ssid.*$/ssid=%s/' /netPrivate/hostapd.conf",ssid);
	system(cmd); //save ssid 
	sprintf(cmd,"hostapd_cli -i wlan0 set ssid %s",ssid);
	system(cmd);//hostapd modify ssid
	
	return 0;
}

int set_phrase(const char *phrase)
{
	char cmd[100];

	if(strlen(phrase)>31){
		PRINTF("phrase too long!\n");
		return -1;
	}
	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd,"sed -i 's/^wpa_passphrase.*$/wpa_passphrase=%s/' /netPrivate/hostapd.conf",phrase);
	system(cmd);//save prhrase
	sprintf(cmd,"hostapd_cli -i wlan0 set wpa_passphrase %s",phrase);
	system(cmd);//hostapd modify phrase

	return 0;
}

int wifiParmSave(void *param,void *value )
{
	int fileSize;
	FILE * fd;
	char* hostapdContent=NULL;
	char *wordstrat=NULL;
	int wordlen,valueLen;
	char *paramValue=(char *)value;

	fd = fopen(ParamPath,"rw+");
	if(fd==NULL){
		PRINTF("no this file\n");
		return -1;
	}
	fseek(fd,0,SEEK_END);
	fileSize=ftell(fd);
	hostapdContent=(char *)malloc (fileSize);
	if(hostapdContent == NULL){
		PRINTF("%s: malloc failed!\n", __func__);
		fclose(fd);
		return -1;
	}
	fseek(fd,0,SEEK_SET);	
	fread(hostapdContent,fileSize,1,fd);
	wordstrat=strstr(hostapdContent,(char *)param);
	if(wordstrat==NULL){
		PRINTF("no find the %s \n",(char *)param);
		free(hostapdContent);
		fclose(fd);
		return -1;
	}
	wordlen=strlen((const char *)param);
	valueLen=strlen((const char *)value);
	wordstrat=wordstrat+wordlen+1;//save value pos

	for(int i=0;i<32;i++)
	{
		if(valueLen>i){
			*wordstrat=paramValue[i];
		}else if(valueLen==i){
			*wordstrat='\n';
		}else if(valueLen<i){
			*wordstrat='#';
		}
		wordstrat++;
	}

	fseek(fd,0,SEEK_SET);
	valueLen=fwrite(hostapdContent,fileSize,1,fd);
	fclose(fd);
	free(hostapdContent);
	
	return 0;
	
}

int wifiParmRead(const void *param, void *value )
{
	int fileSize;
	FILE * fd;
	char* hostapdContent=NULL;
	char *wordstrat=NULL;
	int wordlen;
	char wordvalue;
	char *paramValue=(char *)value;

	fd = fopen(ParamPath,"r");
	if(fd==NULL){
		PRINTF("no this file\n");
		return -1;
	}
	fseek(fd,0,SEEK_END);
	fileSize=ftell(fd);
	hostapdContent=(char *)malloc (fileSize);
	if(hostapdContent == NULL){
		PRINTF("%s: malloc failed!\n", __func__);
		fclose(fd);
		return -1;
	}
	fseek(fd,0,SEEK_SET);
	fread(hostapdContent,fileSize,1,fd);
	fclose(fd);
	wordstrat=strstr(hostapdContent,(char *)param);
	if(wordstrat==NULL){
		PRINTF("no find the %s \n",(char *)param);
		free(hostapdContent);
		return -1;
	}
	wordlen=strlen((const char *)param);
	wordstrat=wordstrat+wordlen+1;//save value pos
	for(int i=0;i<32;i++)
	{
		wordvalue=*wordstrat++;
		if(wordvalue==' '||wordvalue=='\r'||wordvalue=='\n')
			break;
		else
			paramValue[i]=wordvalue;
	}
	free(hostapdContent);
	return 0;
}


int system_get(const char * cmd, char *pRetMsg, int msg_len)  
{  
    FILE * fp;   

    if((fp = popen(cmd, "r")) == NULL)  
    {  
        printf("Popen Error!\n");  
        return -2;  
    }else{  
        memset(pRetMsg, 0, msg_len);  
        while(fgets(pRetMsg, msg_len, fp) != NULL)  
        {  
//            printf("Msg:%s",pRetMsg);
        }  
        if(pclose(fp) == -1){  
            printf("close popenerror!\n");  
            return -3;  
        }  
        pRetMsg[strlen(pRetMsg)-1] = '\0';  
        return 0;  
    }  
}  
#if 0
int wifi_rssi_get(ClientContex *socketData, int *rssi)  
{  
    char cmd[128];  
    char result[128] = {0};
	struct sockaddr_in *sin_addr =  &socketData->clientAddr;

	if(wifi_station_init_flag == 1){
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd,"cat /proc/net/arp | grep %s | awk -F \" \" '{print $4}'", inet_ntoa(sin_addr->sin_addr));
	    if(system_get(cmd, result, sizeof(result))){
			printf("%s failed!\n", cmd);
			return -1;
	    }
		
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd,"cat %s | grep %s | awk -F \" \" '{print $3}'", WIFI_RSSI_PATH, result);
	    if(system_get(cmd, result, sizeof(result))){
			printf("%s failed!\n", cmd);
			return -2;
	    }
	}else{
		return -3;
	}

	*rssi = atoi(result);
    return 0;  
}  
#endif

static int wifi_station_init(void)
{
	int i, ret = 0;

    for(i=0; i<3; i++)
   	{
		if(access(WIFI_RSSI_PATH, F_OK) == 0){
			break;
		}else{
			system("mount -t debugfs none /sys/kernel/debug");
		}
    }
	if(i >= 3){
		ret = -1;
		goto err_exit;
	}
	
	for(i=0; i<NET_CLIENT_NUM; i++)
	{
		if(pthread_mutex_init(&wifi_station_table[i].mutex, NULL)){
			ret = -2;
			goto err_exit;
		}
		wifi_station_table[i].client = -1;
	}
	
	wifi_station_init_flag = 1;

err_exit:
	return ret;
}

int wifi_station_report_register(int client)
{
	int i, number = INVALID;

	if(!wifi_station_init_flag){
		wifi_station_init();
	}
	for(i=0; i<NET_CLIENT_NUM; i++)
	{
		/* have registered ? */
		if(wifi_station_table[i].client == client){
			return -1;
		}
		else if((number == INVALID) && (wifi_station_table[i].client == -1)){
			number = i;
		}
	}

	if(number != INVALID){
		wifi_station_table[number].client = client;
	}

	return 0;
}

int wifi_station_report_unregister(int client)
{
	int i;

	for(i=0; i<NET_CLIENT_NUM; i++)
	{
		if(wifi_station_table[i].client == client){
			pthread_mutex_lock(&wifi_station_table[i].mutex);
			wifi_station_table[i].client = -1;
			pthread_mutex_unlock(&wifi_station_table[i].mutex);
			return 0;
		}
	}

	return -1;
}

int get_string_file(const char *file, unsigned int line, unsigned int colu)
{
#define BUFFSIZE 512
#define	DELIM " "
	FILE *fp;
	int end;
	unsigned int n,curline;
	char data[BUFFSIZE]; 
	char *get = NULL;

	fp = fopen(file, "r");
	if(!fp){
		PRINTF("open %s file fail\n", file);
		return -1;
	}
	curline = 1;
	while(!(end = feof(fp))){
		if(line == curline){
			fgets(data, BUFFSIZE, fp);
			break;
		}else{
			fgets(data, BUFFSIZE, fp);
			curline++;
		}
	}
	if(end){
		PRINTF("not find %d line\n", line);
		fclose(fp);
		return -1;
	}
	for(n = 0; n < colu; n++)
	{
		if(!n)
			get = strtok(data, DELIM);
		else
			get = strtok(NULL, DELIM);
	}
	if(get != NULL){
		fclose(fp);
		return atoi(get);
	}

	fclose(fp);
	return -1;	
}

int wifi_rssi_get(int num, struct wifi_rssi *wifi_rssi)  
{
	int ret = -1;
	
	if(wifi_station_init_flag == 1){
		pthread_mutex_lock(&wifi_station_table[num].mutex);
		if(wifi_station_table[num].client != -1){
			wifi_rssi->rssi = get_string_file(WIFI_RSSI_PATH, 1, 3);
			wifi_rssi->client = wifi_station_table[num].client;
			ret = 0;
		}
		pthread_mutex_unlock(&wifi_station_table[num].mutex);	
	}
	return ret;
}

int wifi_init(void)
{
	return wifi_cmd_init();
}
#ifdef __cplusplus
}
#endif