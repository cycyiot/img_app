#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <led/led.h>
#include <platform_cfg.h>
#include <net/cmd_handle.h>

static struct led_dev *leds = NULL;

int led_blink(unsigned int count)
{
	char str_count[2];
	int ret = -1;

	if(leds != NULL){
		if(leds->mode){
			sprintf(str_count, "%02x", count);
			if(write(leds->fd_blink, str_count, 2) > 0){
				ret = 0;
			}
		}
	}
	
	return ret;
}

int led_switch(unsigned int state)
{
	char str_state[1];
	int ret = -1;
	
	if(leds != NULL){
		sprintf(str_state, "%x", state);
		if(write(leds->fd_switch, str_state, 1) > 0){
			ret = 0;
		}
	}

	return ret;
}

int led_set_mode(LED_BLINK_MODE mode)
{
	int ret = -1;
	if(leds != NULL){
		switch(mode){
			case LED_BLINK_OFF_ON:
				ret = write(leds->fd_def_state, LED_STATE_OFF, 1);
				break;
			case LED_BLINK_ON_OFF:
				ret = write(leds->fd_def_state, LED_STATE_ON, 1);
				break;
			case LED_BLINK_DISABLE:
				ret = 1;
				break;
			default:
				break;
		}
		if(ret > 0) {
			leds->mode = mode;
			ret = 0;
		}
	}
	return ret;
}

int led_mode_cmd_handler(struct cmd_param *cmdParam)
{
	LED_BLINK_MODE mode;
	int ret = -1;
	
	if((!is_error(cmdParam->param)) && (json_object_get_type(cmdParam->param) == json_type_int)){
		mode = (LED_BLINK_MODE)json_object_get_int(cmdParam->param);
		ret = led_set_mode(mode);
	}
	return ret;
}

struct cmd led_cmd[] = {
	{CMD_LED_MODE_SET, led_mode_cmd_handler},
};

int led_init(void)
{
	if(leds == NULL){
		leds = malloc(sizeof(struct led_dev));
		if(leds == NULL)
			goto err_exit;
		leds->fd_switch = open(LED_SWITCH_PATH, O_WRONLY);
		if(leds->fd_switch > 0){
			leds->fd_blink = open(LED_BLINK_PATH, O_WRONLY);
			leds->fd_def_state = open(LED_DEF_STATE_PATH, O_WRONLY);
			led_set_mode(LED_BLINK_ON_OFF);
			led_blink(0xff);
		}else{
			PRINTF("%s error: cannot find led device!\n", __func__);
			goto err_open;
		}
	}else{
		PRINTF("%s: already been initialized!\n", __func__);
		goto err_exit;
	}
	return cmd_register(led_cmd, ARRAY_SIZE(led_cmd));
err_open:
	free(leds);
	leds = NULL;
err_exit:
	return -1;
}

void led_uninit(void)
{
	if(leds != NULL){
		close(leds->fd_switch);
		close(leds->fd_blink);
		close(leds->fd_def_state);
		free(leds);
		leds = NULL;
	}
}

struct led_dev *led_get_handler(void)
{
	return leds;
}
