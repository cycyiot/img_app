#include <tcpServer.hh>
#ifdef __cplusplus
extern "C" {
#endif
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include <action/action_manager.h>
#include <action/steam_control.h>

#include <storage_mgr/storage_mgr.h>
#include <storage_mgr/ap_tf.h>
#include <camera/camera.h>
#include <system/event.h>
#include <led/led.h>
#include <platform_cfg.h>
#include <system/sys_param.h>
#ifdef FLOW_USED
#include <flow/flow.h>
#endif
#include <net/wifi.h>

void terminate(int sig_no)
{
	struct event_data event;

	PRINTF("%s: get signal %d\n", __func__, sig_no);
	event.id = PROCESS_EXIT;
	event_put(&event);
}

void install_sig_handler(void)
{
	signal(SIGTERM, terminate);
    signal(SIGHUP, terminate);
    signal(SIGINT, terminate);
    signal(SIGQUIT, terminate);
}

int main(void)
{
	struct action_object_t *p_action_object_rec = NULL;

	cmd_init();
	camera_mgr_init();
	frame_encode_init();
	storage_mgr_init();
	led_init();
	action_object_init();
	sys_info_detect_init();
	install_sig_handler();
	event_init();
	wifi_init();
	steam_cmd_init();
	tcpServerCreate();
   // hal_linux_create_thread();
	/* get image trans action */
	p_action_object_rec = action_object_get("action image_trans");
	if(p_action_object_rec == NULL) {
		PRINTF("get action image_trans error");
		return -1;
	}

	/* start image trans action */
	action_run(p_action_object_rec, NULL);
	return 0;
}

#ifdef __cplusplus
}
#endif
