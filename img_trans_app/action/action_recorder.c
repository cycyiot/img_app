#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <msg_queue.h>
#include <storage_mgr/ap_tf.h>
#include <action/action_manager.h>
#include <cedarx/video_encode_api.h>
#include <file_manage/file_manage.h>
#include <camera/camera.h>
#include <unistd.h>
#include <system/event.h>
#include <action/action_recorder.h>
#include <action/action_preview.h>

#define VID_ENC_MGR_SIZE	4
#define AUD_ENC_MGR_SIZE	15
#define AUD_ENC_SIZE	    4096

struct thumb_obj_t{
	MSG_QUEUE_T					   *thumb_msg;
	struct frame_encode_context_st *jpgenc_ctx;
	FRM_OBJ 					   *frame_obj;
	char							name[128];
	int 							capture;
	int								runing;
	sem_t							wait;
};

struct recorder_context_t {
	struct encode_config  encode_cfg;
	struct thumb_obj_t	  video_thumb; 
	MSG_QUEUE_T          *vid_empty_queue;
	MSG_QUEUE_T          *aud_empty_queue;
	MSG_QUEUE_T          *enc_ready_queue;
	int                   audio_bytes_per_ms;
	int                   recorder_initial;
	int                   recorder_timeLapse;
	int                   running_flag;
	volatile int          coding_flag;
	long long             videoPts;
	long long             audioPts;
	volatile long long    init_tm_pts;
	struct camera_data   *p_camera_dev;
	pthread_t             thread_id;
	pthread_t			  thumb_thread_id;
	volatile ACTION_STATE_E   state;
};
struct recorder_context_t *gp_recorder_ctx = NULL;

static int action_recorder_queue_exit(struct recorder_context_t *p_recorder_ctx);

static int action_recorder_queue_int(struct recorder_context_t *p_recorder_ctx)
{
	int enc_queue_cnt = 0;
	int i = 0;
	char *buf = NULL;
    CdxMuxerPacketT *packet = NULL;
	
	if(p_recorder_ctx->encode_cfg.video_cfg) {
		p_recorder_ctx->vid_empty_queue = msg_queue_create(VID_ENC_MGR_SIZE, MSG_TYPE_BLOCK);
		if(p_recorder_ctx->vid_empty_queue == NULL){
			PRINTF("%s:create video encode empty mgr failed!\n", __func__);
			return -1;
		}
		enc_queue_cnt += VID_ENC_MGR_SIZE;
	}

	if(p_recorder_ctx->encode_cfg.audio_cfg) {
		p_recorder_ctx->aud_empty_queue = msg_queue_create(AUD_ENC_MGR_SIZE, MSG_TYPE_BLOCK);
		if(p_recorder_ctx->aud_empty_queue == NULL){
			msg_queue_destory(p_recorder_ctx->vid_empty_queue);
			PRINTF("%s:create audio encode empty mgr failed!\n", __func__);
			return -1;
		}

		enc_queue_cnt += AUD_ENC_MGR_SIZE;
	}

	p_recorder_ctx->enc_ready_queue = msg_queue_create(enc_queue_cnt, MSG_TYPE_BLOCK);
	if(p_recorder_ctx->enc_ready_queue == NULL){
		msg_queue_destory(p_recorder_ctx->vid_empty_queue);
		msg_queue_destory(p_recorder_ctx->aud_empty_queue);
		PRINTF("%s:create video encode ready mgr failed!\n", __func__);
		return -1;
	}

	/* create video queue elements */
	if(p_recorder_ctx->vid_empty_queue) {
		for(i = 0; i < VID_ENC_MGR_SIZE; i++) {
			buf = (char *)malloc(sizeof(CdxMuxerPacketT));
			if(buf) {
				memset(buf, 0, sizeof(CdxMuxerPacketT));
				packet = (CdxMuxerPacketT *)buf;
				packet->is_video = 1;
				if(msg_queue_put(p_recorder_ctx->vid_empty_queue, (INT32U)buf) != 0){
					free(buf);
				}
			}
		}
	}
	
	/* create audio queue elements */
	if(p_recorder_ctx->aud_empty_queue) {
		for(i = 0; i < AUD_ENC_MGR_SIZE; i++) {
			buf = (char *)malloc(sizeof(CdxMuxerPacketT) + AUD_ENC_SIZE);
			if(buf) {
				memset(buf, 0, sizeof(CdxMuxerPacketT));
				packet = (CdxMuxerPacketT *)buf;
				packet->buf = (void *)(buf + sizeof(CdxMuxerPacketT));
				if(msg_queue_put(p_recorder_ctx->aud_empty_queue, (INT32U)buf) != 0){
					free(buf);
				}
			}
		}
	}

	if(p_recorder_ctx->vid_empty_queue && !msg_queue_get_used_count(p_recorder_ctx->vid_empty_queue)) {
		action_recorder_queue_exit(p_recorder_ctx);
		PRINTF("creat empty buf for video queue fail!1\n");
		return -1;
	}
	if(p_recorder_ctx->aud_empty_queue && !msg_queue_get_used_count(p_recorder_ctx->aud_empty_queue)) {
		action_recorder_queue_exit(p_recorder_ctx);
		PRINTF("creat empty buf for audio queue fail!1\n");
		return -1;
	}

	return 0;
}

static int action_recorder_queue_exit(struct recorder_context_t *p_recorder_ctx)
{
	INT32U buf = 0;
	int i;
	CdxMuxerPacketT *packet = NULL;

	if((p_recorder_ctx->vid_empty_queue)) {
		for(i = 0; i < VID_ENC_MGR_SIZE; i++) {
			if((buf = msg_queue_get(p_recorder_ctx->vid_empty_queue)) != 0) {
				free((void *)buf);
			}
		}
		msg_queue_destory(p_recorder_ctx->vid_empty_queue);
		p_recorder_ctx->vid_empty_queue = NULL;	  
	}

	if(p_recorder_ctx->aud_empty_queue) {
		for(i = 0; i < AUD_ENC_MGR_SIZE; i++) {
			if((buf = msg_queue_get(p_recorder_ctx->aud_empty_queue)) != 0) {
				free((void *)buf);
			}
		}
		msg_queue_destory(p_recorder_ctx->aud_empty_queue);
		p_recorder_ctx->aud_empty_queue = NULL;	
	}

	if((p_recorder_ctx->enc_ready_queue)) {
		for(i = 0; i < VID_ENC_MGR_SIZE + AUD_ENC_MGR_SIZE; i++) {
			if((buf = msg_queue_get(p_recorder_ctx->enc_ready_queue)) != 0) {			
				packet = (CdxMuxerPacketT *)buf;
				if(packet->is_video) {
					if(packet->buf) free(packet->buf);
				}
				free((void *)buf);
			}
		}
		msg_queue_destory(p_recorder_ctx->enc_ready_queue);
		p_recorder_ctx->enc_ready_queue = NULL;	
	}	

	return 0;
}

static inline int vid_queue_get(void)
{
	INT32U ret = 0;

	if(gp_recorder_ctx->vid_empty_queue)
		return msg_queue_get(gp_recorder_ctx->vid_empty_queue);

	return ret;
}

static inline int vid_queue_put(void *packet)
{
	INT32U ret = -1;

	if(gp_recorder_ctx->vid_empty_queue)
		return msg_queue_put(gp_recorder_ctx->vid_empty_queue, (INT32U)packet);

	return ret;
}

static inline int aud_queue_get(void)
{
	INT32U ret = 0;

	if(gp_recorder_ctx->aud_empty_queue)
		return msg_queue_get(gp_recorder_ctx->aud_empty_queue);

	return ret;
}

static inline int aud_queue_put(void *packet)
{
	INT32U ret = -1;

	if(gp_recorder_ctx->aud_empty_queue)
		return msg_queue_put(gp_recorder_ctx->aud_empty_queue, (INT32U)packet);

	return ret;
}

static inline int  enc_ready_queue_get(void)
{
	INT32U ret = 0;

	if(gp_recorder_ctx->enc_ready_queue)
		return msg_queue_get(gp_recorder_ctx->enc_ready_queue);

	return ret;
}

static inline int  enc_ready_queue_put(void *packet)
{
	INT32U ret = -1;

	if(gp_recorder_ctx->enc_ready_queue)
		return msg_queue_put(gp_recorder_ctx->enc_ready_queue, (INT32U)packet);

	return ret;
}

static inline void enc_queues_post(void)
{
	if(gp_recorder_ctx->vid_empty_queue) sem_post(&gp_recorder_ctx->vid_empty_queue->wait);
	if(gp_recorder_ctx->aud_empty_queue) sem_post(&gp_recorder_ctx->aud_empty_queue->wait);
	if(gp_recorder_ctx->enc_ready_queue) sem_post(&gp_recorder_ctx->enc_ready_queue->wait);
}

static int enc_callback(int cmd, void *arg)
{
	static char *mp4Name = NULL;

	switch(cmd) {
		case ENC_MUXER_GET_LOOP_TIME:
			return (int)900;
			break;
		case ENC_MUXER_GET_NEXT_NAME:
			if(mp4Name) {
				PRINTF("memory may be leak\n");
			}
			mp4Name = (char *)malloc(MAX_NAME_LENGTH);
			if(!mp4Name) {
				PRINTF("malloc mp4 name buf error\n");
				return 0;
			}
			memset(mp4Name, 0, MAX_NAME_LENGTH);
			if(user_file_name_generate(VIDEO_FILE_TYPE, mp4Name) == 0){
				PRINTF("get file name:%s\n", mp4Name);
				return (int)mp4Name;
			}else {
				free(mp4Name);
				mp4Name = NULL;
			}			
			break;
		case ENC_MUXER_PUT_NEXT_NAME:
			free(mp4Name);
			mp4Name = NULL;
			break;
			
		case ENC_MUXER_EXIT_MUXER_OK:
			if(!user_file_list_update((const char *)arg)) {
	//			printf("\n===%s():%d- video recoder file list update success=== \n",__func__,__LINE__);
			}else{
				printf("\n===%s():%d- video recoder file list update fail=== \n",__func__,__LINE__);
				user_folder_list_printf();	
			}				
			break;

		case ENC_MUXER_EXIT_FINALLY:
			if(!user_file_list_update((const char *)arg)) {
	//			printf("\n===%s():%d- video recoder file list update success=== \n",__func__,__LINE__);
			}else{
				printf("\n===%s():%d- video recoder file list update fail=== \n",__func__,__LINE__);
				user_folder_list_printf();	
			}		

			break;
			
		default:
			break;

	}

	return 0;
}

static int action_recorder_write_yuv(unsigned char *buf, int size, long long pts)
{	
	long long tmp_pts = 0;
	static int pre_pts = 0;
	static int cur_pts = 0;
	
	if(!gp_recorder_ctx || !gp_recorder_ctx->recorder_initial) {
		PRINTF("%s:recorder is not initialized\n", __func__);
		return -1;
	}

	if(!gp_recorder_ctx->init_tm_pts) {
		gp_recorder_ctx->init_tm_pts = pts;
		gp_recorder_ctx->coding_flag = 1;
	}
	
	else if(pts <= gp_recorder_ctx->init_tm_pts) return 0;

	tmp_pts = pts - gp_recorder_ctx->init_tm_pts;
	if(gp_recorder_ctx->recorder_timeLapse) {
		cur_pts = (int)(tmp_pts%(gp_recorder_ctx->recorder_timeLapse*1000));
		if(tmp_pts == 0) {
			gp_recorder_ctx->videoPts = 0;
			pre_pts = 0;
			cur_pts = 0;			
		} else {
			if(pre_pts > cur_pts) {
				gp_recorder_ctx->videoPts += 33;
			}else {
				pre_pts = cur_pts;
				return 1;
			}
			pre_pts = cur_pts;
		}
	}else {
		gp_recorder_ctx->videoPts = tmp_pts;
	}
	return video_encode_write_yuv(buf, size, gp_recorder_ctx->videoPts);
}

static int enc_card_detect(void)
{
	return 1;
}

static int _action_recorder_start(struct encode_config *p_encode_cfg, char *p_recorder_file_path)
{
	video_encode_config *p_video_cfg;
	audio_encode_config *p_audio_cfg;
	int ret = 0;

	memcpy(&gp_recorder_ctx->encode_cfg, p_encode_cfg, sizeof(struct encode_config));
	ret = action_recorder_queue_int(gp_recorder_ctx);
	if(ret != 0) {
		free(gp_recorder_ctx);
		gp_recorder_ctx = NULL;
		return -1;
	}

	p_video_cfg = gp_recorder_ctx->encode_cfg.video_cfg;
	if(p_video_cfg) {
		if(!p_video_cfg->video_queue_get) p_video_cfg->video_queue_get = vid_queue_get;
		if(!p_video_cfg->video_queue_put) p_video_cfg->video_queue_put = vid_queue_put;
	}

	p_audio_cfg = gp_recorder_ctx->encode_cfg.audio_cfg;
	if(p_audio_cfg) {
		gp_recorder_ctx->audio_bytes_per_ms = p_audio_cfg->in_chan*p_audio_cfg->sampler_bits/8*p_audio_cfg->in_sample_rate/1000;
		if(!p_audio_cfg->audio_queue_get) p_audio_cfg->audio_queue_get = aud_queue_get;
		if(!p_audio_cfg->audio_queue_put) p_audio_cfg->audio_queue_put = aud_queue_put;
	}

	gp_recorder_ctx->encode_cfg.enc_queue_get	 = enc_ready_queue_get;
	gp_recorder_ctx->encode_cfg.enc_queue_put	 = enc_ready_queue_put;
	gp_recorder_ctx->encode_cfg.enc_queue_post	 = enc_queues_post;
	gp_recorder_ctx->encode_cfg.device_is_online = enc_card_detect;	
	gp_recorder_ctx->encode_cfg.encode_callback  = enc_callback;

	ret = video_encode_init(&gp_recorder_ctx->encode_cfg, p_recorder_file_path);
	if(ret) {
		PRINTF("%s:init video encode fail\n", __func__);
		action_recorder_queue_exit(gp_recorder_ctx);
		free(gp_recorder_ctx);
		gp_recorder_ctx = NULL;
		return -1;
	}

	gp_recorder_ctx->init_tm_pts      = 0;
	gp_recorder_ctx->coding_flag      = 0;
	gp_recorder_ctx->videoPts         = 0;
	gp_recorder_ctx->audioPts         = 0;
	gp_recorder_ctx->recorder_initial = 1;
	gp_recorder_ctx->running_flag     = 1;
	gp_recorder_ctx->state = ACTION_STATE_INIT;

	return ret;
}

static int _action_recorder_stop(void)
{
	if(!gp_recorder_ctx || !gp_recorder_ctx->recorder_initial) {
		PRINTF("%s:recorder is not initialized\n", __func__);
		return -1;
	}

	gp_recorder_ctx->recorder_initial = 0;
	msg_queue_type_set(gp_recorder_ctx->vid_empty_queue, MSG_TYPE_NONBLOCK);
	msg_queue_type_set(gp_recorder_ctx->enc_ready_queue, MSG_TYPE_NONBLOCK);

	video_encode_uninit();
	action_recorder_queue_exit(gp_recorder_ctx);

	gp_recorder_ctx->init_tm_pts        = 0;
	gp_recorder_ctx->coding_flag        = 0;
	gp_recorder_ctx->videoPts           = 0;
	gp_recorder_ctx->audioPts           = 0;	
	gp_recorder_ctx->audio_bytes_per_ms = 0;	

	sync();
	PRINTF("%s\n", __func__);
	return 0;
}

static int recorder_thumb_save(struct frame_enc_outinfo_st *p_frame_encode_out, const char *name)
{
	FILE *fp;
	int ret;

	fp = user_file_open(name,"wb");
	if(NULL == fp){
		PRINTF("%s():open %s file fail\n",__func__,name);
		return -1;
	}
	user_file_write(p_frame_encode_out->p_data0, p_frame_encode_out->size0, 1, fp);
	ret = user_file_close(name, fp);
	ret = (ret?-1:0);
	printf("save thumb file %s\n",name);
	
	return 0;
}
static int  _action_recorder_thumb_uninit(void)
{
	struct thumb_obj_t *video_thumb = &gp_recorder_ctx->video_thumb;

	sem_destroy(&gp_recorder_ctx->video_thumb.wait);
	frame_encode_destroy_obj(video_thumb->jpgenc_ctx, video_thumb->frame_obj);
	frame_encode_put_context(video_thumb->jpgenc_ctx);
	if(gp_recorder_ctx->video_thumb.thumb_msg)
		msg_queue_destory(gp_recorder_ctx->video_thumb.thumb_msg);

	return 0;
}
static int _action_recorder_thumb_init(void *p_param)
{
	struct frame_enc_config_st      frame_encode_config;
	char *name = (char*)p_param;
	char dir[64],basename[64],full_name[128];
	
	gp_recorder_ctx->video_thumb.jpgenc_ctx = frame_encode_get_context(FRM_ENCODE_JPEG);
	if(!gp_recorder_ctx->video_thumb.jpgenc_ctx) {
	   PRINTF("%s:get jpg frame encode context error\n", __func__);
	   return -1;
	}

	gp_recorder_ctx->video_thumb.frame_obj = frame_encode_create_obj(gp_recorder_ctx->video_thumb.jpgenc_ctx);
	if(!gp_recorder_ctx->video_thumb.frame_obj) {
		PRINTF("%s:create frame object error\n", __func__);
		return -1;
	}
	gp_recorder_ctx->video_thumb.thumb_msg = msg_queue_create(4, MSG_TYPE_BLOCK);
	if(!gp_recorder_ctx->video_thumb.thumb_msg) {
		PRINTF("%s:msg_queue_create error\n", __func__);
		_action_recorder_thumb_uninit();
		return -1;
	}		

	memset(&frame_encode_config, 0, sizeof(struct frame_enc_config_st));
	frame_encode_config.input_width = gp_recorder_ctx->p_camera_dev->width;
	frame_encode_config.input_height = ALIGN(gp_recorder_ctx->p_camera_dev->height,16);
	frame_encode_config.output_width = VID_THUMB_WIDTH<<1;
	frame_encode_config.output_height = VID_THUMB_HEIGHT<<1;

	frame_encode_config.spec_param.jpeg_spec.thumb_enable = 1;
	frame_encode_config.spec_param.jpeg_spec.thumb_width  = VID_THUMB_WIDTH;
	frame_encode_config.spec_param.jpeg_spec.thumb_height = VID_THUMB_HEIGHT;	
	frame_encode_config.spec_param.jpeg_spec.vbv_size = 1*1024*1024;

	frame_encode_config_obj(gp_recorder_ctx->video_thumb.jpgenc_ctx, gp_recorder_ctx->video_thumb.frame_obj, &frame_encode_config);
	_file_name_split(name,dir,basename);
	memset(full_name, 0, sizeof(full_name));
	user_file_name_suffix_chang(basename,(const char*)PHOTO_FILE_SUFF);
	strncpy(full_name, VIDEO_THUMB_FOLDER, strlen((char*)VIDEO_THUMB_FOLDER));
	strncat(full_name, basename, strlen(basename));
	memcpy(gp_recorder_ctx->video_thumb.name, full_name, sizeof(full_name));
	gp_recorder_ctx->video_thumb.capture= 1;
	gp_recorder_ctx->video_thumb.runing	= 1;
	sem_init(&gp_recorder_ctx->video_thumb.wait,0,0);

	return 0;
}

static void *recorder_thumb_routine(void *p_param)
{
	frame_enc_outinfo_st frm_out_info;
	struct thumb_obj_t *video_thumb = &gp_recorder_ctx->video_thumb;
	unsigned int yuvbuf;
	int ret;

	while(video_thumb->runing) {
		yuvbuf = msg_queue_get(video_thumb->thumb_msg);
		ret = frame_encode_input_a_frame_by_phy(video_thumb->jpgenc_ctx, video_thumb->frame_obj, (int)yuvbuf);
		if(ret != 0) {
			PRINTF("%s:encode one frame error\n", __func__);
			sem_post(&video_thumb->wait);
			goto EncEnd;
		}	
		video_thumb->capture = 0;
		sem_post(&video_thumb->wait);
		ret = frame_encode_get_thumbnai_a_frame(video_thumb->jpgenc_ctx, video_thumb->frame_obj, &frm_out_info);
		if(ret != 0) {
			PRINTF("%s:get a encode frame error\n", __func__);
			sem_post(&video_thumb->wait);
			goto EncEnd;
		}
		recorder_thumb_save(&frm_out_info,video_thumb->name);
		ret = frame_encode_get_a_frame(video_thumb->jpgenc_ctx, video_thumb->frame_obj, &frm_out_info);
		if(ret != 0) {
			PRINTF("%s:get a encode frame error\n", __func__);
			sem_post(&video_thumb->wait);
			goto EncEnd;
		}
		frame_encode_put_a_frame(video_thumb->jpgenc_ctx, video_thumb->frame_obj, &frm_out_info);
EncEnd:
		pthread_exit("exit thumb thread");
	}

	return NULL;
}

static void *write_stream_routine(void *p_param)
{	
	struct camera_data *p_camera_dev = gp_recorder_ctx->p_camera_dev;
	long long pts = 0;
	int k = 0;
	
	while(gp_recorder_ctx->running_flag) {
		p_camera_dev->opr->GetFrame(p_camera_dev);
		if((gp_recorder_ctx->p_camera_dev->fps > 30) && (k == 0)){
			k = !k;
			continue;
		}
		
		if(gp_recorder_ctx->video_thumb.capture){
			msg_queue_put(gp_recorder_ctx->video_thumb.thumb_msg,p_camera_dev->phy_buf.m.offset);
			sem_wait(&gp_recorder_ctx->video_thumb.wait);
		}
		pts = (long long)(p_camera_dev->phy_buf.timestamp.tv_sec*1000LL + p_camera_dev->phy_buf.timestamp.tv_usec/1000LL);
		action_recorder_write_yuv((unsigned char *)(p_camera_dev->phy_buf.m.offset), p_camera_dev->phy_buf.index, pts);	
	}

	return NULL;
}

static int action_recorder_start(void)
{
	int ret = 0;
	int camera_id;
	int enc_width  = 1920;
	int enc_height = 1080;
	struct encode_config encode_cfg;
	video_encode_config video_cfg;
	char mp4Name[MAX_NAME_LENGTH] = {0};

	if(!gp_recorder_ctx) {
		gp_recorder_ctx = (struct recorder_context_t *)malloc(sizeof(struct recorder_context_t));
		if(!gp_recorder_ctx) {
			PRINTF("%s:malloc recorder_context_t pointer\n", __func__);
			ret = -1;
			goto err_init;
		}
		memset(gp_recorder_ctx, 0, sizeof(struct recorder_context_t));
	}else {
		PRINTF("%s:recorder maybe started already!\n", __func__);
		goto err_exit;
	}
	
	camera_id = image_trans_camera_id_get();
	gp_recorder_ctx->p_camera_dev = camera_get_video_device(camera_id);
	if(!gp_recorder_ctx->p_camera_dev) {
		PRINTF("%s:get camera device error\n", __func__);
		ret = -1;
		goto err_init;
	}
	if(disk_space_is_full())
	{
		PRINTF("card is not online or off-space!\n");
		ret = -1;
		goto err_init;
	}
	if(gp_recorder_ctx->p_camera_dev->width > 1280)
	{
		enc_width = 1920;
		enc_height = 1080;
	}else{
		enc_width = 1280;
		enc_height = 720;
	}

	memset(&encode_cfg, 0x00, sizeof(struct encode_config));
	memset(&video_cfg, 0x00, sizeof(video_encode_config));
	video_cfg.type		  = VIDEO_ENCODE_H264;
	video_cfg.frame_rate  = 30;
	video_cfg.bit_rate	  = 13*1024*1024;
	video_cfg.out_height  = enc_height;//ALIGN(enc_height, 16);
	video_cfg.out_width   = enc_width;
	video_cfg.src_height  = ALIGN(gp_recorder_ctx->p_camera_dev->height, 16);
	video_cfg.src_width   = gp_recorder_ctx->p_camera_dev->width;
	video_cfg.quality	  = 70;//video_enc.m_quality;
	video_cfg.use_phy_buf = 1; /* use physical address */

	encode_cfg.video_cfg = &video_cfg;

	/* cedarx encode init */
	ret = user_file_name_generate(VIDEO_FILE_TYPE, mp4Name);
	if(ret != 0){
		PRINTF("%s:generate mp4 file failed!\n", __func__);
		goto err_init;
	}
	PRINTF("get video file name:%s\n", mp4Name);
	ret = _action_recorder_start(&encode_cfg, mp4Name);
	if(ret) {
		PRINTF("%s:action_recorder_start error\n", __func__);
		goto err_init;
	}
	ret = _action_recorder_thumb_init(mp4Name);
	if(ret) {
		PRINTF("%s:_action_recorder_thumb_init error\n", __func__);
		goto err_start;
	}
	ret = pthread_create(&gp_recorder_ctx->thread_id, NULL, write_stream_routine, NULL);
	if(ret != 0) {
		PRINTF("%s: create thread failed!\n", __func__);
		goto err_start;
	}
	ret = pthread_create(&gp_recorder_ctx->thumb_thread_id, NULL, recorder_thumb_routine, NULL);
	if(ret != 0) {
		PRINTF("%s: create thread failed!\n", __func__);
		goto err_start;
	}			
	gp_recorder_ctx->state = ACTION_STATE_RUNNING;

	return 0;
err_start:
	_action_recorder_stop();
err_init:
	free(gp_recorder_ctx);
	gp_recorder_ctx = NULL;
err_exit:	
	return ret;
}

static int action_recorder_stop(void)
{
	int ret;
	
	if(!gp_recorder_ctx) {
		PRINTF("%s:recorder is not init\n", __func__);
		return -1;
	}
	gp_recorder_ctx->running_flag = 0;
	gp_recorder_ctx->video_thumb.runing = 0;
	pthread_join(gp_recorder_ctx->thread_id, NULL);
	pthread_join(gp_recorder_ctx->thumb_thread_id, NULL);
	gp_recorder_ctx->state = ACTION_STATE_UNINIT;
	PRINTF("%s:stop recorder\n", __func__);
	_action_recorder_thumb_uninit();

	ret = _action_recorder_stop();
	free(gp_recorder_ctx);
	gp_recorder_ctx = NULL;

	return ret;
}

static void* action_run_recorder(struct action_object_t *p_action_obj, void *p_param)
{
	int ret = 0;
	int loop = 1;

	PRINTF("%s\n", __func__);

	ret = action_recorder_start();
	if(ret != 0) {
		return NULL;
	}

	while(loop) {
 /* iRet = GetInputEvent(&tInputEvent);*/		
	}
	action_recorder_stop();
	PRINTF("%s:exiting ok\n", __func__);

	return NULL;
}
static void* action_run_asyn_recorder(struct action_object_t *p_action_obj, void *p_param)
{
	int ret  = 0;
	PRINTF("%s\n", __func__);
	ret = action_recorder_start();
	return (void *)ret;
}

static void* action_run_exit_recorder(struct action_object_t *p_action_obj, void *p_param)
{
	PRINTF("%s\n", __func__);
	action_recorder_stop();

	return NULL;
}

static int action_get_state_recorder(struct action_object_t *p_action_obj, ACTION_STATE_E *p_state)
{

	if(!gp_recorder_ctx) {
		*p_state = ACTION_STATE_UNINIT;
		return 0;
	}
	*p_state = gp_recorder_ctx->state;

	return 0;
}

struct action_object_t action_object_recorder = {
	.name              = "action recorder", 
	.action_run        = action_run_recorder,
	.action_run_asyn   = action_run_asyn_recorder,
	.action_run_exit   = action_run_exit_recorder,
	.action_get_state  = action_get_state_recorder,
};

int action_object_recorder_init(void)
{
	int ret;

	ret = register_action_object(&action_object_recorder);
	if(!ret)
		ret = recorder_cmd_init();

	return ret;
}
