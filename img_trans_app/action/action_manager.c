#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <action/action_manager.h>

static struct action_object_t *gp_action_object_head;


int register_action_object(struct action_object_t *p_action_object)
{
	struct action_object_t *p_tmp;

	if (!gp_action_object_head)
	{
		gp_action_object_head   = p_action_object;
		p_action_object->next = NULL;
	}
	else
	{
		p_tmp = gp_action_object_head;
		while (p_tmp->next)
		{
			p_tmp = p_tmp->next;
		}
		p_tmp->next	  = p_action_object;
		p_action_object->next = NULL;
	}

	return 0;
}

void show_action_object(void)
{
	int i = 0;
	struct action_object_t *p_tmp = gp_action_object_head;

	while (p_tmp)
	{
		PRINTF("%02d %s\n", i++, p_tmp->name);
		p_tmp = p_tmp->next;
	}
}


struct action_object_t * action_object_get(const char *p_name)
{
	struct action_object_t *p_tmp = gp_action_object_head;
	
	while (p_tmp)
	{
		if (strcmp(p_tmp->name, p_name) == 0)
		{
			return p_tmp;
		}
		p_tmp = p_tmp->next;
	}
	return NULL;
}



void* action_run(struct action_object_t *p_action_obj, void *p_param)
{
	if(!p_action_obj) {
		PRINTF("%s:invalid action obj pointer\r\n", __func__);
		return NULL;
	}

	if(p_action_obj->action_run)
		return p_action_obj->action_run(p_action_obj, p_param);

	return NULL;
}

void* action_run_asyn(struct action_object_t *p_action_obj, void *p_param)
{
	if(!p_action_obj) {
		PRINTF("%s:invalid action obj pointer\r\n", __func__);
		return NULL;
	}

	if(p_action_obj->action_run_asyn)
		return p_action_obj->action_run_asyn(p_action_obj, p_param);

	return NULL;
}

void* action_run_exit(struct action_object_t *p_action_obj, void *p_param)
{
	if(!p_action_obj) {
		PRINTF("%s:invalid action obj pointer\r\n", __func__);
		return NULL;
	}

	if(p_action_obj->action_run_exit)
		return p_action_obj->action_run_exit(p_action_obj, p_param);

	return NULL;
}


int action_get_event(struct action_object_t *p_action_obj, void *p_cmd)
{
	if(!p_action_obj) {
		PRINTF("%s:invalid action obj pointer\r\n", __func__);
		return -1;
	}

	if(p_action_obj->action_get_event)
		return p_action_obj->action_get_event(p_action_obj, p_cmd);

	return -1;
}

int action_put_event(struct action_object_t *p_action_obj, void *p_cmd)
{
	if(!p_action_obj) {
		PRINTF("%s:invalid action obj pointer\r\n", __func__);
		return -1;
	}

	if(p_action_obj->action_put_event)
		return p_action_obj->action_put_event(p_action_obj, p_cmd);

	return -1;
}

int action_get_state(struct action_object_t *p_action_obj, ACTION_STATE_E *p_state)
{
	if(!p_action_obj) {
		PRINTF("%s:invalid action obj pointer\r\n", __func__);
		return -1;
	}

	if(p_action_obj->action_get_state)
		return p_action_obj->action_get_state(p_action_obj, p_state);

	return -1;
}

extern int action_object_recorder_init(void);
extern int action_object_image_trans_init(void);
extern int action_object_take_photos_init(void);
extern int action_object_image_algorithm_init(void);

int action_object_init(void)
{
	int iError = 0;
	
	iError = action_object_recorder_init();
	iError |= action_object_image_trans_init();
	iError |= action_object_take_photos_init();
	iError |= action_object_image_algorithm_init();
	
	return iError;
}


