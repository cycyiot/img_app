#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <camera/camera.h>
#include <action/action_manager.h>
#include <action/action_image_algorithm.h>

struct image_algorithm_context_t {
	struct camera_data   *p_camera_dev;
	struct follow_me_parameter fm_param;
	pthread_t             thread_id;
	int                   running_flag;
	sem_t                 mutex;
};
struct image_algorithm_context_t *gp_image_algorithm_ctx = NULL;

//#define __IMG_ALG_TEST__

#ifdef __IMG_ALG_TEST__	
static int image_algorithm_save_yuv(void *buf, unsigned int width, unsigned int height, unsigned int cnt)
{
	char path[64];
	FILE *fp = NULL;
	unsigned int size = 0;
	
	snprintf(path, sizeof(path), "/mnt/Camera_%dX%d_%d.yuv", width, height, cnt);
	fp = fopen(path, "wb");
	if(fp == NULL){
		printf("[%s,%d]:open file err\r\n", __func__, __LINE__);
		return -1;
	}

	size = (ALIGN(width,16) * ALIGN(height,16) * 3) / 2;
	fwrite(buf, size, 1, fp);
	fclose(fp);

	return 0;
}
#endif

static void *image_algorithm_handle_routine(void *p_param)
{	
	struct camera_data *p_camera_dev = gp_image_algorithm_ctx->p_camera_dev;
	struct follow_me_parameter *p_fm_param = NULL;
	struct follow_me_parameter fm_notify_param;
	long long pts_cur = 0, pts_last = 0, pts_diff = 0;
#ifdef __IMG_ALG_TEST__
	unsigned int frame_size = 0;
    void *subChnFrame = NULL;
	unsigned int frame_cnt = 0;
#endif

	 while(1){
		if(gp_image_algorithm_ctx->running_flag == 2){
			sem_wait(&gp_image_algorithm_ctx->mutex);
			memcpy((void *)&gp_image_algorithm_ctx->fm_param, (void *)p_param, sizeof(struct follow_me_parameter));
			p_fm_param = &gp_image_algorithm_ctx->fm_param;
			gp_image_algorithm_ctx->running_flag = 1;
		}else if(gp_image_algorithm_ctx->running_flag == 0){
			break;
		}
		
		p_camera_dev->opr->GetFrame(p_camera_dev);		
		pts_cur = (long long)(p_camera_dev->phy_buf.timestamp.tv_sec*1000LL + p_camera_dev->phy_buf.timestamp.tv_usec/1000LL);
		if(pts_last == 0){
			pts_last = pts_cur;
		}
		
#ifdef __IMG_ALG_TEST__
		frame_cnt++;
		frame_size = ALIGN(p_camera_dev->width, 16) * ALIGN(p_camera_dev->height, 16) * 3 / 2;
		subChnFrame = p_camera_dev->virtual_buf[p_camera_dev->w_idx] + ALIGN(frame_size, 4096);
		image_algorithm_save_yuv(subChnFrame,
								 FM_CAP_WIDTH,
								 FM_CAP_HEIGHT,
								 frame_cnt);
#endif	
		//printf("start=(%d,%d)\t end=(%d,%d)\n", fm_param->x0, fm_param->y0, fm_param->x1, fm_param->y1);
		// T.B.D:algorithm handle
		
		pts_diff = pts_cur - pts_last;
		if(pts_diff > 500)
		{
			pts_last = pts_cur;
			fm_notify_param.ret = 0; // 0:success;1:failed
			fm_notify_param.x0 = p_fm_param->x0;
			fm_notify_param.y0 = p_fm_param->y0;
			fm_notify_param.x1 = p_fm_param->x1;
			fm_notify_param.y1 = p_fm_param->y1;
			printf("start=(%d,%d)\t end=(%d,%d)\n", fm_notify_param.x0, fm_notify_param.y0, fm_notify_param.x1, fm_notify_param.y1);
			follow_me_state_notify(&fm_notify_param);
		}
	}

	return NULL;
}

int action_image_algorithm_stop(void)
{
	if(!gp_image_algorithm_ctx) {
		PRINTF("%s:image_algorithm is not init\n", __func__);
		return -1;
	}
	
	gp_image_algorithm_ctx->running_flag = 0;
	pthread_join(gp_image_algorithm_ctx->thread_id, NULL);

	sem_destroy(&gp_image_algorithm_ctx->mutex);
	
	if(gp_image_algorithm_ctx){
		free(gp_image_algorithm_ctx);
		gp_image_algorithm_ctx = NULL;
	}

	return 0;
}

int action_image_algorithm_start(void *param)
{
	int ret;
	
	if(!gp_image_algorithm_ctx) {
		gp_image_algorithm_ctx = (struct image_algorithm_context_t *)malloc(sizeof(struct image_algorithm_context_t));
		if(!gp_image_algorithm_ctx) {
			PRINTF("%s:malloc image_algorithm_context_t pointer\n", __func__);
			return -1;
		}
		memset(gp_image_algorithm_ctx, 0, sizeof(struct image_algorithm_context_t));

		/* camera init */
		gp_image_algorithm_ctx->p_camera_dev = camera_get_video_device(0);
		if(!gp_image_algorithm_ctx->p_camera_dev) {
			PRINTF("%s:get camera device error\n", __func__);
			return -1;
		}

		sem_init(&gp_image_algorithm_ctx->mutex, 0, 0);
		
		gp_image_algorithm_ctx->running_flag = 2;
		sem_post(&gp_image_algorithm_ctx->mutex);
		
		ret = pthread_create(&gp_image_algorithm_ctx->thread_id, NULL, image_algorithm_handle_routine, param);
		if(ret != 0) {
			PRINTF("%s: create thread failed!\n", __func__);
			return -1;
		}
	}else{
		gp_image_algorithm_ctx->running_flag = 2;
		sem_post(&gp_image_algorithm_ctx->mutex);
	}

	return 0;
}

static void* action_run_image_algorithm(struct action_object_t *p_action_obj, void *p_param)
{
	int ret = 0;

	PRINTF("%s\n", __func__);

	ret = action_image_algorithm_start(p_param);
	if(ret != 0) {
		return NULL;
	}

	return NULL;
}

static void* action_run_asyn_image_algorithm(struct action_object_t *p_action_obj, void *p_param)
{
	int ret  = 0;
	PRINTF("%s\n", __func__);
	ret = action_image_algorithm_start(p_param);
	return (void *)ret;
}

static void* action_run_exit_image_algorithm(struct action_object_t *p_action_obj, void *p_param)
{
	PRINTF("%s\n", __func__);
	action_image_algorithm_stop();

	return NULL;
}

struct action_object_t action_object_image_algorithm = {
	.name              = "action image algorithm", 
	.action_run        = action_run_image_algorithm,
	.action_run_asyn   = action_run_asyn_image_algorithm,
	.action_run_exit   = action_run_exit_image_algorithm,
};

int action_object_image_algorithm_init(void)
{
	int ret;
	
	ret = register_action_object(&action_object_image_algorithm);
	if(!ret)
		ret = image_algorithm_cmd_init();
	return ret;
}
