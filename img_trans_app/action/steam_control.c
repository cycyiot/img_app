
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <action/action_manager.h>
#include <net/cmd_handle.h>
#include <unistd.h>
#include <action/action_recorder.h>
#include <action/steam_control.h>
#include <errno.h>
#include <syslog.h>
#include <time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/un.h>
#include <action/action_preview.h>
#include <led/led.h>

#define    tof(str, arg...)  do{\
           time_t timep;\
           struct tm *p;\
           char time_str[40];\
           memset(time_str,'\0',40);\
           time(&timep);\
           p=localtime(&timep);\
           sprintf(time_str,"[%d.%02d.%02d %02d:%02d:%02d]",(1900+p->tm_year),(1+p->tm_mon),p->tm_mday,p->tm_hour,p->tm_min,p->tm_sec);\
           FILE *debug_fp = fopen("/tmp/tof", "a");\
           if (NULL != debug_fp){\
           fprintf(debug_fp, "%s L%d in %s, ",time_str, __LINE__, __FILE__);\
           fprintf(debug_fp, str, ##arg);\
           fflush(debug_fp);\
           fclose(debug_fp);\
           }\
}while(0)
#define STEAM_CONTROL_SERVER  "/tmp/.send_steam_socket"
#define PILOT_AND_IMG_U       "/tmp/.send_img_socket"  


int server_sock;
struct data_control
{
  int id;
  int vel;
};
int make_local_server_fd()
{
    int val;
 
    unlink(PILOT_AND_IMG_U);

    if(-1 == (server_sock = socket(AF_UNIX, SOCK_DGRAM, 0)) ) 
    {
        tof("Create socket fail. Reason:%s\n", strerror(errno));
        return -1;
    }
    struct sockaddr_un server_addr;
    memset (&(server_addr), '\0', sizeof(struct sockaddr_un));
    server_addr.sun_family = AF_UNIX;  
    memcpy(server_addr.sun_path, PILOT_AND_IMG_U, strlen(PILOT_AND_IMG_U)); 
    
    val = 1;
    
    if(setsockopt(server_sock, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val)) < 0) 
    {    
        close(server_sock);
        return -1;
    }
   
    if(-1 == bind(server_sock, (struct sockaddr *)&server_addr, sizeof(struct sockaddr_un))) 
    {    
        tof("Bind socket fail. Reason:%s\n", strerror(errno));
        close(server_sock);
        return -1;
    }

    return 0;    
}


int hal_linux_create_thread()
{

#if 1
    int rv;
    pthread_t task;
	
	led_blink(0xff);
	rv = pthread_create(&task, NULL, led_control_blink,NULL);
	if (rv != 0) {
		if (rv == EPERM) {
			tof("warning: unable to start");
			rv = pthread_create(&task, NULL, led_control_blink, NULL);

			if (rv != 0) {
				tof("platform_create_thread: failed to create thread");
				return (rv < 0) ? rv : -rv;
			}
		}else{
			return rv;
		}
	}
#endif

  	
	return 0;
}

static int img_send_to_pilot(int control,int vel){	 
	int connect_fd;
	int ret;
	
	struct data_control send_buff;
	
	static struct sockaddr_un srv_addr;	 	
	// creat unix socket	 	
	connect_fd=socket(AF_UNIX,SOCK_DGRAM,0);	 	
	if(connect_fd<0){	 	
		tof("cannot creat socket");	 
		return -1;	 	
		}	
	srv_addr.sun_family=AF_UNIX;	 
	strcpy(srv_addr.sun_path,STEAM_CONTROL_SERVER);	 
	//connect server	
//	memset(send_buff,0,1024);	
	switch(control)
	{
	 case CMD_SAFE_MODE:
	     send_buff.id=CMD_SAFE_MODE;
	     send_buff.vel=vel;
		 action_image_trans_camera_switch(1);
	    break; 
	 case CMD_WING_PROTECTION:
	  	send_buff.id=CMD_WING_PROTECTION;
		send_buff.vel=vel;
		action_image_trans_camera_switch(0);
	  	break;
	 case CMD_CONTROL_YAW:
	 	send_buff.id=CMD_CONTROL_YAW;
		send_buff.vel=vel;
		break;
	 case CMD_CONTROL_ALT:
	 	send_buff.id=CMD_CONTROL_ALT;
		send_buff.vel=vel;
		break;
	case CMD_TOF_ALTHOLD:
		send_buff.id=CMD_TOF_ALTHOLD;
		send_buff.vel=vel;
		break;
	case CMD_TAKEOFF_ALT:
		send_buff.id=CMD_TAKEOFF_ALT;
		send_buff.vel=vel;
		break;
    case CMD_INFRARED_SHOOTING:
        send_buff.id=CMD_INFRARED_SHOOTING;
        send_buff.vel=vel;
	  default:
		break;
	}
	     
	ret=(int)sendto(connect_fd,&send_buff,sizeof(send_buff),0,(struct sockaddr*)&srv_addr,sizeof(srv_addr));	
	if (ret<0){	 
		tof("cannot connect server");	 	
		close(connect_fd);	 	
		return -1;	 	
		}	 	 
		 	
	close(connect_fd);	 	
	return 0;	
}

int img_recv_from_pilot()
{ 
	int ret;
	struct data_control rcv_buff;
	socklen_t addrlen; 
	struct sockaddr_un clt_addr;			
	addrlen=sizeof(clt_addr);


	ret=(int)recvfrom(server_sock,&rcv_buff,sizeof(rcv_buff),0,(struct sockaddr*)&clt_addr,&addrlen);
	if(ret>0)
      { 	

         switch(rcv_buff.vel)
         {
           case 0:
           return 0;
         	break;
           case 1:
            led_blink(1);
            break;
           default:
             break;
          }	
        
	}
  		
	return -1;
}
int img_recv_from_pilot_led()
{ 
	int ret;
	struct data_control rcv_buff;
	socklen_t addrlen; 
	struct sockaddr_un clt_addr;			
	addrlen=sizeof(clt_addr);

	ret=(int)recvfrom(server_sock,&rcv_buff,sizeof(rcv_buff),0,(struct sockaddr*)&clt_addr,&addrlen);
	if(ret>0)
      { 	

         switch(rcv_buff.vel)
         {
           case 1:
            led_blink(1);
            break;
           default:
             break;
          }	
        
	}
  		
	return 0;
}
void *led_control_blink(void *arg)
{

 img_recv_from_pilot_led();
       
 return ((void * )0);
}
int steam_control_action(struct cmd_param *cmdParam)
{
	int ret = -1;
	int param=0;
	if((!is_error(cmdParam->param)) && (json_object_get_type(cmdParam->param)==json_type_int)){
		param = json_object_get_int(cmdParam->param);
		ret=img_send_to_pilot(cmdParam->id,param);

	}
    switch(cmdParam->id)
    {
      case CMD_CONTROL_ALT:
	  case CMD_TOF_ALTHOLD:
	   ret=img_recv_from_pilot();
		break;
      default:
	  	break;
	}
	cmd_response(ret, cmdParam);
	return ret;
}



struct cmd steam_cmd[] = {
	{CMD_SAFE_MODE,         steam_control_action},
	{CMD_CONTROL_ALT,       steam_control_action},
	{CMD_WING_PROTECTION,   steam_control_action},
	{CMD_CONTROL_YAW,       steam_control_action},
	{CMD_TOF_ALTHOLD,       steam_control_action},
	{CMD_TAKEOFF_ALT,       steam_control_action},
	{CMD_INFRARED_SHOOTING, steam_control_action}

};

int steam_cmd_init(void)
{
    make_local_server_fd(); 
	return cmd_register(steam_cmd, ARRAY_SIZE(steam_cmd));
}




































