#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <action/action_manager.h>
#include <cmd.h>
#include <net/cmd_handle.h>
#include <system/sys_param.h>
#include <action/action_preview.h>
#include <ini/aw_ini_parser.h>
#include <net/wifi.h>

static int action_image_trans_camera_switch_cmd_handler(struct cmd_param *cmdParam)
{
	int ret = -1;
	int camera_num;

	if((is_error(cmdParam->param)) || (json_object_get_type(cmdParam->param) != json_type_int)){
		goto err_exit;
	}
	camera_num = json_object_get_int(cmdParam->param);
	ret = action_image_trans_camera_switch(camera_num);

err_exit:
	cmd_response(ret, cmdParam);
	return ret;
}

static int image_trans_resolution_set_cmd_handler(struct cmd_param *cmdParam)
{
	int width, height;
	struct json_object *jsonSubParamObj = NULL;
	int ret = -1;

	if(!is_error(cmdParam->param)){
		jsonSubParamObj = json_object_object_get(cmdParam->param, "width");
		if(json_object_get_type(jsonSubParamObj) == json_type_int){
			width = json_object_get_int(jsonSubParamObj);
			jsonSubParamObj = json_object_object_get(cmdParam->param, "height");
			if(json_object_get_type(jsonSubParamObj) == json_type_int){
				height = json_object_get_int(jsonSubParamObj);
				PRINTF("%s: width=%d, height=%d\n", __func__, width, height);
				if((width < IMAGE_TRANS_OUT_WIDTH_MIN) || (width > IMAGE_TRANS_OUT_WIDTH_MAX) || \
					(height < IMAGE_TRANS_OUT_HEIGHT_MIN) || (height > IMAGE_TRANS_OUT_HEIGHT_MAX)){
						PRINTF("unsupported format!\n");
						goto err_exit;
				}
				ret = image_trans_resolution_set(width, height);
			}
		}
	}
err_exit:
	cmd_response(ret, cmdParam);
	return ret;
}

struct cmd image_trans_cmd[] = {
	{CMD_VID_CAMERA_SWITCH, action_image_trans_camera_switch_cmd_handler},
	{CMD_PREVIEW_RESOLUTION_SET, image_trans_resolution_set_cmd_handler},
};

int preview_cmd_init(void)
{
	return cmd_register(image_trans_cmd, ARRAY_SIZE(image_trans_cmd));
}
