#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <action/action_manager.h>
#include <net/cmd_handle.h>
#include <unistd.h>
#include <action/action_recorder.h>

static struct action_object_t *p_action_obj_recorder;
int action_recorder_cmd_handler(struct cmd_param *cmdParam)
{
	int ret = -1;
	
	if(cmdParam->id == CMD_VID_ENC_START){
		ret = (int)p_action_obj_recorder->action_run_asyn(p_action_obj_recorder, NULL);
	}else if(cmdParam->id == CMD_VID_ENC_STOP){
		ret = (int)p_action_obj_recorder->action_run_exit(p_action_obj_recorder, NULL);
	}
	cmd_response(ret, cmdParam);
	return ret;
}

struct cmd recorder_cmd[] = {
	{CMD_VID_ENC_START, action_recorder_cmd_handler},
	{CMD_VID_ENC_STOP, action_recorder_cmd_handler},
};

int recorder_cmd_init(void)
{
	p_action_obj_recorder = action_object_get("action recorder");
	if(p_action_obj_recorder == NULL) {
		PRINTF("get action recorder error!\n");
		return -1;
	}
	return cmd_register(recorder_cmd, ARRAY_SIZE(recorder_cmd));
}
