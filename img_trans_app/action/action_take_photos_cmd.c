#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <action/action_manager.h>
#include <platform_cfg.h>
#include <net/cmd_handle.h>
#include <action/action_take_photos.h>

static struct action_object_t *p_action_object_take_photos;
struct cmd_param *cmdParam_steam=NULL;

int get_cmdParam_id(void)
{
 return cmdParam_steam->id;
}

int take_photos_handler(struct cmd_param *cmdParam)
{
	struct json_object *jsonSubParamObj;
	struct action_take_photos_param take_picture_param;
	int ret = -1;
	
	if(is_error(cmdParam->param))
	   {   //	goto err_exit;
           cmd_response(ret,cmdParam);
		   return ret;
		}
	
	cmdParam_steam=cmdParam;
	system("rm /tmp/photo/*.jpg");
	system("rm /tmp/photo/thumb/*.jpg");
	jsonSubParamObj = json_object_object_get(cmdParam->param, "num");
	if(json_object_get_type(jsonSubParamObj) == json_type_int){
		take_picture_param.num = json_object_get_int(jsonSubParamObj);
	}
	jsonSubParamObj = json_object_object_get(cmdParam->param, "delay");
	if(json_object_get_type(jsonSubParamObj) == json_type_int){
		take_picture_param.delay = json_object_get_int(jsonSubParamObj);
	}
	ret = (int)p_action_object_take_photos->action_run_asyn(p_action_object_take_photos, &take_picture_param);

//err_exit:
	//cmd_response(ret, cmdParam);
	return ret;
}
void cmd_response_takephoto(const char * name,int result,const char * thumb_name)
{
    if(cmdParam_steam->id == CMD_IMG_LOAD)
	cmd_response_steam(result,cmdParam_steam,name,thumb_name);
	else
    cmd_response(result,cmdParam_steam);
    return ;
}
struct cmd take_photos_cmd[] = {
	{CMD_VID_ENC_CAPTURE, take_photos_handler},
	{CMD_IMG_LOAD,take_photos_handler},
};

int take_photos_cmd_init(void)
{
	p_action_object_take_photos = action_object_get("action take photos");
	if(p_action_object_take_photos == NULL) {
		PRINTF("get action take photos error!\n");
		return -1;
	}
	return cmd_register(take_photos_cmd, ARRAY_SIZE(take_photos_cmd));
}
