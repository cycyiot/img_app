#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <action/action_image_algorithm.h>
#include <net/cmd_handle.h>
#include <system/sys_param.h>
#include <platform_cfg.h>

void follow_me_state_notify(struct follow_me_parameter *fm_param)
{
	struct json_object *jsonCfmPacketObj = NULL;
	struct json_object *jsonParamBattery;
	int len;

	jsonCfmPacketObj = json_object_new_object();
	json_object_object_add(jsonCfmPacketObj, "REPORT", json_object_new_int(RE_FOLLOW_ME));
	jsonParamBattery = json_object_new_object();
	json_object_object_add(jsonParamBattery, "RESULT", json_object_new_int(fm_param->ret));
	json_object_object_add(jsonParamBattery, "X0", json_object_new_int(fm_param->x0));
	json_object_object_add(jsonParamBattery, "Y0", json_object_new_int((int)fm_param->y0));	
	json_object_object_add(jsonParamBattery, "X1", json_object_new_int(fm_param->x1));
	json_object_object_add(jsonParamBattery, "Y1", json_object_new_int((int)fm_param->y1));	
	json_object_object_add(jsonCfmPacketObj, "PARAM", jsonParamBattery);

	PRINTF("%s\n", json_object_to_json_string(jsonCfmPacketObj));
	len = strlen(json_object_to_json_string(jsonCfmPacketObj)) + 1;		
	TcpClientSend(0, json_object_to_json_string(jsonCfmPacketObj), len);
	json_object_put(jsonCfmPacketObj);
}

static int image_algorithm_cmd_handler(struct cmd_param *cmdParam)
{
	int ret = -1;
	struct json_object *jsonSubParamObj;
	static struct follow_me_parameter fm_param;

	if(cmdParam->id == CMD_FOLLOW_START){
		if(is_error(cmdParam->param))
			return -1;
		memset(&fm_param, 0, sizeof(fm_param));
		jsonSubParamObj = json_object_object_get(cmdParam->param, "X0");
		if(json_object_get_type(jsonSubParamObj) == json_type_int){
			fm_param.x0 = json_object_get_int(jsonSubParamObj);
		}
		jsonSubParamObj = json_object_object_get(cmdParam->param, "Y0");
		if(json_object_get_type(jsonSubParamObj) == json_type_int){
			fm_param.y0 = json_object_get_int(jsonSubParamObj);
		}
		jsonSubParamObj = json_object_object_get(cmdParam->param, "X1");
		if(json_object_get_type(jsonSubParamObj) == json_type_int){
			fm_param.x1 = json_object_get_int(jsonSubParamObj);
		}
		jsonSubParamObj = json_object_object_get(cmdParam->param, "Y1");
		if(json_object_get_type(jsonSubParamObj) == json_type_int){
			fm_param.y1 = json_object_get_int(jsonSubParamObj);
		}
		ret = action_image_algorithm_start(&fm_param);
	}else if(cmdParam->id == CMD_FOLLOW_STOP){
		ret = action_image_algorithm_stop();
	}
	return ret;
}

struct cmd image_algorithm_cmd[] = {
	{CMD_FOLLOW_START, image_algorithm_cmd_handler},
	{CMD_FOLLOW_STOP, image_algorithm_cmd_handler},
};

int image_algorithm_cmd_init(void)
{
	return cmd_register(image_algorithm_cmd, ARRAY_SIZE(image_algorithm_cmd));
}
