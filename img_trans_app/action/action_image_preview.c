#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <action/action_manager.h>
#include <camera/camera.h>
#include <cmd.h>
#include <net/cmd_handle.h>
#include <system/event.h>
#include <led/led.h>
#include <system/sys_param.h>
#include <sys/mount.h>
#include <action/action_preview.h>
#include <ini/aw_ini_parser.h>
#include <net/wifi.h>

struct action_image_trans_context_t g_image_trans_ctx;

//#define __H264_FRAME_TYPE_TEST__

#ifdef __H264_FRAME_TYPE_TEST__
static int h264_frame_type_judge(unsigned char *buf)
{
	int nalu_type = 0;
	int frame_type = 0;
	
	if(buf == NULL){
		PRINTF("[%s:%d]:param error!\r\n", __func__, __LINE__);
		return 0;
	}

	nalu_type = (buf[4] & 0x1F);
	switch(nalu_type){
	case 0x05:
		PRINTF("[%s:%d]:I frame!\r\n", __func__, __LINE__);
		frame_type = 0x01;
		break;
	case 0x07:
		PRINTF("[%s:%d]:SPS!\r\n", __func__, __LINE__);
		frame_type = 0x02;
		break;
	case 0x08:
		PRINTF("[%s:%d]:PPS!\r\n", __func__, __LINE__);
		frame_type = 0x03;
		break;
	default:
		PRINTF("[%s:%d]:P frame!\r\n", __func__, __LINE__);
		frame_type = 0x04;
		break;
	}

	return frame_type;
}
#endif

static void image_trans_param_load(void)
{
	INI_VAL iniVal;

	iniVal.type = INI_VAL_TYPE_NUM;
	if(!get_ini_value("image_trans", "width", &iniVal)){
		g_image_trans_ctx.image_trans_packet.format.width = iniVal.val.num;
		if(!get_ini_value("image_trans", "height", &iniVal)){
			g_image_trans_ctx.image_trans_packet.format.height = iniVal.val.num;
			return;
		}
	}
	memset(&g_image_trans_ctx.image_trans_packet, 0, sizeof(g_image_trans_ctx.image_trans_packet));
	g_image_trans_ctx.image_trans_packet.format.width = IMAGE_TRANS_OUT_WIDTH;
	iniVal.val.num = IMAGE_TRANS_OUT_WIDTH;
	set_ini_value("image_trans", "width", &iniVal);
	g_image_trans_ctx.image_trans_packet.format.height = IMAGE_TRANS_OUT_HEIGHT;
	iniVal.val.num = IMAGE_TRANS_OUT_HEIGHT;
	set_ini_value("image_trans", "height", &iniVal);
}

static int image_trans_send_frame(void *fTo, int maxsize)
{
	frame_enc_outinfo_st *p_frm_out_info = &g_image_trans_ctx.image_trans_packet.frm_out_info;
	int cpy_size;

#ifdef __H264_FRAME_TYPE_TEST__
	h264_frame_type_judge(p_frm_out_info->p_data0);
#endif

	if(p_frm_out_info->size0 < maxsize) {
		memcpy(fTo, p_frm_out_info->p_data0, p_frm_out_info->size0);
		cpy_size = p_frm_out_info->size0;
	} else {
		cpy_size = 0;
		PRINTF("%s error! p_frm_out_info->size0 = %d!\r\n", __func__, p_frm_out_info->size0);
		goto exit;
	}
	if(p_frm_out_info->size1) {
		if((p_frm_out_info->size0 + p_frm_out_info->size1) < maxsize) {
			memcpy(fTo + p_frm_out_info->size0, p_frm_out_info->p_data1, p_frm_out_info->size1);
			cpy_size += p_frm_out_info->size1;
		} else {
			memcpy(fTo + p_frm_out_info->size0, p_frm_out_info->p_data1, maxsize - p_frm_out_info->size0);
			cpy_size = maxsize;
		}
	}
exit:
	sem_post(&g_image_trans_ctx.frame_wait);
	return cpy_size;
}

static void *image_trans_routine(void *p_param)
{
	struct action_image_trans_context_t *p_image_trans_ctx = (struct action_image_trans_context_t *)p_param;
	struct frame_encode_context_st      **pp_frame_encode_ctx = NULL;
	FRM_OBJ                             **pp_frm_obj = NULL;
	struct camera_data                  *p_camera_dev = NULL;
	int ret;

	p_camera_dev       = p_image_trans_ctx->p_camera_dev;
	pp_frame_encode_ctx = &p_image_trans_ctx->p_frame_encode_ctx;
	pp_frm_obj          = &p_image_trans_ctx->p_frm_obj;
	while(p_image_trans_ctx->running_flag) {
		ret = p_camera_dev->opr->GetFrame(p_camera_dev);
		if(ret != 0) {
			PRINTF("%s:camera get frame error\r\n", __func__);
			continue;
		}
		ret = frame_encode_input_a_frame_by_phy(*pp_frame_encode_ctx, *pp_frm_obj, (int)p_camera_dev->phy_buf.m.offset);
		if(ret != 0) {
			PRINTF("%s:encode one frame error\r\n", __func__);
			continue;
		}
		ret = frame_encode_get_a_frame(*pp_frame_encode_ctx, *pp_frm_obj, &g_image_trans_ctx.image_trans_packet.frm_out_info);
		if(ret != 0) {
			PRINTF("%s:get a encode frame error\r\n", __func__);
			continue;
		}
		sem_wait(&g_image_trans_ctx.wifi_trans_ready);
		wifi_trans_trigger(g_image_trans_ctx.p_wifi_trans_dev);
		sem_wait(&g_image_trans_ctx.frame_wait);
		frame_encode_put_a_frame(*pp_frame_encode_ctx, *pp_frm_obj, &g_image_trans_ctx.image_trans_packet.frm_out_info);
	}

	return NULL;
}

/******************encode************************************/
static int h264_frame_encode_init(struct frame_enc_config_st *p_config)
{
	struct frame_encode_context_st *p_frame_encode_ctx;
	FRM_OBJ                        *p_frm_obj;

	p_frame_encode_ctx = frame_encode_get_context(FRM_ENCODE_H264);
	if(!p_frame_encode_ctx) {
	   PRINTF("%s:get h264 frame encode context error\r\n", __func__);
	   return -1;
	}
	p_frm_obj = frame_encode_create_obj(p_frame_encode_ctx);
	if(!p_frm_obj) {
		PRINTF("%s:create frame object error\r\n", __func__);
		return -1;
	}
	frame_encode_config_obj(p_frame_encode_ctx, p_frm_obj, p_config);
	g_image_trans_ctx.p_frame_encode_ctx = p_frame_encode_ctx;
	g_image_trans_ctx.p_frm_obj 		 = p_frm_obj;

	return 0;
}

static int image_trans_frame_encode_init(void)
{
	struct frame_enc_config_st      frame_encode_config;
	struct h264_spec_param_st      *p_h264_spec = NULL;

	if(g_image_trans_ctx.p_frame_encode_ctx != NULL){
		PRINTF("%s:image trans context maybe initialized\r\n", __func__);
		return -1;
	}
	image_trans_param_load();
	memset(&frame_encode_config, 0, sizeof(struct frame_enc_config_st));
	frame_encode_config.output_width = g_image_trans_ctx.image_trans_packet.format.width;
//	frame_encode_config.output_width = 1280;
	frame_encode_config.output_height = ALIGN(g_image_trans_ctx.image_trans_packet.format.height, 16);
//	frame_encode_config.output_height = ALIGN(720, 16);

	frame_encode_config.input_width = g_image_trans_ctx.p_camera_dev->width;
	frame_encode_config.input_height = ALIGN(g_image_trans_ctx.p_camera_dev->height, 16);
	p_h264_spec = &frame_encode_config.spec_param.h264_spec;
	p_h264_spec->bit_rate = (frame_encode_config.output_width*frame_encode_config.output_height*1024UL)/(IMAGE_TRANS_OUT_WIDTH*IMAGE_TRANS_OUT_HEIGHT)*2.5*1024;
	p_h264_spec->frame_rate = g_image_trans_ctx.p_camera_dev->fps;
	p_h264_spec->max_key_interval = g_image_trans_ctx.p_camera_dev->fps;
	p_h264_spec->vbv_size = 1*1024*1024;
	p_h264_spec->virtual_iframe = 10;
	PRINTF("width = %d, height = %d, bit rate = %dbps\n", frame_encode_config.output_width, frame_encode_config.output_height, \
		frame_encode_config.spec_param.h264_spec.bit_rate);
	return h264_frame_encode_init(&frame_encode_config);
}

static int image_trans_frame_encode_exit(void)
{
	struct frame_encode_context_st *p_frame_encode_ctx = g_image_trans_ctx.p_frame_encode_ctx;
	FRM_OBJ                        *p_frm_obj          = g_image_trans_ctx.p_frm_obj;

	if(g_image_trans_ctx.p_frame_encode_ctx == NULL){
		PRINTF("%s:image trans context maybe stopped\n", __func__);
		return -1;
	}
	frame_encode_destroy_obj(p_frame_encode_ctx, p_frm_obj);
	frame_encode_put_context(p_frame_encode_ctx);
	g_image_trans_ctx.p_frame_encode_ctx = NULL;
	g_image_trans_ctx.p_frm_obj          = NULL;

	return 0;
}

static int action_run_image_trans_start(void)
{
	int ret = -1;

	ret = image_trans_frame_encode_init();
	if(!ret) {
		sem_init(&g_image_trans_ctx.frame_wait, 0, 0);
		sem_init(&g_image_trans_ctx.wifi_trans_ready, 0, 0);
		g_image_trans_ctx.running_flag = 1;
		ret = pthread_create(&g_image_trans_ctx.thread_id, NULL, image_trans_routine, &g_image_trans_ctx);
	}else{
		PRINTF("%s:encode init error\n", __func__);
	}

	return ret;
}

static int action_run_image_trans_stop(void)
{
	pthread_mutex_lock(&g_image_trans_ctx.mutex);
	g_image_trans_ctx.running_flag = 0;
	sem_post(&g_image_trans_ctx.wifi_trans_ready);
	sem_post(&g_image_trans_ctx.frame_wait);
	pthread_join(g_image_trans_ctx.thread_id, NULL);
	sem_destroy(&g_image_trans_ctx.frame_wait);
	sem_destroy(&g_image_trans_ctx.wifi_trans_ready);
	image_trans_frame_encode_exit();
	pthread_mutex_unlock(&g_image_trans_ctx.mutex);

	return 0;
}

int image_trans_camera_id_get(void)
{
	return g_image_trans_ctx.camera_id;
}

/******************camera************************************/
static int image_trans_camera_init(const int camera_id)
{	
	struct camera_data *p_camera_dev = NULL;
	int ret = -1;

	g_image_trans_ctx.camera_id = camera_id;
	p_camera_dev = camera_get_video_device(camera_id);
	if(p_camera_dev == NULL) {
		printf("%s:get camera device error\n", __func__);
		goto err_exit;
	}	
	ret = p_camera_dev->opr->StartDevice(p_camera_dev);
	if(!ret)
		g_image_trans_ctx.p_camera_dev = p_camera_dev;
err_exit:
	return ret;
}

static int actions_init(void)
{
	pthread_mutex_init(&g_image_trans_ctx.mutex, NULL);
	g_image_trans_ctx.p_action_recorder = action_object_get("action recorder");
	if(!g_image_trans_ctx.p_action_recorder) {				
		PRINTF("get action recorder error\r\n");
	}

	g_image_trans_ctx.p_action_take_photos = action_object_get("action take photos");
	if(!g_image_trans_ctx.p_action_take_photos) {
		PRINTF("get action take photos error\r\n");
	}

	g_image_trans_ctx.p_action_image_algorithm = action_object_get("action image algorithm");
	if(!g_image_trans_ctx.p_action_image_algorithm) {
		PRINTF("get action image algorithm error\r\n");
	}
	
	return 0;
}

static int image_trans_liver_status(void *cmd)
{
	h264Head *p_h264pps = (h264Head *)cmd;
	static struct frame_enc_head_st pps_spp;

	if(g_image_trans_ctx.p_frame_encode_ctx && g_image_trans_ctx.p_frm_obj){
		frame_encode_get_head_info(g_image_trans_ctx.p_frame_encode_ctx, g_image_trans_ctx.p_frm_obj, &pps_spp);			
		p_h264pps->buff = pps_spp.head_buf;
		p_h264pps->size = pps_spp.head_size;	
	}
	
	return 0;
}

static int image_trans_liver_ready(void *p_param)
{
	sem_post(&g_image_trans_ctx.wifi_trans_ready);
	return 0;
}

static void image_trans_liver_start(void)
{
	action_run_image_trans_start();
}

static void image_trans_liver_stop(void)
{
	action_run_image_trans_stop();
}

int action_run_wifi_start(void)
{
	g_image_trans_ctx.p_wifi_trans_dev = wifi_trans_create();
	if(g_image_trans_ctx.p_wifi_trans_dev) {
		wifi_trans_register_liver_status(g_image_trans_ctx.p_wifi_trans_dev, image_trans_liver_status);
		wifi_trans_register_liver_send(g_image_trans_ctx.p_wifi_trans_dev, image_trans_send_frame);
		wifi_trans_register_liver_ready(g_image_trans_ctx.p_wifi_trans_dev, image_trans_liver_ready);
		wifi_trans_register_liver_control(g_image_trans_ctx.p_wifi_trans_dev,TRANS_START,image_trans_liver_start);
		wifi_trans_register_liver_control(g_image_trans_ctx.p_wifi_trans_dev,TRANS_STOP,image_trans_liver_stop);
	}
	
	return 0;
}

static void* action_run_image_trans(struct action_object_t *p_action_obj, void *p_param)
{
	struct event_data event;
	int ret;
	
	actions_init();
	ret = image_trans_camera_init(0);
	if(ret == 0)
		action_run_wifi_start();
	while(1) {
		event_get(&event);
		switch(event.id) {
			case CARD_DISABLE:
				action_run_exit(g_image_trans_ctx.p_action_recorder, NULL);
				break;

			case NET_DISCONNECT:
				liverClose();
				action_run_exit(g_image_trans_ctx.p_action_recorder, NULL);
				action_run_exit(g_image_trans_ctx.p_action_image_algorithm, NULL);
				break;

			case PROCESS_EXIT:
				action_run_exit(g_image_trans_ctx.p_action_recorder, NULL);
				action_run_exit(g_image_trans_ctx.p_action_image_algorithm, NULL);
				sync();
				exit(0);
				break;

			case CAMERA_ERROR:
				action_run_exit(g_image_trans_ctx.p_action_image_algorithm, NULL);
				g_image_trans_ctx.p_camera_dev->opr->StopDevice(g_image_trans_ctx.p_camera_dev);
				g_image_trans_ctx.p_camera_dev->opr->StartDevice(g_image_trans_ctx.p_camera_dev);
				break;

			case SYS_LOW_POWER:
				liverClose();
				action_run_exit(g_image_trans_ctx.p_action_recorder, NULL);
				action_run_exit(g_image_trans_ctx.p_action_image_algorithm, NULL);
				g_image_trans_ctx.p_camera_dev->opr->StopDevice(g_image_trans_ctx.p_camera_dev);
				wifi_stop();
				PRINTF("system enter low power mode...\n");
				break;

			case SYS_RESUME:
				g_image_trans_ctx.p_camera_dev->opr->StartDevice(g_image_trans_ctx.p_camera_dev);
				wifi_start();
				break;
				
			default:
				break;
		}
	
	}
	return NULL;
}

static void* action_run_exit_image_trans(struct action_object_t *p_action_obj, void *p_param)
{
	PRINTF("%s\n", __func__);
	action_run_image_trans_stop();
	return NULL;
}

struct action_object_t action_object_image_trans = {
	.name             = "action image_trans", 
	.action_run       = action_run_image_trans,
	.action_run_exit  = action_run_exit_image_trans,
};

int action_image_trans_camera_switch(int camera_id)
{
	int ret = -1;
	int cnt = 0;

	while(g_image_trans_ctx.p_frame_encode_ctx != NULL){
		usleep(100000);
		if(++cnt > 20){
			PRINTF("%s: image trans is running......\n", __func__);
			goto err_exit;
		}
	}

	if(camera_id != g_image_trans_ctx.p_camera_dev->id)
		ret = image_trans_camera_init(camera_id);
	else
		PRINTF("%s: same camera, do nonthing!\n", __func__);
err_exit:
	return ret;
}

int image_trans_resolution_set(int width, int height)
{
	INI_VAL iniVal;
	int ret = -1;

	if(g_image_trans_ctx.p_frame_encode_ctx != NULL){
		PRINTF("image transfer is running\r\n");
		goto err_exit;
	}
	g_image_trans_ctx.image_trans_packet.format.width = width;
	g_image_trans_ctx.image_trans_packet.format.height = height;
	iniVal.type = INI_VAL_TYPE_NUM;
	iniVal.val.num = width;
	set_ini_value("image_trans", "width", &iniVal);
	iniVal.val.num = height;
	set_ini_value("image_trans", "height", &iniVal);
	ret = 0;
err_exit:
	return ret;
}

int action_object_image_trans_init(void)
{
	int ret;
	
	ret = register_action_object(&action_object_image_trans);
	if(!ret)
		ret = preview_cmd_init();
		
	return ret;
}
