#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <action/action_manager.h>
#include <camera/camera.h>
#include <cedarx/frame_encode.h>
#include <file_manage/file_manage.h>
#include <platform_cfg.h>
#include <led/led.h>
#include <storage_mgr/ap_tf.h>
#include <action/action_take_photos.h>
#include <action/action_preview.h>
char full_name[64]={0};

struct action_take_photos_context_t {
	struct frame_encode_context_st *p_frame_encode_ctx;
	FRM_OBJ                        *p_frm_obj;
	struct camera_data *p_camera_dev;
};

struct action_take_photos_context_t *gp_take_photos_ctx = NULL;

static int get_local_date_time(char *dt)
{
	time_t stime;	
	struct tm *ptm;

	if(!dt)
		return -1;
	if(((time_t)-1) == time(&stime)){		
		PRINTF("time() error!\n"); 	
		return -1;	
	}
	ptm = localtime(&stime);	
	if(!ptm){		
		PRINTF("local time error!\n"); 	
		return -1;
	}

	snprintf(dt, 32, "%d:%02d:%02d %02d:%02d:%02d", 
		ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);

	return 0;
}

static int frame_encode_jpeg_encode_init(void)
{
	struct frame_encode_context_st *p_frame_encode_ctx = gp_take_photos_ctx->p_frame_encode_ctx;
	FRM_OBJ                        *p_frm_obj;
	struct frame_enc_config_st      frame_encode_config;
	char dtime[32];

	if(!p_frame_encode_ctx){
		PRINTF("%s:jpeg frame encode context is NULL\n", __func__);
		return -1;
	}
	p_frm_obj = frame_encode_create_obj(p_frame_encode_ctx);
	if(!p_frm_obj) {
		PRINTF("%s:create frame object error\n", __func__);
		return -1;
	}
	
	memset(&dtime, 0, sizeof(dtime));
	memset(&frame_encode_config, 0, sizeof(struct frame_enc_config_st));
	frame_encode_config.input_width = gp_take_photos_ctx->p_camera_dev->photograph.width;
	frame_encode_config.input_height = ALIGN(gp_take_photos_ctx->p_camera_dev->photograph.height,16);
	frame_encode_config.output_width = frame_encode_config.input_width;
	frame_encode_config.output_height = frame_encode_config.input_height;

	frame_encode_config.spec_param.jpeg_spec.thumb_enable = 1;
	frame_encode_config.spec_param.jpeg_spec.thumb_width = IMG_THUMB_WIDTH;
	frame_encode_config.spec_param.jpeg_spec.thumb_height= IMG_THUMB_HEIGHT;
	frame_encode_config.spec_param.jpeg_spec.exif.camera_manufact = (unsigned char*)"ALLWINNER";
	frame_encode_config.spec_param.jpeg_spec.exif.camera_model    = (unsigned char*)"MR100";
	if(!get_local_date_time((char*)dtime)){
		frame_encode_config.spec_param.jpeg_spec.exif.data_time	  = (unsigned char*)&dtime;
	}
	frame_encode_config_obj(p_frame_encode_ctx, p_frm_obj, &frame_encode_config);
	gp_take_photos_ctx->p_frm_obj = p_frm_obj;

	return 0;
}
static int action_jpeg_encode_init(void)
{
	int camera_id;
	
	if(!gp_take_photos_ctx) {
		gp_take_photos_ctx = (struct action_take_photos_context_t *)malloc(sizeof(struct action_take_photos_context_t));
		if(!gp_take_photos_ctx) {
			PRINTF("%s: malloc take photos context fail\n", __func__);
			return -1;
		}
	}
	camera_id = image_trans_camera_id_get();
	gp_take_photos_ctx->p_camera_dev = camera_get_video_device(camera_id);
	if(gp_take_photos_ctx->p_camera_dev == NULL) {
		PRINTF("camera_get_video_device failed!\n");
		goto err_exit;
	}
	/* stop camera for changing resolution */
	if(gp_take_photos_ctx->p_camera_dev->opr->InitTakePicture(gp_take_photos_ctx->p_camera_dev))
	{
		PRINTF("camera InitTakePicture failed!\n");
		goto err_exit;
	}
	
	gp_take_photos_ctx->p_frame_encode_ctx = frame_encode_get_context(FRM_ENCODE_JPEG);
	if(!gp_take_photos_ctx->p_frame_encode_ctx) {
	   PRINTF("%s:get jpeg frame encode context error\n", __func__);
	   return -1;
	}

	return 0;

err_exit:
	free(gp_take_photos_ctx);
	gp_take_photos_ctx = NULL;
	return -1;
}

static int action_jpeg_encode_exit(void)
{
	/* start to video mode */
	gp_take_photos_ctx->p_camera_dev->opr->ExitTakePicture(gp_take_photos_ctx->p_camera_dev);
	gp_take_photos_ctx->p_frame_encode_ctx = NULL;
	gp_take_photos_ctx->p_frm_obj 		 = NULL;
	free(gp_take_photos_ctx);
	gp_take_photos_ctx = NULL;

	return 0;
}

static int action_save_a_photo(struct frame_enc_outinfo_st *p_frame_encode_out, const char *name)
{
	FILE *p_file = NULL;
	int	ret = 0;

	p_file = user_file_open(name,"wb");
	if(NULL == p_file){
		PRINTF("%s():open %s file fail\n",__func__,name);
		return -1;
	}

	user_file_write(p_frame_encode_out->p_data0, p_frame_encode_out->size0, 1, p_file);
	if(p_frame_encode_out->p_data1){
		user_file_write(p_frame_encode_out->p_data1, p_frame_encode_out->size1, 1, p_file);
	}
	ret = user_file_close(name, p_file);
	ret = (ret?-1:0);
    cmd_response_takephoto(name,ret,(const char *)full_name);
	return ret;
}
static int action_save_a_photo_thumb(struct frame_enc_outinfo_st *p_frame_encode_out, const char *name)
{
	FILE *p_file = NULL;
	int ret = 0;
	char dirname[32], basename[32];
    
	memset(dirname, 0, sizeof(dirname));
	memset(basename, 0, sizeof(basename));
	memset(full_name, 0, sizeof(full_name));
	_file_name_split(name, dirname, basename);
	snprintf(full_name, strlen((char*)PHOTO_THUMB_FOLDER) + strlen(basename) + 1, "%s%s",PHOTO_THUMB_FOLDER,basename);
	p_file = user_file_open(full_name,"wb");
	if(NULL == p_file){
		PRINTF("%s():open %s file fail\n",__func__,full_name);
		return -1;
	}
	user_file_write(p_frame_encode_out->p_data0, p_frame_encode_out->size0, 1, p_file);
	ret = user_file_close(full_name, p_file);
	ret = (ret?-1:0);
	printf("save photo thumb file %s success \n",full_name);

	return 0;
}
static int action_take_photos_routine(unsigned int phy_addr)
{
	struct frame_encode_context_st *p_frame_encode_ctx = NULL; 
	FRM_OBJ 					   *p_frm_obj = NULL;		   
	frame_enc_outinfo_st            frm_out_info;
	int ret = 0;
	char name[MAX_NAME_LENGTH];

	ret = frame_encode_jpeg_encode_init();
	if(ret != 0) {
		PRINTF("%s:frame encode jpeg init error\n", __func__);
		goto err_exit1;
	}
	p_frame_encode_ctx = gp_take_photos_ctx->p_frame_encode_ctx;
	p_frm_obj		   = gp_take_photos_ctx->p_frm_obj;
	ret = frame_encode_input_a_frame_by_phy(p_frame_encode_ctx, p_frm_obj, phy_addr);
	if(ret != 0) {
		PRINTF("%s:encode one frame error\n", __func__);
		goto err_exit1;
	}
	memset(name, 0, sizeof(name));
	user_file_name_generate(PHOTO_FILE_TYPE, name);
	/* the first must be save thumbnail */
	ret = frame_encode_get_thumbnai_a_frame(p_frame_encode_ctx, p_frm_obj, &frm_out_info);
	if(ret != 0) {
		PRINTF("%s:get a thumbnail encode frame error\n", __func__);
		goto err_exit1;
	}
	action_save_a_photo_thumb(&frm_out_info, name);
	ret = frame_encode_get_a_frame(p_frame_encode_ctx, p_frm_obj, &frm_out_info);
	if(ret != 0) {
		PRINTF("%s:get a encode frame error\n", __func__);
		goto err_exit1;
	}
	action_save_a_photo(&frm_out_info, name);

	ret = frame_encode_put_a_frame(p_frame_encode_ctx, p_frm_obj, &frm_out_info);
	if(ret != 0) {
		PRINTF("%s:put a encode frame error\n", __func__);
		goto err_exit1;
	}	

err_exit1:
	if(p_frame_encode_ctx && p_frm_obj) {
		frame_encode_destroy_obj(p_frame_encode_ctx, p_frm_obj);
		frame_encode_put_context(p_frame_encode_ctx);
	}
	return ret;
}

static void* action_run_take_photos(struct action_object_t *p_action_obj, void *p_param)
{
	int ret = -1;
	struct action_take_photos_param *take_picture_param = (struct action_take_photos_param*)p_param;
	unsigned int led_current_state, led_tmp_state;
	struct led_dev *leds = led_get_handler();

	/* led blink during delay period */
	if(take_picture_param->delay > 0)
	{
		if(leds != NULL){
			led_current_state = (leds->mode == LED_BLINK_ON_OFF);
			led_tmp_state = !led_current_state;
			while(take_picture_param->delay--)
			{
				led_switch(led_tmp_state);
				led_tmp_state = !led_tmp_state;
				sleep(1);
			}
			/* recovery led state */
			led_switch(led_current_state);
		}else{
			sleep(take_picture_param->delay);
		}
	}

	if((take_picture_param->num > IMG_CAP_MAX_NUM) || (!take_picture_param->num))
	{
		PRINTF("%s: invalid number!\n", __func__);
		goto err_exit;
	}
	led_blink(take_picture_param->num);
#if 0	
if(disk_space_is_full())
	{
		ret = -1;
		PRINTF("card is not online or off-space!\n");
		goto err_exit;
	}
#endif
	if(gp_take_photos_ctx == NULL) {
		ret = action_jpeg_encode_init();
		if(ret != 0) {
			PRINTF("%s:action_jpeg_encode_init error\n", __func__);
			goto err_exit;
		}
	}
	while(take_picture_param->num--) {
		ret = gp_take_photos_ctx->p_camera_dev->opr->StartTakePicture(gp_take_photos_ctx->p_camera_dev);
		if(ret < 0)
		{
			PRINTF("%s: take picture error!\n", __func__);
			goto err_take_picture;
		}
		ret = action_take_photos_routine(gp_take_photos_ctx->p_camera_dev->phy_buf.m.offset);
		gp_take_photos_ctx->p_camera_dev->opr->StopTakePicture(gp_take_photos_ctx->p_camera_dev);
	}

err_take_picture:
	action_jpeg_encode_exit();
err_exit:
	sync();
	return (void *)ret;
}

static void* action_run_asyn_take_photos(struct action_object_t *p_action_obj, void *p_param)
{
	int ret  = 0;

	PRINTF("%s\n", __func__);
	action_run_take_photos(p_action_obj, p_param);
	return (void*)ret;
}

static struct action_object_t action_object_take_photos = {
	.name             = "action take photos", 
	.action_run       = action_run_take_photos,
	.action_run_asyn  = action_run_asyn_take_photos,
};

int action_object_take_photos_init(void)
{
	int ret = -1;

	if(!register_action_object(&action_object_take_photos))
		ret = take_photos_cmd_init();
	return ret;
}
