#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>

void init_daemon(void)
{
	int fdTableSize, i, pid;

	pid = fork();
	if (pid > 0)
		exit(0);

	setsid();

	pid = fork();
	if (pid > 0)
		exit(0);

	fdTableSize = getdtablesize();
	for (i=0; i<fdTableSize; i++)
		close(i);


	chdir("/");
	umask(0);
}
int main(int argc, char **argv)
{
	unsigned char key_val;
	int  fd;

	init_daemon();

	fd = open("/dev/key_pwr", O_RDONLY);
	if (fd < 0)
		exit(1);
	while (1)
	{
		read(fd, &key_val, 1);
		if (key_val)
		{			
			system("killall VideoTransmission -s SIGTERM");
			sleep(1);                    //wait for app processing
			umount("/netPrivate");
			system("poweroff -f");
			break;
		}
	}
	
	return 0;
}

