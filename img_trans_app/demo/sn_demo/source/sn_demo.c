#include <sn.h>
#include <../../../include/ini/aw_ini_parser.h>
unsigned int sn_data[4];
unsigned int sn_data_check[4];
int get_sn_from_firmware(unsigned int *sn_data_check)//get sn from firmware
{
	INI_VAL iniVal;
	iniVal.type = INI_VAL_TYPE_NUM;
	if(!get_ini_value("SN", "sn0", &iniVal)&&iniVal.val.num!=0){
		sn_data_check[0]=iniVal.val.num;
	}else{
		return 0;
	}
	
	if(!get_ini_value("SN", "sn1", &iniVal)&&iniVal.val.num!=0){
		sn_data_check[1]=iniVal.val.num;
	}
	else
	{
		return 0;
	}
		if(!get_ini_value("SN", "sn2", &iniVal)&&iniVal.val.num!=0){
		sn_data_check[2]=iniVal.val.num;
	}else{
		return 0;
	}
	
	if(!get_ini_value("SN", "sn3", &iniVal)&&iniVal.val.num!=0){
		sn_data_check[3]=iniVal.val.num;
	}
	else
	{
		return 0;
	}
		
		return 1;
}
int set_sn_to_firmware(unsigned int *sn_data_check)//write sn to firmware
{
		INI_VAL iniVal;
		iniVal.type = INI_VAL_TYPE_NUM;
		iniVal.val.num = sn_data_check[0];
		set_ini_value("SN", "sn0", &iniVal);
		iniVal.val.num = sn_data_check[1];
		set_ini_value("SN", "sn1", &iniVal);
		iniVal.val.num = sn_data_check[2];
		set_ini_value("SN", "sn2", &iniVal);
		iniVal.val.num = sn_data_check[3];
		set_ini_value("SN", "sn3", &iniVal);
		system("umount /netPrivate/");
	  system("mount /dev/mtdblock4 /netPrivate/");
	  return 1;
}
int sn_check(void)
{
	if(get_sn_from_firmware(sn_data_check))
	  {
	  	if(get_sn(MR100,sn_data))////get sn from MR100 IC
	  		{
	  			printf("sn_data[0]:%x,sn_data[1]:%x,sn_data[2]:%x,sn_data[3]:%x \n",sn_data[0],sn_data[1],sn_data[2],sn_data[3]); 
	  			printf("sn_data_check[0]:%x,sn_data_check[1]:%x,sn_data_check[2]:%x,sn_data_check[3]:%x \n",sn_data_check[0],sn_data_check[1],sn_data_check[2],sn_data_check[3]); 
	  			if(sn_data[0]==sn_data_check[0]&&sn_data[1]==sn_data_check[1]&&sn_data[2]==sn_data_check[2]&&sn_data[3]==sn_data_check[3])
	  				{
	  					printf("sn_check ok\n");
	  					return 1;
	  				}
	  				else
	  				{
	  					printf("sn_check fail\n");
	  					return -1;
	  				}
	  		}
	  	
	  }
	  else
	  {
	  	printf("first burn firmware sn_check null\n");
	  	if(get_sn(MR100,sn_data))
	  	{
	  		 set_sn_to_firmware(sn_data);
	  	}
	  	else
	  	{
	  		printf("get_sn fail\n");
	  	}
	  	return 0;
	  }
}
void main(int argc, char *argv[])
{
	 	  sn_check();			
}


