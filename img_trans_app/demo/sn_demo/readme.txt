MR100有唯一的SN序列号，附件为SN获取的库和绑定IC使用DEMO。
使用方法：
                      1.将本目录config.ini放置在lichee\tools\img_app\bin下，config.ini文件中添加了[SN]选项
                      2.进入sn_demo目录下，make clean;
                                           make
                      3.执行sn_demo即可
库对外提供的接口：int get_sn(unsigned int chip_type,unsigned int *src);//chip_type:MR100;src:SN存放的位置。