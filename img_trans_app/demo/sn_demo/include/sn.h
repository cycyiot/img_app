#ifndef __SN_H__
#define __SN_H__
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <termios.h>
#include <sys/msg.h>
#include <unistd.h>
#include <stdint.h>
#define MR100 1
int get_sn(unsigned int chip_type,unsigned int *src);
#endif

