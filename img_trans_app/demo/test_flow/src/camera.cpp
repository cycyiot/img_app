#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/videodev2.h>

#include <sys/sem.h>
#include <sys/shm.h>
#include <semaphore.h>
#include <sys/ipc.h>

#include "camera.h"
#define FLOW_SHM_PIC 1


#ifdef FLOW_SHM_PIC
static int shm_id, shm_fmt_id;   
static char * shm_ptr = NULL, *shm_fmt_ptr;
//static int shm_opencv_id;
//static char * shm_opencv_ptr=NULL;
FILE * fp=NULL;


union semun
{
    int val;
    struct semid_ds *buf;
    unsigned short *arry;
};

static int sem_id1, sem_id2,sem_id3,sem_id4;

int set_semvalue(int sem_id,int val)
{
    union semun sem_union;
 
    sem_union.val = val;
    if(semctl(sem_id, 0, SETVAL, sem_union) == -1){
        return 0;
    }
	
    return 1;
}

int semaphore_p(int sem_id)
{
    struct sembuf sem_b;
    sem_b.sem_num = 0;
    sem_b.sem_op = -1;
    sem_b.sem_flg = SEM_UNDO;
    if(semop(sem_id, &sem_b, 1) == -1){
        fprintf(stderr, "semaphore_p failed\n");
        return 0;
    }
	
    return 1;
}
 
int semaphore_v(int sem_id)
{
    struct sembuf sem_b;
    sem_b.sem_num = 0;
    sem_b.sem_op = 1;
    sem_b.sem_flg = SEM_UNDO;
    if(semop(sem_id, &sem_b, 1) == -1){
		set_semvalue(sem_id,0);
        fprintf(stderr, "semaphore_v failed\n");
        return 0;
    }
	
    return 1;
}

static struct camera_data camera_dev;

int camera_dev_init(struct v4l2_format *format)
{
	camera_dev.fps = FPS;
	camera_dev.width = format->fmt.pix.width;
	camera_dev.height = format->fmt.pix.height;
	camera_dev.format = format->fmt.pix.pixelformat;
	
	memcpy(shm_fmt_ptr,&camera_dev,sizeof(struct camera_data));
//	semaphore_v(sem_fmt_id);
	printf("fps = %d, width = %d, height = %d, camera_dev.format = %d\n", camera_dev.fps, camera_dev.width, camera_dev.height, camera_dev.format);
	return 0;
}

void camera_flow_init(struct v4l2_format *format)
{
	sem_id1 = semget((key_t)2234, 1, 0666 | IPC_CREAT);
	sem_id2 = semget((key_t)2235, 1, 0666 | IPC_CREAT);
//	sem_fmt_id = semget((key_t)2236, 1, 0666 | IPC_CREAT);
	
	set_semvalue(sem_id1,1);
	set_semvalue(sem_id2,0);
//	set_semvalue(sem_fmt_id,0);

	shm_id = shmget(777,1024,IPC_CREAT);	
	if(shm_id == -1){
        perror("shmget failed");
		exit(1);
    }else{
		shm_ptr = (char *)shmat(shm_id, 0, 0); 
    }

	shm_fmt_id = shmget(778,1024,IPC_CREAT);	
	if(shm_fmt_id == -1){
        perror("shmget failed");
		exit(1);
    }else{
		shm_fmt_ptr = (char *)shmat(shm_fmt_id, 0, 0); 
    }

	camera_dev_init(format);
}

void camera_opencv_init()
{
    sem_id3 = semget((key_t)2236, 1, 0666 | IPC_CREAT);
    sem_id4 = semget((key_t)2237, 1, 0666 | IPC_CREAT);
//  sem_fmt_id = semget((key_t)2236, 1, 0666 | IPC_CREAT);
    
    set_semvalue(sem_id3,1);
    set_semvalue(sem_id4,0);
    
    fp=fopen("/tmp/aruco.jpg","wb");
    if(fp==NULL){
     printf("open error\n");
     }

//  set_semvalue(sem_fmt_id,0);
#if 0
    shm_opencv_id= shmget(779,1024,IPC_CREAT);    
    if(shm_opencv_id == -1){
        perror("shmget failed");
        exit(1);
    }else{
        shm_opencv_ptr = (char *)shmat(shm_opencv_id, 0, 0); 
    }
#endif
}

#endif

//封装后的ioctl()
static void xioctl(int fh, int request, void *arg)
{
    int r;
    do {
        r = ioctl(fh, request, arg);
    } while ( (r == -1) && ((errno == EINTR) || (errno == EAGAIN)) );
    if (r == -1) {
        printf("%s(): ioctl('request = %d') failed: %s\n", __func__, request, strerror(errno));
        exit(EXIT_FAILURE);
    }
}

struct my_buffer {
    void   *start;
    size_t length;
};

//static struct myyuv_buf myyuv_b;
static struct my_buffer *g_my_buffers; 
static struct v4l2_buffer g_frame;  //代表驱动的一帧
static int    g_camera_fd = 0;      //摄像头的设备文件句柄
//unsigned char *yuv422frame;
//bool flag_t = false;
int camera_init(const char *dev_name, int width, int height)
{
    g_camera_fd = open(dev_name, O_RDWR | O_NONBLOCK, 0);
    //g_camera_fd = open(dev_name, O_RDWR);
    if (g_camera_fd < 0) {
        printf("%s(): open('%s') failed: %s\n", __func__, dev_name, strerror(errno));
        return -1;
    }
    
    //fcntl(g_camera_fd, F_SETFD, FD_CLOEXEC);
    
    struct v4l2_input inp;
    memset(&inp, 0, sizeof(inp));
    inp.index = 0;
    if (-1 == ioctl(g_camera_fd, VIDIOC_S_INPUT, &inp)) {
        printf("%s(): ioctl('VIDIOC_S_INPUT') failed: %s\n", __func__, strerror(errno));
        return -1;
    }
    
    struct v4l2_streamparm parms;
    memset(&parms, 0, sizeof(parms));
    parms.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    parms.parm.capture.timeperframe.numerator = 1;
    parms.parm.capture.timeperframe.denominator = FPS;
    //parms.parm.capture.capturemode = V4L2_MODE_VIDEO; //V4L2_MODE_IMAGE
    if (-1 == ioctl(g_camera_fd, VIDIOC_S_PARM, &parms)) {
        printf("%s(): ioctl('VIDIOC_S_PARM') failed: %s\n", __func__, strerror(errno));
        return -1;
    }
    
    struct v4l2_format format;  //帧的格式，比如高度，宽度等
    memset(&format, 0, sizeof(format));
    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;  //设定视频捕获格式
    format.fmt.pix.width  = width;
    format.fmt.pix.height = height;
    format.fmt.pix.pixelformat = V4L2_PIX_FMT_YUV420;
    format.fmt.pix.field = V4L2_FIELD_NONE;
    xioctl(g_camera_fd, VIDIOC_S_FMT, &format);  //设定当前驱动的捕获格式，同时读取出参数
   
    if (format.fmt.pix.pixelformat != V4L2_PIX_FMT_YUV420) {
        printf("%s(): Libv4l didn't accept YUV420 format. Can't proceed\n", __func__);
        return -1;
    }
    
    if (    (format.fmt.pix.width  != (unsigned int)width) 
         || (format.fmt.pix.height != (unsigned int)height) ) {  //检查命令行参数是否设置成功
        printf("%s(): Set resolution failed: Driver is sending image at %dx%d\n", __func__, format.fmt.pix.width, format.fmt.pix.height);
        return -1;
    }
    
    struct v4l2_requestbuffers req;  //向驱动申请帧缓冲的请求，包含申请个数
    memset(&req, 0, sizeof(req));
    req.count = BUFFER_CNT;  //申请 6 帧图像内存
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;
    xioctl(g_camera_fd, VIDIOC_REQBUFS, &req);  //分配内存
    g_my_buffers = (struct my_buffer *)calloc(req.count, sizeof(*g_my_buffers));  //calloc()申请到的内存会清零
    
    struct v4l2_buffer frame;  //代表驱动中的一帧
    unsigned int i = 0;
    for (i = 0; i < req.count; i++) {
        memset(&frame, 0, sizeof(frame));
        frame.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        frame.memory = V4L2_MEMORY_MMAP;
        frame.index  = i;
        xioctl(g_camera_fd, VIDIOC_QUERYBUF, &frame);  //把 VIDIOC_REQBUFS  中分配的数据缓存转换成物理地址
        g_my_buffers[i].length = frame.length;
        g_my_buffers[i].start = mmap(NULL, frame.length, PROT_READ | PROT_WRITE, MAP_SHARED, g_camera_fd, frame.m.offset);
        if (MAP_FAILED == g_my_buffers[i].start) {
            printf("%s(): mmap() failed: %s\n", __func__, strerror(errno));
            return -1;
        }
    }
    
    for (i = 0; i < req.count; i++) {
        memset(&frame, 0, sizeof(frame));
        frame.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        frame.memory = V4L2_MEMORY_MMAP;
        frame.index  = i;
        xioctl(g_camera_fd, VIDIOC_QBUF, &frame);  //放入缓存队列
    }
    
    enum v4l2_buf_type V4l2_buf_type;
    V4l2_buf_type = V4L2_BUF_TYPE_VIDEO_CAPTURE;  //数据流类型，必须永远是V4L2_BUF_TYPE_VIDEO_CAPTURE
    xioctl(g_camera_fd, VIDIOC_STREAMON, &V4l2_buf_type);  //开始视频显示函数

#ifdef FLOW_SHM_PIC
	camera_flow_init(&format);
    camera_opencv_init();
#endif

    return 0;
}
#if 0
void yuvtomat()
{

 //aruco_init();

  while(1){
    sleep(1);
    if(flag_t == true)
        break;
  }
 
  int Frame_Height=480;
  int Frame_Width=640;
  Mat mymat;
  while(1){
  //  printf("--==========================\n");
  Mat myuv( Frame_Height + Frame_Height / 2, Frame_Width, CV_8UC1, (unsigned char *) yuv422frame);
  cvtColor(myuv, mymat, CV_YUV420p2RGB);
  imwrite("/tmp/2222.jpg",mymat);
  opencv_aruco_distinguish(mymat);
//  sleep(1);
  }
}
#endif
//取了YUV 里面的 Y 分量，所以拿到的图像是会灰度化的
uint8_t *camera_get_image(size_t *length)
{
    int ret = 0;
    static bool s_first_run = true;
    
    if(s_first_run == false) {
        //将缓冲重新入队列尾，这样可以循环采集
        xioctl(g_camera_fd, VIDIOC_QBUF, &g_frame);  
    }
    
    //监视 camera_fd 是否有数据过来
    do {
        fd_set fds;
        FD_ZERO(&fds);
        FD_SET(g_camera_fd, &fds);
        struct timeval tv;
        tv.tv_sec = 2;
        tv.tv_usec = 0;
        ret = select(g_camera_fd + 1, &fds, NULL, NULL, &tv);
    } while ((ret == -1) && (errno == EINTR));
    if (ret == -1) {
        printf("%s(): select() failed: %s", __func__, strerror(errno));
        return NULL;
    }
    
    memset(&g_frame, 0, sizeof(g_frame));
    g_frame.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    g_frame.memory = V4L2_MEMORY_MMAP;
    xioctl(g_camera_fd, VIDIOC_DQBUF, &g_frame); //出队列以取得以采集数据的帧缓冲，取得原始采集数据
    //printf("%s(): g_frame.index = %d\n", __func__, g_frame.index);
#if 1
   if(s_first_run == false){
  // yuv422frame =(unsigned char*)g_my_buffers[g_frame.index].start;
  
#ifdef FLOW_SHM_PIC
    set_semvalue(sem_id4,0);
    semaphore_p(sem_id3);
    fwrite((uint8_t *)g_my_buffers[g_frame.index].start,g_my_buffers[g_frame.index].length,1,fp);
    fseek(fp,0,SEEK_SET);
    semaphore_v(sem_id3);
    semaphore_v(sem_id4);
#endif
}
#endif

#ifdef FLOW_SHM_PIC
	set_semvalue(sem_id2,0);
	semaphore_p(sem_id1);
	memcpy(shm_ptr,&g_frame,sizeof(struct v4l2_buffer));
	semaphore_v(sem_id1);
	semaphore_v(sem_id2);
#endif

    s_first_run = false;
    *length=g_my_buffers[g_frame.index].length;
   // printf("len= %d\n", g_my_buffers[g_frame.index].length);
    return (uint8_t *)g_my_buffers[g_frame.index].start;
}

void camera_deinit(void)
{
    enum v4l2_buf_type V4l2_buf_type;
    V4l2_buf_type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    xioctl(g_camera_fd, VIDIOC_STREAMOFF, &V4l2_buf_type);  //结束视频显示函数
    
    int i = 0;
    for (i = 0; i < BUFFER_CNT; ++i) {
        munmap(g_my_buffers[i].start, g_my_buffers[i].length);  //取消参数start所指的映射内存起始地址
    }
#ifdef FLOW_SHM_PIC	
    fclose(fp);
#endif
    close(g_camera_fd);
}

