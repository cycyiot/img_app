#ifndef __COMMU_H__
#define __COMMU_H__

#include "flow.h"  //usr_result_t

int  commu_init(void);
int  commu_send(const usr_result_t *result);
void commu_deinit(void);

#endif
