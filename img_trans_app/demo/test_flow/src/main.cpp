#include <stdio.h>
#include <pthread.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>

#include "flow.h"
#include "camera.h"
#include "commu.h"

#define STACK_SIZE                      (64*1024)
#define ARRAY_SIZE(arr)                 ( sizeof(arr) / sizeof((arr)[0]) )
#define IS_IN_RANGE(var, min, max)      ( ((min) <= (var)) && ((var) <= (max)) )

pthread_t thid1;

static void *thread_flow(void *arg)
{
    int ret = 0;
    usr_args_t *usr_args = (usr_args_t *)arg;
    uint8_t *image = NULL;
    usr_result_t result;
    size_t len=0;
  
    ret = camera_init("/dev/video1", usr_args->width, usr_args->height);
    if(ret < 0)  exit(-1);
    
       
    ret = flow_init(usr_args);
    if(ret < 0)  exit(-1);
    
       
    ret = commu_init();
    if(ret < 0)  exit(-1);
    
      
    while(1) {
        //从摄像头获取一帧数据
      //  printf("get_imgage\n");
        image = camera_get_image(&len);
       
        //将图像送入光流算法
        flow_get_data(image, &result);
        /*
        printf("version = %3u, vx = %6.0f, vy = %6.0f, confidence = %3u, brightness = %3u (%s)\n", 
               result.version, result.vx, result.vy, result.confidence, result.image_brightness,
               (result.image_grade != 0) ? "abnormal" : "normal");
        */
        //将光流数据发送给飞控进程
        commu_send(&result);

    }
    
    commu_deinit();
    flow_deinit();
    camera_deinit();
    return NULL;
}


static int parse_cmd(int argc, char **argv, usr_args_t *usr_args)
{
    int opt;
    const char *optstring = "w:h:f:m:r:d:pv";
    
    //榛樿鍊�
    usr_args->width   = 640;
    usr_args->height  = 480;
    usr_args->frate   = 50;
    usr_args->optimize = 3;
    usr_args->record  = 0;
    usr_args->directory = "/mnt";
    usr_args->print   = 0;
    
    while ((opt = getopt(argc, argv, optstring)) != -1)
    {
        switch (opt) {
			
            case 'w':
                usr_args->width = atoi(optarg);
                break;
            case 'h': 
                usr_args->height = atoi(optarg);
                break;
            case 'f': 
                usr_args->frate = atoi(optarg);
                break;
            case 'm': 
                usr_args->optimize = atoi(optarg);
                if( !IS_IN_RANGE(usr_args->optimize, 0, 3) ) {
                    printf("invalid option -- '%s'\n", optarg);
                    goto SHOW_USAGE;
                }
                break;
            case 'r': 
                usr_args->record = atoi(optarg);
                if( !IS_IN_RANGE(usr_args->record, 0, 3) ) {
                    printf("invalid option -- '%s'\n", optarg);
                    goto SHOW_USAGE;
                }
                break;
            case 'd':
                usr_args->directory = optarg;
                break;
            case 'p':
               // usr_args->print = 1;
				
                break;
            case 'v':
                printf("Optical-flow algorithm version: %d\n", flow_get_version());
                exit(0);
                break;
            default:
                goto SHOW_USAGE;
                break;
        }
    }
    
    //鍒ゆ柇鍙傛暟鏄惁绗﹀悎瑕佹眰
    if(optind != argc) {
        printf("%s: invalid option -- '%s'\n", argv[0], argv[optind]);
        goto SHOW_USAGE;
    }
    return 0;

SHOW_USAGE:
    printf("Usage: %s [-w width] [-h height] [-f frate] [-m optimize] [-r record] [-d directory] [-p] [-v]\n", argv[0]);
    printf("-w width:\n");
    printf("  set camera's output width. default:640\n");
    printf("-h height:\n");
    printf("  set camera's output height. default:480\n");
    printf("-f frate:\n");
    printf("  set camera's frame rate. default:50FPS\n");
    printf("-m optimize:\n");
    printf("  set optimization level (0~3). default:1\n");
    printf("  smaller number means better result and higher CPU usage\n");
    printf("  bigger  number means worse  result and lower  CPU usage\n");
    printf("-r record\n");
    printf("  record video in YUV 4:0:0 format. default:0\n");
    printf("  0     none\n");
    printf("  1     raw video\n");
    printf("  2     clipped video\n");
    printf("  3     resized video\n");
    printf("-d directory\n");
    printf("  specify a directory to save the video. default:/mnt\n");
    printf("-p\n");
    printf("  print optical-flow data\n");
    printf("-v\n");
    printf("  show optical-flow algorithm version, then exit\n");
    return -1;
}

static int platform_create_thread(const char *name, int priority, int stack_size, void *(*entry) (void *), void* parameter, pthread_t *thid)
{
	int rv;
	pthread_t task;
	pthread_attr_t attr;
	struct sched_param param;

	rv = pthread_attr_init(&attr);
	if (rv != 0) {
		printf("%s(): failed to init thread attrs\n", __func__);
		return rv;
	}

	rv = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
	if (rv != 0) {
		printf("%s(): failed to set inherit sched\n", __func__);
		return rv;
	}

	rv = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
	if (rv != 0) {
		printf("%s(): failed to set sched policy\n", __func__);
		return rv;
	}

	rv = pthread_attr_setstacksize(&attr, stack_size);
	if (rv != 0) {
		printf("%s(): failed to set stack size\n", __func__);
		return rv;
	}

	param.sched_priority = priority;
	rv = pthread_attr_setschedparam(&attr, &param);
	if (rv != 0) {
		printf("%s(): failed to set sched param\n", __func__);
		return rv;
	}

	rv = pthread_create(&task, &attr, entry, parameter);
	if (rv != 0) {
		if (rv == EPERM) {
			printf("%s(): warning: unable to start\n", __func__);
			rv = pthread_create(&task, NULL, entry, parameter);
			if (rv != 0) {
				printf("%s(): failed to create thread\n", __func__);
				return (rv < 0) ? rv : -rv;
			}
		}
		else {
			return rv;
		}
	}
    
    *thid = task;
	return 0;
}

void hal_linux_mlock(void)
{
	if(mlockall(MCL_CURRENT|MCL_FUTURE) == -1){
		perror("mlockall failed");
		exit(-2);
	}
}


int main(int argc, char *argv[])
{
	usr_args_t usr_args;
    int ret = 0;
    
	hal_linux_mlock();
   // printf("join main\n");
    ret = parse_cmd(argc, argv, &usr_args);
    if(ret < 0) {
        exit(-1);
    }
   
	ret = platform_create_thread("flow", 60, STACK_SIZE, thread_flow, &usr_args, &thid1);
    if(ret != 0) {
        exit(-1);
    }

   // printf("join thid1\n");
    pthread_join(thid1, NULL);
	return 0;
}
