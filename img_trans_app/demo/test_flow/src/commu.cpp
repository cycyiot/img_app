#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>

#include <sys/msg.h>
#include <sys/types.h>
#include <sys/sysinfo.h>

#include "commu.h"

#define USE_MSGQUE      1  //是否使用消息队列来发送数据

#define CONSOLE "/dev/ttyS0"
static int console_fd = -1;

#if USE_MSGQUE
static int g_msgque = -1;  //消息队列，用来向飞控进程发送光流数据

//光流程序通过消息队列发送数据给飞控程序，此为消息队列元素的数据结构
typedef struct {
	long		msg_type;  //消息类型。固定为1
	uint16_t	version;   //光流程序版本号
	float		vx;    //x轴的光流速度
	float		vy;    //y轴的光流速度
	uint8_t		confidence;    //光流数据可信度，范围为0~100，0表示最差，100表示最好
    //以下三个变量从不同角度描述了图像的亮度。选用三者之一即可。
    uint8_t		image_brightness;  //图像亮度：0~255，0为最暗（全黑），255为最亮（全白）
    uint8_t		image_score;       //图像评分：0~100，0为最差（过暗或过亮），100为最好（亮度适中）
    int8_t		image_grade;       //图像等级：-1为过暗，0为正常，1为过亮
    float		time_loop;
    float		time_run;
	int			frame;
} flow_msg_t;
#else

#define PACKED __attribute__((__packed__))

typedef struct PACKED{
	float		vx;
	float		vy;
	uint8_t		confidence;
} flow_msg_t;

#endif

void hal_linux_uart_cfg(int fd, int speed)
{  
    struct termios options;  

    tcgetattr(fd, &options);  
	
    options.c_cflag |= (CLOCAL | CREAD);
    options.c_cflag &= ~CSIZE;
    options.c_cflag &= ~CRTSCTS;
    options.c_cflag &= ~CSTOPB;
    options.c_cflag |= CS8;

    options.c_lflag = 0;  
    options.c_lflag &= ~ISIG;
    options.c_lflag &= ~ECHO;

    options.c_oflag = 0;
	options.c_oflag &= ~OPOST;

    options.c_iflag |= IGNPAR;
    options.c_iflag &= ~IXON;
    options.c_iflag &= ~ICRNL;
	
    cfsetospeed(&options, speed); 

    tcflush(fd, TCIFLUSH);
    tcsetattr(fd, TCSANOW, &options);
	tcflush(fd,TCIOFLUSH);
}

int commu_init(void)
{
#if USE_MSGQUE
    g_msgque = msgget((key_t)1234, 0666 | IPC_CREAT);
    if(g_msgque < 0) {
        fprintf(stderr, "%s(): msgget() failed: %s\n", __func__, strerror(errno));
        return -1;
    }
    return 0;
#else
	console_fd = open(CONSOLE, O_RDWR | O_NONBLOCK | O_NDELAY | O_NOCTTY);
	if(console_fd < 0){
		printf("commu_init:open console(%s) failed \r\n", CONSOLE);
		return false;
	}else{
		hal_linux_uart_cfg(console_fd,B115200);
		printf("commu_init:open console(%s) ok \r\n", CONSOLE);
	}
    return 0;
#endif
}   

int commu_send(const usr_result_t *result)
{
#if USE_MSGQUE
    flow_msg_t msg;

	msg.msg_type			= 1;
	msg.version				= result->version;
	msg.vx					= result->vx;
	msg.vy					= result->vy;
	msg.confidence			= result->confidence;
	msg.image_brightness	= result->image_brightness;
	msg.image_score			= result->image_score;
	msg.image_grade			= result->image_grade;
	msg.time_loop			= result->time_loop;
	msg.time_run			= result->time_run;
	msg.frame				= result->frame;

    int ret = msgsnd(g_msgque, &msg, sizeof(msg) - sizeof(msg.msg_type), IPC_NOWAIT);
    if(ret < 0) {
        //fprintf(stderr, "%s(): msgsnd() failed: %s\n", __func__, strerror(errno));
        return -1;
    }
	
    return 0;
#else
    flow_msg_t msg;

	msg.vx					= result->vx;
	msg.vy					= result->vy;
	msg.confidence			= result->confidence;

	if(console_fd > 0){
		write(console_fd,&msg,sizeof(msg));
	}

    return 0;
#endif
}

void commu_deinit(void)
{
#if USE_MSGQUE
    msgctl(g_msgque, IPC_RMID, NULL);
#else
    //@TODO: 用户自定义功能
#endif
}
