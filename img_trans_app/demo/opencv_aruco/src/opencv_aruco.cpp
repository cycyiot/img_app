#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/videodev2.h>

#include <sys/sem.h>
#include <sys/shm.h>
#include <semaphore.h>
#include <sys/ipc.h>

#include <sys/msg.h>
#include <sys/types.h>
#include <sys/sysinfo.h>
#include <sys/time.h>
#include <time.h> 

#include <iostream>
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/imgcodecs/imgcodecs.hpp>
#include<opencv2/opencv.hpp>
#include<Eigen/Eigen>
#include <opencv2/core/eigen.hpp>

#include <opencv2/aruco.hpp>
#include "opencv_aruco.h"
#include "libaruco_math.h"
using namespace cv;
using namespace std;

#define MARKLEN 0.096//0.155

Mat cameraMatrix;
Mat distCoeffs;
cv::Ptr<cv::aruco::Dictionary> dictionary;
std::vector<int> ids;
std::vector<std::vector<cv::Point2f> > corners;
std::vector<cv::Vec3d> rvecs, tvecs;
//static int shm_opencv_id;
//static char * shm_opencv_ptr=NULL;
 
static int g_msgque=-1;

float Pitch=0;
float Roll=0;
float Yaw=0;

#if 0
typedef struct {
  long    msg_type;  
  uint16_t  version; 
  float     pitch;
  float     yaw;
  float     roll;
  float     z;
} aruco_msg_t;
#endif

typedef struct {
  long    msg_type;  
  uint16_t  version; 
  float id_data[9][4];
} aruco_msg_t;

aruco_msg_t msg;
union semun
{
    int val;
    struct semid_ds *buf;
    unsigned short *arry;
};

static int sem_id3, sem_id4;

int set_semvalue(int sem_id,int val)
{
    union semun sem_union;
 
    sem_union.val = val;
    if(semctl(sem_id, 0, SETVAL, sem_union) == -1){
        return 0;
    }
  
    return 1;
}

int semaphore_p(int sem_id)
{
    struct sembuf sem_b;
    sem_b.sem_num = 0;
    sem_b.sem_op = -1;
    sem_b.sem_flg = SEM_UNDO;
    if(semop(sem_id, &sem_b, 1) == -1){
        fprintf(stderr, "semaphore_p failed\n");
        return 0;
    }
  
    return 1;
}
 
int semaphore_v(int sem_id)
{
    struct sembuf sem_b;
    sem_b.sem_num = 0;
    sem_b.sem_op = 1;
    sem_b.sem_flg = SEM_UNDO;
    if(semop(sem_id, &sem_b, 1) == -1){
    set_semvalue(sem_id,0);
        fprintf(stderr, "semaphore_v failed\n");
        return 0;
    }
  
    return 1;
}

#if 0
static int commu_send(float pitch,float yaw,float roll);

static void *send_heart(void *arg){

   while(1){
    usleep(10000); // 10ms
    commu_send(Pitch,Yaw,Roll);
   }

 return NULL;
}

static int opencv_linux_create_thread()
{

    int rv;
    pthread_t task;
    pthread_attr_t attr;
    pthread_attr_init(&attr);

    pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
    rv = pthread_create(&task, NULL, send_heart,NULL);
    if (rv != 0) {
        if (rv == EPERM) {
            printf("warning: unable to start");
            rv = pthread_create(&task, NULL, send_heart, NULL);

            if (rv != 0) {
                printf("platform_create_thread: failed to create thread");
                return (rv < 0) ? rv : -rv;
            }
        }else{
            return rv;
        }
    }
   pthread_attr_destroy(&attr);
   return 0;
}
#endif
void opencv_shm_init()
{
    sem_id3 = semget((key_t)2236, 1, 0666 | IPC_CREAT);
    sem_id4 = semget((key_t)2237, 1, 0666 | IPC_CREAT);
#if 0
    shm_opencv_id= shmget(779,1024,IPC_CREAT);    
    if(shm_opencv_id == -1){
        perror("shmget failed");
        exit(1);
    }else{
        shm_opencv_ptr = (char *)shmat(shm_opencv_id, 0, 0); 
    }
#endif
}
static int commu_init(void)
{
    g_msgque = msgget((key_t)1235, 0666 | IPC_CREAT);
    if(g_msgque < 0) {
        fprintf(stderr, "%s(): msgget() failed: %s\n", __func__, strerror(errno));
        return -1;
    }

	//opencv_linux_create_thread();
    return 0;
 }  

int commu_send(float pitch,float yaw,float roll,float z,int id)
{

#if 0
  aruco_msg_t msg;

  msg.msg_type      = 1;
  msg.version       = id;
  msg.pitch         =pitch;
  msg.yaw           =yaw;
  msg.roll          =roll;
  msg.z             =z;
#endif
  msg.msg_type      = 1;

  int ret = msgsnd(g_msgque, &msg, sizeof(msg) - sizeof(msg.msg_type), IPC_NOWAIT);
   if(ret < 0) {
     fprintf(stderr, "%s(): msgsnd() failed: %s\n", __func__, strerror(errno));
     return -1;
    }

    return 0;
}
void commu_deinit(void)
{
 
 msgctl(g_msgque, IPC_RMID, NULL);

}

 cv::Ptr<aruco::DetectorParameters> parameters;

void aruco_init_t(){

  cameraMatrix = (cv::Mat_<double>(3, 3) <<
  553.5154109341001, 0.0, 318.1205174289681,
  0.00000000e+00, 552.0717598191222, 230.6581903014068,
  0.00000000e+00, 0.00000000e+00, 1.00000000e+00);

  distCoeffs = (cv::Mat_<double>(1, 5) <<-0.03896422027559478 ,0.04657580873484304 ,-0.0044651362731342676,  -0.0017040737057739694 ,-0.10916314588856893
);
  dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_4X4_50);
#if 1
  parameters = aruco::DetectorParameters::create();

  parameters->adaptiveThreshWinSizeMin = 3 ; 
  parameters->adaptiveThreshWinSizeMax = 6; 
  parameters->adaptiveThreshWinSizeStep = 3; 
  parameters->minMarkerPerimeterRate = 0.21;
 
#endif
}


void test_t()
{ 
  int fd=-1;
  fd=open("/dev/video1",O_RDWR | O_NONBLOCK, 0);
  if(fd < 0)
  {
    perror("error :");
    printf("fd =%d open error\n",fd);
  }
  else{
    printf("fd = %d open ok\n", fd);
    close(fd);
  }

}

void opencv_aruco_init()
{
   //test_t();
   aruco_init_t();
   opencv_shm_init();
   commu_init();
   
}
void opencv_aruco_exit()
{

 commu_deinit();

}

//struct timespec start; 
void get_shm_opencv()
{
#if 1
   FILE * fp=NULL;
   char * buff=NULL;
   int len=0;
   
   fp=fopen("/tmp/aruco.jpg","rb");
   if(fp==NULL){
    debug_t("aruco open fail\n");
    exit(1);
   }
   else{
    fseek(fp,0,SEEK_END);
    len=ftell(fp);
    buff=(char *)malloc(sizeof(char)*(len+1));
    if(buff == NULL){
    debug_t("aruco malloc fail\n");
	exit(1);
    }
    fseek(fp,0,SEEK_SET);
   // ret=fread(buff,1,len,fp);
   // printf("len = %d  ret = %d\n",len,ret);
    //fclose(fp);
   }
  int Frame_Height=480; 
  int Frame_Width=640;
#endif
  Mat mymat;
while(1){
#if 1 
  semaphore_p(sem_id4);
  semaphore_p(sem_id3);
  fread(buff,1,len,fp);
  fseek(fp,0,SEEK_SET);
  semaphore_v(sem_id3);
  
  Mat myuv( Frame_Height + Frame_Height / 2, Frame_Width, CV_8UC1,(unsigned char *)buff);
 // cvtColor(myuv, mymat, CV_YUV420p2RGB);
  cvtColor(myuv, mymat,CV_YUV420p2GRAY);
  //imwrite("/mnt/666.jpg",mymat);
 // printf("%d\n",mymat.size());
 #endif
  // printf("eeeeeeeeeeeeeeeeeeeee\n");
   //mymat=imread("/mnt/2.jpg",1);
  opencv_aruco_distinguish(mymat);
 
  }
  free(buff);
  fclose(fp);

}
#if 0
void set_zero()
{
	Pitch=0;
    Roll=0;
 //   Yaw=0;
}
/**********************************************************
                 ^
                 |y
                 |                    机头
                 |
                 |
<---------------------------------------
 x               |
                 |
                 |
                |
                 |

***********************************************************/
#define DISTANCE 0.1   //圆心距离
#define POWER    70  //动力
#define POWER_ZERO 0
static bool a_flag=false;
#define   FRIST_TARGET     2
#define   SECOND_TARGET    4
static int rt_target=FRIST_TARGET;

#if 0
static void target_switch()
{
  if(rt_target == FRIST_TARGET){
     rt_target=SECOND_TARGET;
  }
  else{
    rt_target=FRIST_TARGET;
  }

}
#endif
void calculation_move_alt(float x,float y,float yaw)
{
  // printf("x0: %f  y0: %f  z0: %f\r\n",x,y,z);
  
   if( pythagorous2(x,y) > pythagorous2(DISTANCE,DISTANCE)){             //标记在圆外，需要移动
     // printf("x: %f  y: %f  z: %f\r\n",x,y,z);
     if(a_flag == false){
	  if(x > DISTANCE && y > DISTANCE){
         Pitch=POWER;
		 Roll=-POWER;                                        
	  }
	  else if(x > DISTANCE && y < DISTANCE && y > -DISTANCE){
        Roll=-POWER;
		Pitch=POWER_ZERO;
	  }
	  else if(x > DISTANCE && y < -DISTANCE){
         Roll=-POWER;
		 Pitch=-POWER;
	  }
	  else if(x < -DISTANCE && y < -DISTANCE){
        Roll=POWER;
		Pitch=-POWER;
	  }
	  else if(x < -DISTANCE && y > DISTANCE){
        Roll=POWER;
		Pitch=POWER;
	  }
	  else if(x < -DISTANCE && y < DISTANCE && y > -DISTANCE){
        Roll=POWER;
	    Pitch=POWER_ZERO;
	  }
	  else if(x < DISTANCE && x > -DISTANCE && y > DISTANCE){
        Roll=POWER_ZERO;
		Pitch=POWER;
	  }
	  else if(x < DISTANCE && x > -DISTANCE && y < -DISTANCE){
        Roll=POWER_ZERO;
		Pitch=-POWER;
	  }
	  else{
       debug_t("error xy\n");
	  }

	  commu_send(Pitch,rt_target,Roll,0);
	  debug_t("id = %d Roll = %3f Pitch = %3f x = %f y = %f \n",rt_target,Roll,Pitch,x,y);
     }
   }
   else{                                       //标记在圆内，不做动作
     set_zero();
	 a_flag=true;
	 commu_send(0,0,0,0);
	 debug_t("=====================in cicle====================================\n");
    // target_switch();                        //切换标记
	//debug_t("in cicle \n");
   }

}

#endif
void opencv_aruco_distinguish(Mat frame)
{
   //Mat frame1=frame.clone();
  //  Mat frame1;
   //cvtColor(frame, frame1, CV_BGR2GRAY);
   // frame.copyTo(frame1);
    //imwrite("/tmp/3333.jpg",frame1);
  //  printf("fffffffffffffffffffffffffff\n");

   // get_diff_time(&start,true);
  
    cv::aruco::detectMarkers(frame, dictionary, corners, ids,parameters);//检测标记

	//cout<<get_diff_time(&start,false)*1000<<"ms"<<endl;
		
      // if at least one marker detected
    if (ids.size() > 0) {
    // cv::aruco::drawDetectedMarkers(frame, corners, ids);//画出标记
    
     
     cv::aruco::estimatePoseSingleMarkers(corners, MARKLEN, cameraMatrix, distCoeffs, rvecs, tvecs);//求解旋转矩阵
     for(unsigned int i =0;i <ids.size();i++){
     //cout<<"R :"<<rvecs[0]<<endl;
      // cout<<"T :"<<tvecs[0]<<endl;
     // draw axis for each marker
     //   for(int i=0; i<ids.size(); i++)
     //  cv::aruco::drawAxis(frame, cameraMatrix, distCoeffs, rvecs[i], tvecs[i], 0.1);
     //   imwrite("/tmp/5555.jpg",frame);
 #if 1                                                //计算欧拉角
      cv::Mat rmat;
    
      Rodrigues(rvecs[i], rmat);
     
      Eigen::Matrix3d rotation_matrix3d;
      cv2eigen(rmat,rotation_matrix3d);
    
      Eigen::Vector3d eulerAngle = rotation_matrix3d.eulerAngles(0,1,2);//(0,1,2)表示分别绕XYZ轴顺序，即 顺序，逆时针为正
   //   cout<<"pitch: "<<eulerAngle.x()*180 / 3.1415926<<" yaw: "<<eulerAngle.z() *180 / 3.1415926<<" roll:"<<eulerAngle.y()*180 / 3.1415926<<endl;
 #endif     
    //  cout<<"x="<<tvecs[i][0]<<" y="<<tvecs[i][1]<<" z="<<tvecs[i][2]<<endl;
    //  cout<<"id ="<<ids[i]<<endl;
	 // Pitch=eulerAngle.x()*180 / 3.1415926;
	//  Roll=eulerAngle.y()*180 / 3.1415926;
	 // Yaw=eulerAngle.z() *180 / 3.1415926;
	// debug_t("x: %f  y: %f  z: %f\r\n",tvecs[i][0],tvecs[i][1],tvecs[i][2]);

	//if(ids[i] == rt_target){
  //   calculation_move_alt(tvecs[i][0],tvecs[i][1],eulerAngle.z() *180 / 3.1415926);
   //  	}
      //commu_send(tvecs[i][1],tvecs[i][2],tvecs[i][0],ids[i]);
      if(i < 9){
       msg.id_data[i][0]=ids[i];              //id
       msg.id_data[i][1]=tvecs[i][0];         //x
	   msg.id_data[i][2]=tvecs[i][1];          // y
	   msg.id_data[i][3]=eulerAngle.z() *180 / 3.1415926; //yaw
      	}
      // commu_send(tvecs[i][1],eulerAngle.z() *180 / 3.1415926,tvecs[i][0],tvecs[i][2],ids[i]);
      
          } 
       msg.version=ids.size();
	   commu_send(0,0,0,0,0);
		
	  }
     else{
	  msg.version=0;
	  commu_send(0,0,0,0,0);
	 }
}
