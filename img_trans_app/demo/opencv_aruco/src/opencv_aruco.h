#ifndef __OPENCV_ARUCO_H__
#define __OPENCV_ARUCO_H__
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
using namespace cv;
void opencv_aruco_distinguish(Mat frame);
void opencv_aruco_init();
void opencv_aruco_exit();
void get_shm_opencv();

#define    debug_t(str, arg...)  do{\
           time_t timep;\
           struct tm *p;\
           char time_str[40];\
           memset(time_str,'\0',40);\
           time(&timep);\
           p=localtime(&timep);\
           sprintf(time_str,"[%d.%02d.%02d %02d:%02d:%02d]",(1900+p->tm_year),(1+p->tm_mon),p->tm_mday,p->tm_hour,p->tm_min,p->tm_sec);\
           FILE *debug_fp = fopen("/tmp/debug", "a");\
           if (NULL != debug_fp){\
           fprintf(debug_fp, "%s L%d in %s, ",time_str, __LINE__, __FILE__);\
           fprintf(debug_fp, str, ##arg);\
           fflush(debug_fp);\
           fclose(debug_fp);\
           }\
}while(0)


#endif
