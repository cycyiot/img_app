#include "videodev2.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <sys/time.h>

#define CLEAR(x)				memset(&(x), 0, sizeof(x))

#define WIFI_IF_NAME			"wlan0"// "uap0"
#define CAMERA_FRAME_BUF_NUM 	4
#define VID_CAP_WIDTH 			1632
#define VID_CAP_HEIGHT 			928
#define FM_CAP_WIDTH 			640
#define FM_CAP_HEIGHT 			480

struct camera_buf{
	unsigned int buf_len;
	void *virtual_buf[CAMERA_FRAME_BUF_NUM];
};


struct camera_buf v4l2_buf;

int test_spi_nor_flash(void)
{
	char buf[100];
	int fd = 0;
	int ret = 0;
	
	system("/sbin/test_spi_nor.sh");
	sleep(1);
	fd = open("/tmp/test_spi_nor.sh_ret", O_RDONLY);
	if(fd == -1){
		printf("[%s,%d]:open test_spi_nor.sh_ret failed!\r\n", __func__, __LINE__);
		return -1;
	}

	memset(buf, 0, 100);
	read(fd, buf, 100);
	printf("read state:%s\n", buf);
	if(strncmp(buf, "0", 1) == 0) {
		ret = 0;
	}else {
		ret = -1;
	}

	close(fd);
	return ret;
}

int test_wifi(void)
{
	int ret = 0;
	char path[64];
	DIR *dir = NULL;

	CLEAR(path);
	sprintf(path, "/sys/class/net/%s", WIFI_IF_NAME);
	dir = opendir(path);
	if(dir == NULL){
		printf("[%s,%d]:open %s err!\r\n", __func__, __LINE__, path);
		ret = -1;
		goto errHdl;
	}else{
		printf("[%s,%d]:open %s success!\r\n", __func__, __LINE__, path);
		ret = 0;
	}

errHdl:
	return ret;
}

int test_tf(void)
{
	int ret = 0;
	char path[64];

	CLEAR(path);
	sprintf(path,"/dev/mmcblk0");
	if(access(path, F_OK) != 0){
		printf("[%s,%d]:open %s err!\r\n", __func__, __LINE__, path);
		ret = -1;
		goto errHdl;
	}

errHdl:	
	return ret;
}

int camera_buf_request(int fd, unsigned int count)
{
	int ret;
	unsigned int i;
	struct v4l2_requestbuffers requestbuffers;
	struct v4l2_buffer v4l2Buf; 
	
	CLEAR(requestbuffers);
	requestbuffers.count	= count;
	requestbuffers.type	= V4L2_BUF_TYPE_VIDEO_CAPTURE;
	requestbuffers.memory	= V4L2_MEMORY_MMAP;	
    ret = ioctl(fd, VIDIOC_REQBUFS, &requestbuffers);
	if(ret != 0){
		printf("%s:ioctl v4l2 buf request error!, ret = %d, count = %d\n", __func__, ret, requestbuffers.count);
		goto errHdl;
    }
	
	for(i = 0; i < requestbuffers.count; i++){
		CLEAR(v4l2Buf);
		v4l2Buf.type		= V4L2_BUF_TYPE_VIDEO_CAPTURE;
		v4l2Buf.memory		= V4L2_MEMORY_MMAP;
		v4l2Buf.index		= i;
		ret = ioctl(fd, VIDIOC_QUERYBUF, &v4l2Buf);
		if(ret != 0){
			printf("%s:ioctl v4l2 buf query error, ret = %d!\n", __func__, ret);
			goto errHdl;
	    }

		v4l2_buf.buf_len = v4l2Buf.length;;
		v4l2_buf.virtual_buf[i] = mmap(NULL, v4l2Buf.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, v4l2Buf.m.offset);
		if(v4l2_buf.virtual_buf[i] == MAP_FAILED){
			printf("%s:mmap kernel space alloc buf to user space error!\n", __func__);
			goto errHdl;
		}

		v4l2Buf.type	= V4L2_BUF_TYPE_VIDEO_CAPTURE;
		v4l2Buf.memory	= V4L2_MEMORY_MMAP;
		v4l2Buf.index	= i;
		ret = ioctl(fd, VIDIOC_QBUF, &v4l2Buf);
		if(ret != 0){
			printf("%s:ioctl v4l2 buf enqueue error!\n", __func__);
			goto errHdl;
	    }
	}
	
errHdl:
	return ret;
}

int camera_buf_release(int fd)
{
	int i = 0; 
	int	ret = -1;
	enum v4l2_buf_type type;

	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	ret = ioctl(fd, VIDIOC_STREAMOFF, &type);
	for (i = 0; i < CAMERA_FRAME_BUF_NUM; i++){
		ret = munmap(v4l2_buf.virtual_buf[i], v4l2_buf.buf_len);
		if(ret == -1){
			printf("[%s,%d]:munmap error!\n", __func__, __LINE__);
		}
    }
	close(fd);
	return ret;
}

int camera_frame_get(int fd)
{
	struct v4l2_buffer v4l2Buf;
	int ret;

	v4l2Buf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;	
	v4l2Buf.memory = V4L2_MEMORY_MMAP;
	ret = ioctl(fd, VIDIOC_DQBUF, &v4l2Buf);
	if(ret == 0){
		ret = ioctl(fd, VIDIOC_QBUF, &v4l2Buf);
	}

	return ret;
}

int test_camera(char devIdx, unsigned int width, unsigned int height, unsigned int format, unsigned int fps)
{
	int ret = 0;
	char path[64];
	int fd;
	struct v4l2_input input;
	struct v4l2_streamparm streamparam;
	struct v4l2_format streamformat;
	enum v4l2_buf_type type;
	fd_set fdSet;	
	struct timeval time;
	
	CLEAR(path);
	sprintf(path, "/dev/video%d", devIdx);
	fd = open(path, O_RDWR);
	if(fd == -1){
		printf("[%s,%d]:open %s err!\r\n", __func__, __LINE__, path);	
		ret = -1;
		goto errHdl;
	}

	CLEAR(input);
	input.index = 0;
	input.type = V4L2_INPUT_TYPE_CAMERA;
    ret = ioctl(fd, VIDIOC_S_INPUT, &input);
	
	CLEAR(streamparam);
	streamparam.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    streamparam.parm.capture.timeperframe.numerator = 1;
    streamparam.parm.capture.timeperframe.denominator = fps;
    streamparam.parm.capture.capturemode = V4L2_MODE_VIDEO; 
    ret = ioctl(fd, VIDIOC_S_PARM, &streamparam);
	
	CLEAR(streamformat);
	streamformat.type                	= V4L2_BUF_TYPE_VIDEO_CAPTURE;
    streamformat.fmt.pix.width       	= width; 	
    streamformat.fmt.pix.height      	= height;
    streamformat.fmt.pix.pixelformat 	= format;
    streamformat.fmt.pix.field       	= V4L2_FIELD_NONE;
	ret = ioctl(fd, VIDIOC_S_FMT, &streamformat);

	ret = camera_buf_request(fd, CAMERA_FRAME_BUF_NUM);
	
	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	ret = ioctl(fd, VIDIOC_STREAMON, &type);
	if(ret != 0) {
		printf("[%s,%d]:stream on failed!\r\n", __func__, __LINE__);	
		goto errHdl;
	}
	
	FD_ZERO(&fdSet);	
	FD_SET(fd, &fdSet);
	while(1){
		CLEAR(time);
		time.tv_sec = 2;
		time.tv_usec = 0;
		ret = select(fd + 1, &fdSet, NULL, NULL, &time);
		if(ret > 0){
			ret = camera_frame_get(fd);
			if(ret < 0){
				printf("[%s,%d]:get frame from %s failed!\r\n", __func__, __LINE__, path);	
				ret = -1;
				goto errHdl;
			}else{
				printf("[%s,%d]:get frame from %s success! ret = %d\r\n", __func__, __LINE__, path, ret);	
				ret = 0;
				goto errHdl;
			}
		}else if(ret == 0){
			printf("[%s,%d]:select %s timeout!\r\n", __func__, __LINE__, path);	
			ret = -1;
			goto errHdl;
		}else{
			printf("[%s,%d]:select %s error!\r\n", __func__, __LINE__, path);	
			goto errHdl;
		}
	}

errHdl:	
	camera_buf_release(fd);
	return ret;
}

int main(void)
{
	char ret = 0;

	ret = test_spi_nor_flash();
	if(ret == 0){
		printf("[%s,%d]:spi nor flash hardware is normal!\r\n", __func__, __LINE__);
	}
	
	ret = test_wifi();
	if(ret == 0){
		printf("[%s,%d]:wifi hardware is normal!\r\n", __func__, __LINE__);
	}
	
	ret = test_tf();
	if(ret == 0){
		printf("[%s,%d]:tf card hardware is normal!\r\n", __func__, __LINE__);
	}
	
	ret = test_camera(0, VID_CAP_WIDTH, VID_CAP_HEIGHT, V4L2_PIX_FMT_YUV420, 30);
	if(ret == 0){
		printf("[%s,%d]:sensor 0 hardware is normal!\r\n", __func__, __LINE__);
	}
	
	ret = test_camera(1, FM_CAP_WIDTH, FM_CAP_HEIGHT, V4L2_PIX_FMT_YUV420, 30);
	if(ret == 0){
		printf("[%s,%d]:sensor 1 hardware is normal!\r\n", __func__, __LINE__);
	}
	
    return 0;
}
