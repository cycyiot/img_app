#cross compiler
CUR_DIR=$(shell pwd)
export CROSS_COMPILE=${CUR_DIR}/../../../out/sun8iw8p1/linux/common/buildroot/external-toolchain/bin/arm-buildroot-linux-musleabi-
#export CROSS_COMPILE=arm-none-linux-gnueabi-
#export CROSS_COMPILE=


export AS              = $(CROSS_COMPILE)as
export CC              = $(CROSS_COMPILE)gcc
export LD              = $(CROSS_COMPILE)ld
export CPP             = $(CROSS_COMPILE)g++
export AR              = $(CROSS_COMPILE)ar
export NM              = $(CROSS_COMPILE)nm
export STRIP           = $(CROSS_COMPILE)strip
export OBJCOPY         = $(CROSS_COMPILE)objcopy
export OBJDUMP         = $(CROSS_COMPILE)objdump


#CFLAGS = -isystem /arm/arm-2010.09/arm-none-linux-gnueabi/libc/usr/include
#CFLAGS += -D_GNU_SOURCE
