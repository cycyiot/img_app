#ifndef __KEY_H__
#define __KEY_H__

#include <pthread.h>
#include <semaphore.h>

#define KEY_PATH	"/dev/input/event0"

typedef enum {
	KEY_TYPE_NONE,
	KEY_TYPE_SHORT,
	KEY_TYPE_LONG,
}KEY_TYPE;

struct key_dev {
	int fd_key;
	pthread_t thread_id_key;
	pthread_mutex_t mutex;
};


void key_init(void);
void key_uninit(void);

#endif
