#ifdef __cplusplus
extern "C" {
#endif

#ifndef	__AP_TF__
#define	__AP_TF__

#include <platform_cfg.h>
#include <pthread.h>
//#define		_CARD_DEMO_		(1)

#define	CARD_DEV_NODE1		"/dev/mmcblk0"
#define	CARD_DEV_NODE2		"/dev/mmcblk0p1"
#define	CARD_NSECTORS_NODE	"/sys/class/block/mmcblk0/size"
#define CARD_CAPACITY_MAX	(64*1024)	//unit is MB

#define MAX_PATH	(64)	
#define TF_FREE_SPACE_MIN	(10 * 1024 * 1024) 

typedef enum{
	FS_UNKNOW,	
	FAT32_FS,
	NTFS_FS,
	FS_MAX
}card_fstype;

typedef enum{
	UNMOUNT,
	MOUNTED,
	NOSUPPORT
}card_status;

typedef struct{
	char dname[MAX_PATH];
	char mpath[MAX_PATH];		//mount filesytem directory
	int valid;					//insert or not of card
	card_status status;			//status: NOSUPPORT,NOMOUNT,MOUNT 
	card_fstype ftype;			//file system type:"1" fat32,"0" not mount	
	unsigned long long tsize;	//total filesystem space
	unsigned long long rsize;	//remain filesystem space
	pthread_mutex_t lock;
}card_s;

int card_insert_detect(void);

/*
 * card_format() - format the card according to card->ftype variable,
 * please set value of card->ftype varibale before call this function.
 * only support the fat32 filesystem,card->ftype is 1 that indicate 
 * fat32 filesystem,if is 0 that indicate no format. 
 * @ftype:filesystem type to want format
 * @:return value:success-> 0
 *				  fail   -> -1
 */
int card_format(card_fstype ftype);
/*
 * card_status_get() - get status of card
 * @tcard: pointer copy of card status
 * @:return value:success-> 0
 *				  fail   -> -1
 */
int card_status_get(card_s *tcard);

/* get card capacity via file system node,capacity unit is MB */
int card_capacity_get(unsigned int *cap_mb);
int disk_space_is_full(void);

/*
 * card_fs_capacity_get() - get filesystem capacity of card
 * @tcard: pointer copy of card status
 * @:return value:success-> 0
 *				  fail   -> -1
 */
int card_fs_capacity_get(card_s *tcard);
/*
 * card_is_mounted() - test card is mount or not
 * @:return value:mount -> 1   not --> 0
 */
int card_is_mounted(void);
/*
 * card_space_is_less() - test card space whether enough
 * @rspace:card space of want to reserve,unit is byte.
 * @:return value:enough -> 1   not enough --> 0  other --> -1
 */
int card_space_is_less(unsigned long long rspace);

/*
 * card_mount() - mount specify filesystem to card.please set dir to
 * mount of card->mpath variable before call this function.
 * @*mpath:path to want mount 
 * @:return value:success-> 0
 *				  fail   -> -1
 */
int card_mount(const char *mpath);
/*
 * card_unmount() - unmount the card
 * @:return value:success-> 0
 *				  fail   -> -1
 */
int card_unmount(void);
/*
 * is_need_format_card() - judge whether or not the card is format 
 * it will mount card to *mpath.if mount success,return 0,or else
 * return 1.
 * @:return value:need     -> 1
 *				  not need -> 0
 */
int is_need_format_card(const char *mpath);
/*
 * card_init() - card resource initialization.malloc struct card_s
 * memory and initial variable of this,create thread to detect card
 * status,and mount filesystem to card if card is valid.
 * @mpath:mount dir of the card
 * @ftype:filesytem to format 
 * @:return value:success-> 0
 *				  fail   -> -1;
 */
int card_init(void);
/*
 * card_exit() - free memory,destroy the card thread,and umount 
 * the filesystem.
 * @:return value:success-> 0
 *				  fail   -> -1
 */
int card_uninit(void);

#ifdef	_CARD_DEMO_
int card_demo(int argc, char *argv[]);
#endif /* _CARD_DEMO_ */


#endif /*__AP_TF__*/

#ifdef __cplusplus
}
#endif

