#ifndef	__STORAGE_MGR_H__
#define	__STORAGE_MGR_H__
#ifdef __cplusplus
extern "C" {
#endif
/*********************************************************************************************************
*
*  Header File Include
**********************************************************************************************************/
#include <platform_cfg.h>
#include <storage_mgr/ap_tf.h>
#include <pthread.h>
#include <semaphore.h>
#include <file_manage/file_manage.h>

/*********************************************************************************************************
*
*  User Macro Define
**********************************************************************************************************/
#define STORAGE_MGR_QUEUE_MAX_MSG 	 			5

typedef enum{
	CMD_REQ_STG_MGR_DEL_FILE = 0xb000,
	CMD_REQ_STG_MGR_DEL_ALL_FILE,
	CMD_REQ_STG_MGR_LOCK_FILE,
	CMD_REQ_STG_MGR_LOCK_ALL_FILE,
	CMD_REQ_STG_MGR_UNLOCK_FILE,
	CMD_REQ_STG_MGR_UNLOCK_ALL_FILE,
	CMD_REQ_STG_MGR_FORMAT,
	CMD_REQ_STG_MGR_ENC_MP4_CMPL,
	CMD_REQ_STG_MGR_ENC_JPG_CMPL,
	CMD_REQ_STG_MGR_SD_STS_UPDATE,
	CMD_REQ_STG_MGR_NET_DISCONN,
	CMD_REQ_STG_MGR_MAX
}CMD_STG_MGR;

/*********************************************************************************************************
*
*  Structure Define
**********************************************************************************************************/
typedef struct {
	INT8S (*fname)[MAX_NAME_LENGTH];
	INT32U fnum;
}STG_FILE_LIST;

/*********************************************************************************************************
*
*  Fuction Protype Declare
**********************************************************************************************************/
INT32S storage_mgr_init(void);
INT32S storage_mgr_uninit(void);
void card_state_notify(card_s *cardInfo);
int storage_cmd_init(void);

#ifdef __cplusplus
}
#endif

#endif // __STORAGE_MGR_H__