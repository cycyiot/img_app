#ifndef __PLATFORM_CFG_H__
#define __PLATFORM_CFG_H__

typedef unsigned char  															INT8U;    
typedef unsigned short 															INT16U;
typedef unsigned int 															INT32U;	 
typedef unsigned long long 														INT64U;
typedef signed char 															INT8S;    
typedef signed short 															INT16S;	 
typedef signed int 																INT32S;	 
typedef signed long long 														INT64S;

//#define BATTERY_DETECT 1
//#define SYSTEM_LOW_POWER
#define VERSION "3.1.00"
#define DBG_EN 

#ifdef DBG_EN
#define PRINTF(x,arg...) printf(x,##arg)
#else
#define PRINTF(x,arg...)
#endif

#define ALIGN(x, order) (((x) + (order - 1)) & ~(order - 1))
#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))

#endif // __PLATFORM_CFG_H__
