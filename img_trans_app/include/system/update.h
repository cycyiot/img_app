#ifndef UPDATE_H_
#define UPDATE_H_

#ifdef __cplusplus
extern "C" {
#endif

struct firmware {
	char *version;
	char *md5;
};
void set_firm_version(const char *src);
void get_firm_version(char *dst);
int  firmware_update(struct firmware *fw);

#ifdef __cplusplus
}
#endif

#endif
