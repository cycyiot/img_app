#ifndef __EVENT_H
#define __EVENT_H

typedef enum{
	PREVIEW_START,
	PREVIEW_STOP,
	RECORD_START,
	RECORD_STOP,
	TAKE_PICTURE,
	FOLLOW_START,
	FOLLOW_STOP,
	CAMERA_SWITCH,
	CARD_DISABLE,
	CAMERA_ERROR,
	NET_DISCONNECT,
	PROCESS_EXIT,
	SYS_LOW_POWER,
	SYS_RESUME,
}EV;

struct event_data{
	EV id;
	int client;
	void *param;
};

void event_init(void);
void event_put(struct event_data *event);
void event_get(struct event_data *event);

#endif
