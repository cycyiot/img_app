#ifndef __SYS_PARAM_H
#define __SYS_PARAM_H
#include <sys/time.h>
#include <time.h>

typedef enum{
	RE_CARD,
	RE_BATTERY,
	RE_FOLLOW_ME,
	RE_WIFI,
}REPORT;

int system_date_time_set(struct tm *dt);
int sys_info_detect_init(void);
void mtd_sync(void);
int sys_cmd_init(void);

#ifdef GESTURE_RECOGNITION
int ecp(int);
#endif

#endif
