#ifndef __FOLLOWME_H__
#define __FOLLOWME_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#define Min(a,b)		(((a) < (b)) ? (a) : (b))
#define Max(a,b)	(((a) > (b)) ? (a) : (b))
#define ABS(p)        	((p) > 0 ? (p):(-(p)))

#define MaxIter		8
#define MaxSize		128
#define MinSize		32
#define IniSize		48
#define PI 3.14159265358979f

typedef struct{
	int32_t x;//image columns
	int32_t y;//image rows
}Point;


uint16_t GetHistInf(uint8_t *src, uint16_t img_height, uint16_t img_width, Point *Center, uint16_t boxsize, uint8_t *lost_flag);

void GetHist(uint8_t *src, uint16_t img_height, uint16_t img_width, Point *Center, uint8_t boxsize);

void FollowMe(uint8_t * src, float *angle, Point *RectCenter, uint16_t img_height, uint16_t img_width, uint8_t *lost_flag);

uint8_t DrawRect(uint8_t *src, Point start, Point end, uint16_t img_height, uint16_t img_width);

void DrawCross(uint8_t *src, uint16_t img_height, uint16_t img_width);

uint8_t F_sqrt(uint16_t y);

float GTime(void);

void DrawLine(uint8_t *src, Point begin, Point end, uint16_t img_height, uint16_t img_width);

void Draw_Rectangle(uint8_t *src, Point Start, Point End, float sn, float cs, uint16_t img_height, uint16_t img_width);


#ifdef __cplusplus
}
#endif

#endif
