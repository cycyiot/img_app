#ifndef __FLIST_H__
#define __FLIST_H__

#define		MAX_NAME_LENGTH		(64)
#define		IS_ERROR(p)			(NULL == p)
#define		print_err			1
#define		print_dbg			1
#define		PRINT_ERR(fmt,arg...)		if (print_err)	printf("%s():%d - "fmt, __FUNCTION__,__LINE__,##arg)
#define		PRINT_DBG(fmt,arg...)		if (print_dbg)	printf("%s():%d - "fmt, __FUNCTION__,__LINE__,##arg)
#define		assert_f(p)					if (NULL == p)	{PRINT_ERR("parameter is error (0x%x) \n", (unsigned int)p); return -1;}

struct file_s{
	char				dir[MAX_NAME_LENGTH];
	char 				name[MAX_NAME_LENGTH];
	int					status;
	int					lock;
	unsigned int 		bsize;
	unsigned long long 	ctime;
};
struct node{
	struct file_s	file;
	struct node		*prev;
	struct node		*next;
};
typedef	struct node *linklist;

void flist_init(linklist *head);
void flist_destroy(linklist *head);
int flist_push(linklist *head, struct file_s *file);
int flist_pop(linklist *head, struct file_s *file);
unsigned int flist_count(linklist *head);
struct node *flist_find_node(linklist *head, struct file_s *file);
struct node *flist_next(linklist *head);
struct node *flist_prev(linklist *head);
void flist_traverse(linklist *head, void (*func)(struct file_s *file));
void flist_print_node(struct file_s *file);

#endif /*__FLIST_H__*/
