#ifndef __MEDIA_THUMB_H__
#define	__MEDIA_THUMB_H__


#define		MEDIA_THUMB_PAGENUM_MAX				(10)
#define		MEDIA_VIDEO_REAL_DIR				"mnt/video"
#define		MEDIA_PHOTO_REAL_DIR				"tmp/photo"
#define		MEDIA_THUMB_NAME_LIST				(0)
#define		MEDIA_THUMB_DATA_LIST				(1)

typedef struct app_thumb_st{
	int				file_type;
	int 			page_numb;
	int 			page_size;
	int 			thumb_width;
	int 			thumb_height;
	char 			*folder_name;
	char			*folder_thum;
	char			*thum_suffix;
	unsigned int 	list_type;
	unsigned int 	list_size;
	void			*list_data;
}app_thumb_obj;


int media_thumb_get(struct app_thumb_st *thumb);
int media_thumb_put(struct app_thumb_st *thumb);
int media_thumb_file_delete(struct app_thumb_st *thumb);



#endif
