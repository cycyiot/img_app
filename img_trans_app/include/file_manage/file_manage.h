#ifndef	__FILE_MANAGE_H__
#define __FILE_MANAGE_H__
#include <file_manage/flist.h>


#define	MAX_NAME_LENGTH		(64)
#define	VIDEO_FOLDER		"/mnt/video/"
#define	PHOTO_FOLDER		"/tmp/photo/"
#define	VIDEO_THUMB_FOLDER	"/mnt/video/thumb/"
#define	PHOTO_THUMB_FOLDER	"/tmp/photo/thumb/"
#define	VIDEO_FILE_SUFF		"mp4"
#define	PHOTO_FILE_SUFF		"jpg"
#define	VIDEO_THUMB_FILE_SUFF		"jpg"
#define	PHOTO_THUMB_FILE_SUFF		"jpg"
#define	MAX_FILE_ID			(99)
#define	MAX_FOLDER_NUM		(5)
#define	MAX_THUMB_FILE_LIST	(10)




#define    tof(str, arg...)  do{\
           time_t timep;\
           struct tm *p;\
           char time_str[40];\
           memset(time_str,'\0',40);\
           time(&timep);\
           p=localtime(&timep);\
           sprintf(time_str,"[%d.%02d.%02d %02d:%02d:%02d]",(1900+p->tm_year),(1+p->tm_mon),p->tm_mday,p->tm_hour,p->tm_min,p->tm_sec);\
           FILE *debug_fp = fopen("/tmp/tof", "a");\
           if (NULL != debug_fp){\
           fprintf(debug_fp, "%s L%d in %s, ",time_str, __LINE__, __FILE__);\
           fprintf(debug_fp, str, ##arg);\
           fflush(debug_fp);\
           fclose(debug_fp);\
           }\
}while(0)






typedef enum file_type{
	UNKNOW_FILE_TYPE = -1,
	ALL_FILE_TYPE,
	PHOTO_FILE_TYPE,
	VIDEO_FILE_TYPE,
	PHOTO_THUMB_FILE_TYPE,
	VIDEO_THUMB_FILE_TYPE,
	FILE_TYPE_MAX,
}ufile_type;


int user_folder_init(const char *dir,enum file_type type);
void user_folder_exit(void);
int user_folder_refresh(void);
void user_folder_file_list_free(void);
int user_folder_file_number_get(const char *folder_name);
int user_file_name_generate(enum file_type type, char name[]);
int _file_name_split(const char *full_name,char *dirname,char *basename);
int user_file_name_suffix_chang(char *name, const char *suffix);
int user_file_list_update(const char *full_name);
FILE *user_file_open(const char *full_name, const char *mode);
int user_file_close(const char *full_name, FILE *fp);
size_t user_file_write(const void *buffer, size_t size, size_t nsize, FILE *fp);
size_t user_file_read(char *buffer, size_t size, size_t nsize, FILE *fp);
int user_file_save(const char *buf, const unsigned int size, const char *full_name);
int user_file_data_get(char *name, unsigned int *size, char *buf);
int user_file_delete(const char *name);
int user_folder_file_list_get(const char *folder, unsigned int offset, unsigned int filenum, struct file_s *buf);
int user_folder_file_list_page_get(const char *folder, int pagenum, int *pagesize, struct file_s *buf);
void user_folder_list_printf(void);
int file_cmd_init(void);

#endif /*__FILE_MANAGE_H__*/
