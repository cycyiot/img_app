#ifndef __SWITCH_H_
#define __SWITCH_H_

#ifdef __cplusplus
extern "C" {
#endif


#define INI_TEMP_PATH							"./bin"
#define INI_CONFIG_FILE						"config.ini"
#define INI_CONFIG_BAK_FILE					"config.ini.bak"
#define CONFIG_PARTITION_PATH				"/dev/mtdblock3"
#define CONFIG_PARTITION_MOUNT_PATH			"/media/fat"

typedef enum {
	SWITCH_SPI2SDCARD = 0,
	SWITCH_SDCARD2SPI = 1,
	SWITCH_UNKOWN_ACT
}SWITCH_ACT_TYPE;

/*
NOTICE: 
when you call this function, you need to umount sdcard first. Because this function will switch from sdcard
to spi nor flash, and sdcard will not be used until this function return.
*/
extern int save_sys_param_to_nor_flash(void);

#ifdef __cplusplus
}
#endif

#endif

