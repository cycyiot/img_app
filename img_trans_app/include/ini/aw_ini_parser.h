#ifndef __AW_INI_PARSER__
#define __AW_INI_PARSER__

/***************************************************************************************
*
* Header File
****************************************************************************************/


/***************************************************************************************
*
* Macro Define
****************************************************************************************/
#define ASCIILINESZ     1024
#define INI_PATH        "/netPrivate/config.ini"
	
typedef enum {
	INI_VAL_TYPE_NUM, 
	INI_VAL_TYPE_STR,
	INI_VAL_TYPE_MAX,
}INI_VAL_TYPE;

/***************************************************************************************
*
* Structure Define
****************************************************************************************/
typedef struct {
	INI_VAL_TYPE type; 
	union {
		int num;
		char str[ASCIILINESZ + 1];
	}val;
}INI_VAL;
#ifdef __cplusplus
extern "C" {
#endif

/***************************************************************************************
*
* Function Declare
****************************************************************************************/

//int iniparser_init(const char *filename);
//void iniparser_exit(void);

/*-------------------------------------------------------------------------*/
/**
  @brief    Get the value type according to the `section` and the `key` from ini file.
  @param    section  Section of the value u wanted
  		   key      Key of the Section u wanted
  		   val      Return the value and the value type

  @return   int 0 if Ok, -1 otherwise.
 */
/*--------------------------------------------------------------------------*/

int get_ini_value(const char *section, const char *key, INI_VAL *val);

/*-------------------------------------------------------------------------*/
/**
  @brief    Set the value according to the `section` , the `key` and the value type from ini file.
  @param    section  Section of the value u wanted
  		   key      Key of the Section u wanted
  		   val      Will be set to ini 

  @return   int 0 if Ok, -1 otherwise.
 */
/*--------------------------------------------------------------------------*/

int set_ini_value(const char *section, const char *key, INI_VAL *val);

#ifdef __cplusplus
}
#endif
#endif // __AW_INI_PARSER__
