#ifndef _ACTION_MANAGER_H_
#define _ACTION_MANAGER_H_

#include <cedarx/frame_encode.h>
#include <camera/camera.h>
#include <liver/liver.h>
#include <msg_queue.h>

typedef enum {
	ACTION_STATE_INIT = 1,
	ACTION_STATE_RUNNING,
	ACTION_STATE_UNINIT,
	ACTION_STATE_MAX,
}ACTION_STATE_E;


typedef struct action_object_t {
	char *name;            /* action name */
	void* (*action_run)(struct action_object_t *p_action_obj, void *p_param); 
	void* (*action_run_asyn)(struct action_object_t *p_action_obj, void *p_param);
	void* (*action_run_exit)(struct action_object_t *p_action_obj, void *p_param);
	int (*action_get_event)(struct action_object_t *p_action_obj, void *p_cmd);  
	int (*action_put_event)(struct action_object_t *p_action_obj, void *p_cmd); 
	int (*action_get_state)(struct action_object_t *p_action_obj, ACTION_STATE_E *state);      
	struct action_object_t *next;
}action_object_t;

#ifdef __cplusplus
extern "C" {
#endif


int register_action_object(struct action_object_t *p_action_object);
void show_action_object(void);
struct action_object_t * action_object_get(const char *p_name);
void* action_run(struct action_object_t *p_action_obj, void *p_param);
void* action_run_asyn(struct action_object_t *p_action_obj, void *p_param);
void* action_run_exit(struct action_object_t *p_action_obj, void *p_param);
int action_get_event(struct action_object_t *p_action_obj, void *p_cmd);
int action_put_event(struct action_object_t *p_action_obj, void *p_cmd);
int action_get_state(struct action_object_t *p_action_obj, ACTION_STATE_E *p_state);


int action_object_init(void);

#ifdef __cplusplus
}
#endif



#endif
