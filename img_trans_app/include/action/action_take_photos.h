#ifndef __ACTION_RECORDER_H__
#define __ACTION_RECORDER_H__

struct action_take_photos_param{
	unsigned int num;
	unsigned int delay;
};

int take_photos_cmd_init(void);
void cmd_response_takephoto(const char * name,int result,const char * thumb_name);
int get_cmdParam_id(void);
#endif

