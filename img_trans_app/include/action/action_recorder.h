#ifndef __ACTION_RECORDER_H__
#define __ACTION_RECORDER_H__

inline int action_recorder_set_out_resolution(int width, int height);
inline int action_recorder_get_out_resolution(int *p_width, int *p_height);
int recorder_cmd_init(void);

#endif

