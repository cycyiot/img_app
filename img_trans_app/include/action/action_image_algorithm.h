#ifndef __ACTION_IMAGE_ALGORITHM_H__
#define __ACTION_IMAGE_ALGORITHM_H__

struct follow_me_parameter {
	int ret;
	int x0;
	int y0;
	int x1;
	int y1;
};

int image_algorithm_cmd_init(void);
int action_image_algorithm_start(void *param);
int action_image_algorithm_stop(void);
void follow_me_state_notify(struct follow_me_parameter *fm_param);

#endif

