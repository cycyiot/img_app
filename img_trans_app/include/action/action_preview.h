#ifndef __ACTION_PREVIEW_H__
#define __ACTION_PREVIEW_H__

#include <cedarx/frame_encode.h>
#include <liver/liver.h>
#include <liver/liveCommom.h>
#include <liver/liverDevice.h>
#include <semaphore.h>

#define IMAGE_TRANS_OUT_WIDTH	1280
#define IMAGE_TRANS_OUT_HEIGHT	720
#define IMAGE_TRANS_OUT_WIDTH_MIN	320
#define IMAGE_TRANS_OUT_HEIGHT_MIN	240
#define IMAGE_TRANS_OUT_WIDTH_MAX	1280
#define IMAGE_TRANS_OUT_HEIGHT_MAX	720

typedef struct{
	unsigned int width;
	unsigned int height;
}image_trans_fmt;

struct image_trans_packet_t {
	frame_enc_outinfo_st frm_out_info;
	image_trans_fmt format;
};

struct action_image_trans_context_t {
	struct frame_encode_context_st *p_frame_encode_ctx;
	FRM_OBJ                        *p_frm_obj;
	int								camera_id;
	struct camera_data             *p_camera_dev;
	WIFI_TRANS_CTX_T               *p_wifi_trans_dev;
	struct action_object_t         *p_action_take_photos;
	struct action_object_t         *p_action_recorder;
	struct action_object_t         *p_action_image_algorithm;
	struct image_trans_packet_t     image_trans_packet;
//	MSG_QUEUE_T                    *p_image_trans_queue;
	pthread_t						thread_id;
//	sem_t                           priew_wait;
	sem_t                           frame_wait;
	sem_t							wifi_trans_ready;
	volatile int                    running_flag;
	pthread_mutex_t mutex;
};

int preview_cmd_init(void);
int image_trans_resolution_set(int width, int height);
int action_image_trans_camera_switch(int camera_num);
int image_trans_camera_id_get(void);

#endif

