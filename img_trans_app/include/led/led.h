#ifndef __LED_H__
#define __LED_H__

#define LED_SWITCH_PATH	"/sys/class/leds/led1/switch"
#define LED_BLINK_PATH	"/sys/class/leds/led1/blink"
#define LED_DEF_STATE_PATH	"/sys/class/leds/led1/default_state"

#define LED_STATE_ON "1"
#define LED_STATE_OFF "0"

typedef enum {
	LED_BLINK_DISABLE,
	LED_BLINK_ON_OFF,
	LED_BLINK_OFF_ON,
}LED_BLINK_MODE;

struct led_dev {
	int fd_switch;
	int fd_blink;
	int fd_def_state;
	LED_BLINK_MODE mode;
};

int led_blink(unsigned int count);
int led_switch(unsigned int state);
int led_set_mode(LED_BLINK_MODE mode);
int led_init(void);
void led_uninit(void);
struct led_dev *led_get_handler(void);

#endif
