#ifndef	__FRAME_ENCODE_H__
#define	__FRAME_ENCODE_H__

typedef enum{
	FRM_ENCODE_JPEG = 0,
	FRM_ENCODE_H264,
}FRM_ENCODE_TYPE;

typedef void FRM_OBJ;

typedef struct frame_enc_outbuf{
	unsigned int size;
	unsigned char *buf;
}frame_enc_outbuf;

typedef enum h264_spec_params_index {
	BIT_RATE_INDEX = (1 << 0),
	FRM_RATE_INDEX = (1 << 1),
	MAX_KINT_INDEX = (1 << 3),
	IQP_OSET_INDEX = (1 << 4),
	ENI_FMON_INDEX = (1 << 5),
}h264_user_params_index;

typedef struct h264_spec_param_st {
	h264_user_params_index index;
	int bit_rate;
	int frame_rate;
	int vbv_size;
	int max_key_interval;
	int iqp_offset;
	int encode_iframe_on;
	unsigned int virtual_iframe;
}h264_spec_param_st;

typedef struct jpeg_exif_info_st{
	unsigned char *camera_manufact;
	unsigned char *camera_model;
	unsigned char *data_time;
}jpeg_exif_info_st;

typedef struct jpeg_spec_param_st{
	unsigned int thumb_enable;
	unsigned int thumb_width;
	unsigned int thumb_height;
	int			 vbv_size;
	struct jpeg_exif_info_st exif;
}jpeg_spec_param_st;

typedef struct frame_enc_config_st{
	int input_width;
	int input_height;
	int output_width;
	int output_height;
	union {
		struct h264_spec_param_st h264_spec;
		struct jpeg_spec_param_st jpeg_spec;
	}spec_param;	
}frame_enc_config_st;

typedef struct frame_enc_outinfo_st {
    unsigned int   size0;
    unsigned int   size1;
    unsigned char* p_data0;
    unsigned char* p_data1;
    unsigned char* p_priv;
}frame_enc_outinfo_st;
typedef struct frame_enc_head_st{
	int head_size;
	void *head_buf;
}frame_enc_head_st;


typedef struct frame_encode_context_st{
	FRM_ENCODE_TYPE type;
	struct frame_encode_context_st *next;
	FRM_OBJ *(*frame_enc_create_obj)(void);
	int      (*frame_enc_destroy_obj)(FRM_OBJ *obj);
	int      (*frame_enc_get_head_info)(FRM_OBJ *obj, struct frame_enc_head_st *p_head_info);
	int      (*frame_enc_config_obj)(FRM_OBJ *obj, frame_enc_config_st *cfg);
	int      (*frame_enc_get_vbvsize)(FRM_OBJ *obj, int *p_vbvsize);
	int      (*frame_enc_input_a_frame_by_phy_yc)(FRM_OBJ *obj, int phy_addr_y, int phy_addr_c);
	int      (*frame_enc_input_a_frame_by_phy)(FRM_OBJ *obj, int phy_addr);
	int      (*frame_enc_input_a_frame_by_vrt)(FRM_OBJ *obj, int vrt_addr);
	int      (*frame_enc_get_a_frame)(FRM_OBJ *obj, frame_enc_outinfo_st *out_info);
	int      (*frame_enc_put_a_frame)(FRM_OBJ *obj, frame_enc_outinfo_st *out_info);
	int 	 (*frame_enc_set_params_h264)(FRM_OBJ *obj, h264_spec_param_st *params);
	int		 (*frame_enc_get_thumbnail)(FRM_OBJ *obj, frame_enc_outinfo_st *out_info);
}frame_encode_context_st;


int frame_encode_context_register(struct frame_encode_context_st *p_frame_encode_context);
int frame_encode_context_unregister(struct frame_encode_context_st *p_frame_encode_context);
FRM_OBJ *frame_encode_create_obj(struct frame_encode_context_st *p_frame_encode_context);
int frame_encode_destroy_obj(struct frame_encode_context_st *p_frame_encode_context, FRM_OBJ *p_obj);
int frame_encode_get_head_info(struct frame_encode_context_st *p_frame_encode_context, FRM_OBJ *p_obj, struct frame_enc_head_st *p_head_info);
int frame_encode_config_obj(struct frame_encode_context_st *p_frame_encode_context, FRM_OBJ *p_obj, frame_enc_config_st *p_cfg);
int frame_encode_input_a_frame_by_phy(struct frame_encode_context_st *p_frame_encode_context, FRM_OBJ *p_obj, int phy_addr);
int frame_encode_input_a_frame_by_vrt(struct frame_encode_context_st *p_frame_encode_context, FRM_OBJ *p_obj, int vrt_addr);
int frame_encode_get_thumbnai_a_frame(struct frame_encode_context_st *p_frame_encode_context, FRM_OBJ *p_obj, frame_enc_outinfo_st*p_out_info);
int frame_encode_get_a_frame(struct frame_encode_context_st *p_frame_encode_context, FRM_OBJ *obj, frame_enc_outinfo_st *out_info);
int frame_encode_put_a_frame(struct frame_encode_context_st *p_frame_encode_context, FRM_OBJ *obj, frame_enc_outinfo_st *out_info);
int frame_encode_set_params_h264(struct frame_encode_context_st *p_frame_encode_context, FRM_OBJ *p_obj, h264_spec_param_st *param);


struct frame_encode_context_st * frame_encode_get_context(FRM_ENCODE_TYPE type);
int frame_encode_put_context(struct frame_encode_context_st *p_frame_encode_ctx);
void frame_encode_show_context_list(void);
#ifdef __cplusplus
extern "C" {
#endif

int frame_encode_init(void);
int frame_encode_uninit(void);
#ifdef __cplusplus
}
#endif


typedef enum{
	FRM_ENCODE_STATE_IDLE,
	FRM_ENCODE_STATE_PROCESSING,
	FRM_ENCODE_STATE_CMPL,
	FRM_ENCODE_STATE_MAX
}FRM_ENCODE_STATE;

int frm_encode_get_state(FRM_ENCODE_STATE *status);
int frm_encode_set_state(FRM_ENCODE_STATE status);

#endif

