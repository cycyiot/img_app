#ifndef _VIDEO_ENCODE_API_H_
#define _VIDEO_ENCODE_API_H_


struct CdxMuxerPacketS
{
    void *buf;
    int buflen;
    long long pts;
    long long duration;
    int type;
    int length;
    int streamIndex;
	int is_video;
};
typedef struct CdxMuxerPacketS CdxMuxerPacketT;



typedef enum VIDEO_ENCODE_TYPE
{
	VIDEO_ENCODE_H264,
	VIDEO_ENCODE_JPEG,
}VIDEO_ENCODE_TYPE;

typedef enum AUDIO_ENCODE_TYPE
{
     AUDIO_ENCODE_AAC_TYPE,
     AUDIO_ENCODE_LPCM_TYPE,    //only used by mpeg2ts
     AUDIO_ENCODE_PCM_TYPE,
     AUDIO_ENCODE_MP3_TYPE,
}AUDIO_ENCODE_TYPE;


#define ENCODE_FILE_PATH_LEN			128

#define ENC_MUXER_GET_LOOP_TIME			1
#define ENC_MUXER_GET_NEXT_NAME			2
#define ENC_MUXER_PUT_NEXT_NAME			3
#define ENC_MUXER_EXIT_MUXER_OK			4
#define ENC_MUXER_EXIT_FINALLY			5


typedef struct video_encode_config
{
	VIDEO_ENCODE_TYPE type;
	int             frame_rate;
	int             bit_rate;
    int             out_width;
    int				out_height;
    int				src_frame_rate;
    int				src_width;
    int				src_height;
	int             quality;
    int             use_phy_buf;
	int  (*video_queue_get)(void);
	int  (*video_queue_put)(void *);
}video_encode_config;

typedef struct audio_encode_config
{
	AUDIO_ENCODE_TYPE	type;
	int			    in_sample_rate;   
    int				in_chan;         
    int				bit_rate;        
    int				sampler_bits;    
    int				out_sample_rate;  
    int				out_chan;        
    int				frame_style;  
	int  (*audio_queue_get)(void);
	int  (*audio_queue_put)(void *);
}audio_encode_config;

typedef struct encode_config
{
	video_encode_config *video_cfg;
	audio_encode_config *audio_cfg;
	int  (*enc_queue_get)(void);
	int  (*enc_queue_put)(void *);
	void (*enc_queue_post)(void);
	int  (*device_is_online)(void);	
	int  (*encode_callback)(int cmd, void *arg);
	int  (*enc_vid_data_out)(char *buf, int size);
}encode_config;



int video_encode_init(encode_config *enc_cfg, char *path);
int video_encode_uninit(void);
int video_encode_write_yuv(unsigned char *buf, int size, long long pts);
int video_encode_write_pcm(unsigned char *buf, int size, long long pts);
int video_encode_set_frame_rate(int rate);
int video_encode_get_encoding_time(void);
int video_encode_get_encoding_path(char *path, int size);
int register_enc_jpg(int (*enc_jpg_fun)(void *, int));
int unregister_enc_jpg(void);


#endif

