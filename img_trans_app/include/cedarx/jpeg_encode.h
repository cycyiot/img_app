#ifndef	__ENCODE_JPEG_H__
#define	__ENCODE_JPEG_H__


typedef void* cdx_mem_ops;
typedef void* cdx_ve_ops;

#ifndef JPEG_ENCODE_LIB

typedef enum VENC_PIXEL_FMT {
    VENC_PIXEL_YUV420SP,
    VENC_PIXEL_YVU420SP,
    VENC_PIXEL_YUV420P,
    VENC_PIXEL_YVU420P,
    VENC_PIXEL_YUV422SP,
    VENC_PIXEL_YVU422SP,
    VENC_PIXEL_YUV422P,
    VENC_PIXEL_YVU422P,
    VENC_PIXEL_YUYV422,
    VENC_PIXEL_UYVY422,
    VENC_PIXEL_YVYU422,
    VENC_PIXEL_VYUY422,
    VENC_PIXEL_ARGB,
    VENC_PIXEL_RGBA,
    VENC_PIXEL_ABGR,
    VENC_PIXEL_BGRA,
    VENC_PIXEL_TILE_32X32,
    VENC_PIXEL_TILE_128X32,
}VENC_PIXEL_FMT;

typedef void* VideoEncoder;

typedef struct VencBaseConfig {
    unsigned int        nInputWidth;
    unsigned int        nInputHeight;
    unsigned int        nDstWidth;
    unsigned int        nDstHeight;
    unsigned int        nStride;
    VENC_PIXEL_FMT      eInputFormat;
    cdx_mem_ops 		*memops;
	cdx_ve_ops			*veOpsS;
    void*             pVeOpsSelf;
    unsigned char     bOnlyWbFlag;	
}VencBaseConfig;

typedef struct FrameInfo {
    int             CurrQp;
    int             avQp;
    int             nGopIndex;
    int             nFrameIndex;
    int             nTotalIndex;
}FrameInfo;

typedef struct VencOutputBuffer {
    int               nID;
    long long         nPts;
    unsigned int   nFlag;
    unsigned int   nSize0;
    unsigned int   nSize1;
    unsigned char* pData0;
    unsigned char* pData1;

    FrameInfo       frame_info;
}VencOutputBuffer;

#endif


typedef struct ven_jpg_obj_st{
	VideoEncoder *pve;
	cdx_mem_ops *ops;
//	h264_param  *param;
	int          input_width;
	int          input_height;
}ven_jpg_obj_st;

typedef struct ven_thumb_cfg_st{
	unsigned int 		ThumbEn;
	unsigned int		ThumbWidth;
	unsigned int		ThumbHeight;
}ven_thumb_cfg_st;
typedef struct ven_jpg_exif_st{
	unsigned char *camera_manufac;
	unsigned char *camera_model;
	unsigned char *data_time;
	unsigned int  fn_number;
	unsigned int  expo_time;	/* ms */
	unsigned int  iso_speed;
	unsigned int  focal_len;	
}ven_jpg_exif_st;
typedef struct ven_jpg_cfg_st {
	VencBaseConfig baseConfig;
	int            vbv_size;
	int            quality;
	int            frame_rate;
	int            bit_rate;
	int            bitRateMax;
	int            bitRateMin;	
	ven_thumb_cfg_st ThumbCfg;
	ven_jpg_exif_st exif_cfg;
}ven_jpg_cfg_st;

#ifdef	__cplusplus
extern "C"
{
#endif

struct ven_jpg_obj_st * jpeg_encode_create(void);
int jpeg_encode_destroy(struct ven_jpg_obj_st *p_decoder);
int jpeg_encode_config(struct ven_jpg_obj_st *p_decoder, struct ven_jpg_cfg_st *cfg);
int jpeg_encode_set_bitrate(struct ven_jpg_obj_st *p_decoder, struct ven_jpg_cfg_st *cfg);
int jpeg_encode_set_frame_rate(struct ven_jpg_obj_st *p_decoder, int frame_rate);
int jpeg_encode_get_vbv_info(struct ven_jpg_obj_st *p_decoder, int *p_vbvsize);
int jpeg_encode_input_a_frame_by_phy_yc(struct ven_jpg_obj_st *p_decoder, int phy_addr_y, int phy_addr_c);
int jpeg_encode_get_one_bitstream_frame(struct ven_jpg_obj_st *p_decoder, VencOutputBuffer *pBuffer);
int jpeg_encode_put_one_bitstream_frame(struct ven_jpg_obj_st *p_decoder, VencOutputBuffer *pBuffer);

int jpeg_encode_get_thumbnail_data(struct ven_jpg_obj_st *p_decoder, unsigned int *addr, unsigned *size);

#ifdef	__cplusplus
}
#endif

#endif
