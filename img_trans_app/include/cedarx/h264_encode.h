#ifndef	__H264_ENCODE_H__
#define	__H264_ENCODE_H__



typedef void* VidEnc;
typedef void* cdx_mem_ops;
typedef void* cdx_ve_ops;
typedef void* h264_param;

#ifndef H264_ENCODE_LIB


typedef struct VencHeaderData {
    unsigned char*  pBuffer;
    unsigned int    nLength;
}VencHeaderData;


typedef enum VENC_PIXEL_FMT {
    VENC_PIXEL_YUV420SP,
    VENC_PIXEL_YVU420SP,
    VENC_PIXEL_YUV420P,
    VENC_PIXEL_YVU420P,
    VENC_PIXEL_YUV422SP,
    VENC_PIXEL_YVU422SP,
    VENC_PIXEL_YUV422P,
    VENC_PIXEL_YVU422P,
    VENC_PIXEL_YUYV422,
    VENC_PIXEL_UYVY422,
    VENC_PIXEL_YVYU422,
    VENC_PIXEL_VYUY422,
    VENC_PIXEL_ARGB,
    VENC_PIXEL_RGBA,
    VENC_PIXEL_ABGR,
    VENC_PIXEL_BGRA,
    VENC_PIXEL_TILE_32X32,
    VENC_PIXEL_TILE_128X32,
}VENC_PIXEL_FMT;


typedef struct VencBaseConfig {
    unsigned int        nInputWidth;
    unsigned int        nInputHeight;
    unsigned int        nDstWidth;
    unsigned int        nDstHeight;
    unsigned int        nStride;
    VENC_PIXEL_FMT      eInputFormat;
    cdx_mem_ops 		*memops;
	cdx_ve_ops			*veOpsS;
    void*             pVeOpsSelf;
    unsigned char     bOnlyWbFlag;
}VencBaseConfig;

typedef struct FrameInfo {
    int             CurrQp;
    int             avQp;
    int             nGopIndex;
    int             nFrameIndex;
    int             nTotalIndex;
}FrameInfo;

typedef struct VencOutputBuffer {
    int               nID;
    long long         nPts;
    unsigned int   nFlag;
    unsigned int   nSize0;
    unsigned int   nSize1;
    unsigned char* pData0;
    unsigned char* pData1;

    FrameInfo       frame_info;
}VencOutputBuffer;


#endif

typedef struct ven_obj{
	VidEnc      *pve;
	cdx_mem_ops *ops;
	h264_param  *param;
	int          input_width;
	int          input_height;
}ven_obj;

typedef struct ven_cfg {
	VencBaseConfig baseConfig;
	int            bit_rate;
	int            frame_rate;
	int            vbv_size;
	int            max_key_interval;
	unsigned int   virtual_iframe;
}ven_cfg;

#ifdef	__cplusplus
extern "C"
{
#endif
	
struct ven_obj * h264_encode_create(void);
int h264_encode_config(struct ven_obj *p_decoder, struct ven_cfg *cfg);
int h264_encode_get_sps_pps(struct ven_obj *p_decoder, VencHeaderData *p_sps_pps_data);
int h264_encode_destroy(struct ven_obj *p_decoder);
int h264_encode_get_vbv_info(struct ven_obj *p_decoder, int *p_vbvsize);
int h264_encode_is_venc_field(struct ven_obj *p_decoder);
int h264_encode_input_a_frame_by_phy(struct ven_obj *p_decoder, int phy_addr);
int h264_encode_input_a_frame_by_phy_yc(struct ven_obj *p_decoder, int phy_addr_y, int phy_addr_c);
int h264_encode_get_one_bitstream_frame(struct ven_obj *p_decoder, VencOutputBuffer *pBuffer);
int h264_encode_put_one_bitstream_frame(struct ven_obj *p_decoder, VencOutputBuffer *pBuffer);

int h264_encode_set_bitrate(struct ven_obj *p_decoder, int bit_rate);
int h264_encode_set_frame_rate(struct ven_obj *p_decoder, int frame_rate);
int h264_encode_set_IQpOffset(struct ven_obj *p_decoder, int qp);
int h264_encode_set_encode_Iframe(struct ven_obj *p_decoder, int on);
int h264_encode_set_max_frame_interval(struct ven_obj *p_decoder, int max_frame_interval);

#ifdef	__cplusplus
}
#endif

#endif

