#ifndef __BATTERY_H_
#define __BATTERY_H_

enum battery_charge_state{
	NO_INSERT,
	CHARGING,
	CHARGED,
};

struct battery_info {
	unsigned int voltage;
	enum battery_charge_state state;
};

#define BATTERY_MAX_VOLTAGE 8400

int battery_init(void);
int battery_uninit(void);
int battery_info_get(struct battery_info *battery);
enum battery_charge_state battery_is_charging(void);

#endif
