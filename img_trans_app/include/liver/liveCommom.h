#ifndef LIVER_COMMON_H
#define LIVER_COMMON_H
#ifdef __cplusplus
extern "C" {
#endif
#define  JPEG_STREAM           ((1<<0)&0XFF)
#define  PCM_STREAM            ((1<<1)&0XFF)
#define  VIDEOJPEG_STREAM      ((1<<2)&0XFF)
#define  H264_STREAM           ((1<<3)&0XFF)
#define  FILE_NAME_NUMER	(60)

typedef signed int (*INSTREAM)(unsigned int *buff) ;
typedef signed int (*OUTSTREAM)(unsigned int buff) ;
typedef int (*FRAME)(void *buff,int len) ;
//typedef int (*PCMFRAME)(void *buff,unsigned int len) ;
//typedef int (*VIDEOFRAME)(void *buff, long len);
typedef int (*LINKCOM)(void *Comd);

typedef struct Command{
	char key[100];
	char str[100];
	char cmd[60];
	long int value;
	int  boolen;
	int type;
	int lock;
	//int jsonType;
}JsonCommand;
typedef enum {
	// Sensor Resolution
	JSON_INT=0,
	JSON_STRING,
	JSON_BOOLEN,
	JSON_ARRY
}JsonType;


typedef struct Img {
	unsigned int QFactor;
	unsigned int width;
	unsigned int height;
}imgJpeg;

typedef struct Audio{
	unsigned int audioFormat;
    unsigned int numChannels;
    unsigned int samplingFrequency;
	unsigned int bitsPerSample;
}audioPcm;
typedef struct h264{
	void *buff;
	
	unsigned int size;
}h264Head;

typedef enum{
 	TAKEPICTURE=0,	        
 	FILELOCK	,  
 	FILEUNLOCK	,
 	RECORDING  ,       
	RESOLUTION1	   ,     
	WHITEBALANCE  ,     
	COLORSETTING  ,    
	VOICE,	        
	SHOWDATE 	,       
	GRAVITYSENSOR  ,  
	RECORDINGTIME ,
	RECORDINGWHENSTART ,
	PREVIEW		,	
	
	PHOTO_THUMB_DATA,
	PHOTO_THUMB_PAGE_DEC,
	VIDEO_THUMB_DATA,
	VIDEO_THUMB_PAGE_DEC,
	
	PLAYVIDEO 	,
	DELETEFILE		,
	SETSYSDATE	,
	GETSYSTEMDATE      ,
	SETCARNUMBER  ,   
	GETCARNUMBER     ,
	GETCONTAINERSIZE  , 
 	RESETSD	,	
 	PARAMCORRECT,
 	VERSIONINFO      
}appCom;

#define CommandNum			26

typedef enum {
 	CARNUMBER =0  ,        		
	PHOTOFILENUM , 
 	PHOTOFILENAME ,      	
 	VIDEOFILENUM,	    
 	VIDEOFILENAME ,      		
 	SYSDATE	     ,       	
 	SYSTIME    ,        	
 	CARTATOSIZE	,       	
 	CARUSEDSIZE	 ,      
	CARDFREESIZE        
}mcuCom;

#define SERCOMMANDNUM			10
typedef enum{
	PHOTO_PREVIEW=0,
	VIDEO_PREVIEW,
	LOOKBACK_VIDEO_PREVIEW,
	REALTIME_PREVIEW,
	STOP_REALTIME_PREVIEW
}appPreview;

#ifdef __cplusplus
}
#endif
#endif
