#ifndef MIDDLE_LIVER_H
#define MIDDLE_LIVER_H

#ifdef __cplusplus
extern "C" {
#endif 

#include <liver/liveCommom.h>
// #include "../UsageEnvironment/BasicUsageEnvironment.hh"
// extern UsageEnvironment* env;
void *midLiverThread(void *buff);
int  midLiverRegisterFrameCb(FRAME stream,int type);
void midLiverRegerterLinkCom(LINKCOM getlinkCom);
int midLivergetTriggerId(int type);
void midLiverTriggerId(int id );
void midLiverInf(imgJpeg *imgInf,audioPcm *audInf);
int midLiverStreamInf(h264Head *h264Inf);
int  midLiverRegisterStreamCb(INSTREAM inCb ,OUTSTREAM outCb, int type);
void midLiverGetInf(imgJpeg *imgInf, audioPcm *audInf);
void midLiverClose(void);
void  midLiverCloseReset(void);



#ifdef __cplusplus
}
#endif 

#endif 

