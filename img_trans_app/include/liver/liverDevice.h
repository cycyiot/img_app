#ifndef LIVER_DEVICE_H
#define LIVER_DEVICE_H
#ifdef __cplusplus
extern "C"{
#endif

#include <liver/liveCommom.h>

/*******************************
start the liver and create a thread for 
liver device
********************************/
int liverCreate(void);


/*******************************
desytroy the resource and thread;

********************************/
void liverDesytroy(void);

/*******************************
live 555 need the media information
 set the information  so that set to 
 the liver
********************************/
int liverSetInf( imgJpeg  *jpegInf,  audioPcm  *pcmInf);
int liverSetStreamParam(h264Head *h264Param);

/*******************************
driver the liver send the data
save to the threads to call

********************************/
void liverTrigger(int id);

 
/*******************************
now have two stream  mjpeg and id 
get the triggerID

********************************/
int  liverGetTriggerID(int type);


/*******************************
registerTheCallback
********************************/
void liverRegisterFrameCb(FRAME stream,int type );
void  liverRegisterLinkCB(LINKCOM linkStatus);

int liverGetStreamHead(h264Head *h264Param);
void liverLiverRegisterStreamCb(INSTREAM inCb,OUTSTREAM outCb,int type);

void  liverClose(void);
void liverReset (void);




/*******************************
registerTheCallback

********************************/

#ifdef __cplusplus
}

#endif //__cplusplus 
#endif
