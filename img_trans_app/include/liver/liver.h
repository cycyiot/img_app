#ifndef __WIFI_INTERFACE_H_
#define __WIFI_INTERFACE_H_

typedef void WIFI_TRANS_CTX_T;

typedef   void (*TRANS_CONTROL)(void);
#define  TRANS_START  (0X01)
#define  TRANS_STOP   (0X02)

struct wifi_trans_context_st {
	int (*wifi_get_liver_status)(void *cmd);
	int (*wifi_liver_send_Jpg_cb)(void *fTo, int maxsize);
	int (*wifi_liver_ready)(void *p_param);
	TRANS_CONTROL liver_trans_start;
	TRANS_CONTROL liver_trans_stop; 
	volatile int jpg_trigger_id;
	volatile int video_jpg_trigger_id;
	volatile int h264_trigger_id;
};

WIFI_TRANS_CTX_T * wifi_trans_create(void);
int wifi_trans_register_liver_status(WIFI_TRANS_CTX_T *p_ctx, int (*wifi_get_liver_status)(void *cmd));
int wifi_trans_register_liver_send(WIFI_TRANS_CTX_T *p_ctx, int (*wifi_liver_send_Jpg_cb)(void *fTo, int maxsize));
int wifi_trans_register_liver_ready(WIFI_TRANS_CTX_T *p_ctx, int (*wifi_liver_ready)(void *p_param));
int wifi_trans_get_trigger_id(WIFI_TRANS_CTX_T *p_ctx);
int wifi_trans_trigger(WIFI_TRANS_CTX_T *p_ctx);
int wifi_trans_register_liver_control(WIFI_TRANS_CTX_T *p_ctx,int type,TRANS_CONTROL trans);



#endif
