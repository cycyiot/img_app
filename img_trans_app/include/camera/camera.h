#ifndef __CAMERA_H
#define __CAMERA_H
#ifdef __cplusplus
extern "C" {
#endif

#include <platform_cfg.h>
#include "videodev2.h"
#include <pthread.h>
#include <net/cmd_handle.h>

#define CAMERA_FRAME_BUF_NUM 4
#define VID_FPS 30
#define IMG_CAP_MAX_NUM 15
#define IMG_CAP_WIDTH 3264
#define IMG_CAP_HEIGHT 2448
#define VID_CAP_WIDTH 1632
#define VID_CAP_HEIGHT 928

//#define IMG_CAP_WIDTH 1280
//#define IMG_CAP_HEIGHT 720
//#define VID_CAP_WIDTH 1280
//#define VID_CAP_HEIGHT 720

#define FM_CAP_WIDTH 320
#define FM_CAP_HEIGHT 240
#define	IMG_THUMB_WIDTH 320
#define IMG_THUMB_HEIGHT 240
#define VID_THUMB_WIDTH (IMG_THUMB_WIDTH)
#define VID_THUMB_HEIGHT (IMG_THUMB_HEIGHT)

//#ifdef FLOW_USED
#if 1
#define VID1_CAP_WIDTH 640
#define VID1_CAP_HEIGHT 480
#endif

//#define FB_SIZE ((ALIGN_16B(VID_CAP_WIDTH))*(ALIGN_16B(VID_CAP_HEIGHT))*3/2)

struct camera_parameter{
	int ae;
	int awb;
	int brightness;
	int contrast;
};

struct photograph_parameter {
	unsigned int width;
	unsigned int height;
	unsigned int fps;
	unsigned int format;
};

struct camera_data{
	int id;
	int fd;
	volatile int running_state;
	unsigned int width;
	unsigned int height;
	unsigned int fps;
	unsigned int format;
	unsigned int mode;
	unsigned int buf_len;
	void *virtual_buf[CAMERA_FRAME_BUF_NUM];
	struct v4l2_buffer phy_buf;
	struct camera_parameter sys_param;
	struct photograph_parameter photograph;
	struct VideoOpr *opr;
	pthread_mutex_t mutex;
	pthread_cond_t frm_update;
	struct camera_data *ptNext;
};

struct VideoOpr {
    int (*InitDevice)(struct camera_data *VideoDevice);
    void (*ExitDevice)(struct camera_data *VideoDevice);
    int (*GetFrame)(struct camera_data *VideoDevice);
    int (*StartDevice)(struct camera_data *VideoDevice);
    int (*StopDevice)(struct camera_data *VideoDevice);
	int (*CtrlSet)(struct camera_data *VideoDevice, int ctrlId, int ctrlVal);
	int (*CtrlGet)(struct camera_data *VideoDevice, int ctrlId, int *value);
	int (*InitTakePicture)(struct camera_data *VideoDevice);
	int (*ExitTakePicture)(struct camera_data *VideoDevice);
	int (*StartTakePicture)(struct camera_data *VideoDevice);
	int (*StopTakePicture)(struct camera_data *VideoDevice);
};

int register_camera_opr(struct VideoOpr *VideoOpr);
int camera_mgr_init(void);
void camera_opr_init(struct camera_data *VideoDevice);
int camera_control(CMD cmd_id, int param);
int camera_set_default_param(void);
int camera_param_load(void);
int camera_frame_get(struct camera_data *VideoDevice);
struct camera_data *camera_get_video_device(const int id);
int camera_cmd_init(void);

#ifdef __cplusplus
}
#endif

#endif
