#ifndef __MSG_QUEUE_H__
#define __MSG_QUEUE_H__
#ifdef __cplusplus
extern "C" {
#endif

/***************************************************************************************
*
* Header File
****************************************************************************************/
#include <pthread.h>
#include <semaphore.h>
#include "platform_cfg.h"

/***************************************************************************************
*
* Macro Define
****************************************************************************************/
//#define MSG_MAX 				10
typedef enum MSG_TYPE_S{
	MSG_TYPE_BLOCK,
	MSG_TYPE_NONBLOCK,
	MSG_TYPE_MAX,
}MSG_TYPE_T;

/***************************************************************************************
*
* Structure Define
****************************************************************************************/
typedef struct MSG_QUEUE_S{
	//INT32U msg_addr[MSG_MAX];
	INT32U *msg_addr;
	INT32U msg_num;
	INT32U r_idx;
	INT32U w_idx;
	MSG_TYPE_T msg_type;
	pthread_mutex_t lock;    
	sem_t  wait;
}MSG_QUEUE_T;


/***************************************************************************************
*
* Function Declare
****************************************************************************************/
MSG_QUEUE_T *msg_queue_create(INT32U msgNum, MSG_TYPE_T msgType);
INT32S msg_queue_destory(MSG_QUEUE_T *pMsgQ);
INT32S msg_queue_type_set(MSG_QUEUE_T *pMsgQ, MSG_TYPE_T msgType);
INT32S msg_queue_is_empty(MSG_QUEUE_T *pMsgQ);
INT32S msg_queue_is_full(MSG_QUEUE_T *pMsgQ);
INT32S msg_queue_get_used_count(MSG_QUEUE_T *pMsgQ);
INT32S msg_queue_put(MSG_QUEUE_T *pMsgQ, INT32U msgAddr);
INT32U msg_queue_get(MSG_QUEUE_T *pMsgQ);
INT32S msg_queue_flush(MSG_QUEUE_T * pMsgQ);

#ifdef __cplusplus
}
#endif


#endif
