#ifndef __FLOW_H__
#define __FLOW_H__

#include <stdint.h>  //uint8_t ...

//光流算法参数
typedef struct{
	uint16_t width;    //图像分辨率-宽。默认是640
	uint16_t height;   //图像分辨率-高。默认是480
	uint8_t  frate;    //摄像头帧率。默认是50FPS
    uint8_t  optimize; //优化等级。默认是1
	uint8_t  record;   //录制视频的类型
    const char *directory;  //录制视频的目录
    uint8_t  print;    //是否打印光流数据
} usr_args_t;

//光流运算结果
typedef struct {
	uint16_t version;   //光流算法库的版本号
	float    vx;    //x轴的光流速度
	float    vy;    //y轴的光流速度
	uint8_t  confidence;    //光流数据可信度，范围为0~100，0表示最差，100表示最好
    //以下三个变量从不同角度描述了图像的亮度。选用三者之一即可。
    uint8_t  image_brightness;  //图像亮度：0~255，0为最暗（全黑），255为最亮（全白）
    uint8_t  image_score;       //图像评分：0~100，0为最差（过暗或过亮），100为最好（亮度适中）
    int8_t   image_grade;       //图像等级：-1为过暗，0为正常，1为过亮
	float time_loop;
	float time_run;
} usr_result_t;

//功能: 获取光流算法库的版本号
//参数: 无
//返回: 光流算法库的版本号，如 110 表示 V1.1.0
uint16_t flow_get_version(void);

//功能: 初始化光流模块
//参数: usr_args - 光流参数
//返回:  0 - 成功
//      -1 - 失败
int flow_init(const usr_args_t *usr_args);

//功能: 获取光流数据
//参数: image - 输入参数: 参与运算的图像数据（灰度数据或Y分量）
//      usr_result - 输出参数: 光流算法的输出结果
//返回:  0 - 成功
//      -1 - 失败
int flow_get_data(const uint8_t *image, usr_result_t *usr_result);

//功能: 反初始化光流模块
//参数: 无
//返回: 无
void flow_deinit(void);

#ifdef __cplusplus
extern "C" {
#endif
int flow_thread_init(void);
#ifdef __cplusplus
}
#endif

#endif
