#ifndef TCP_CLIENT_CONNETION_HH
#define TCP_CLIENT_CONNETION_HH
#ifdef __cplusplus
extern "C" {
#endif
#include <sys/types.h> 
#include <netinet/in.h>

#define BUFFER_SIZE    (1024)
#define NET_CLIENT_NUM (2)

int  TcpClientOpen(void *env, int clientSocket, void *clientAddr);//struct sockaddr_in clientAddr);
void TcpClientClose(int clientSocket);
void TcpClientReceive(void *, int);
int TcpClientInt( void);
int TcpClientActiveCount(void);

//void TcpClientSend(char *buf, unsigned int size);
int TcpClientSend(int socket , const char *buf, unsigned int size);

void *TcpClientManageAdd(int socket,void *value);
void* TcpClientManageLookUp(int socket);
int TcpClientManageRmove(int socket);

typedef struct{
	int ourSocket;
	struct sockaddr_in clientAddr;
	char RequestBuffer[BUFFER_SIZE];
	int active;
}ClientContex;

#ifdef __cplusplus
}
#endif

#endif 
