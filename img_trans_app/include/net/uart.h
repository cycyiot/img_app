#ifndef	__UART_H__
#define __UART_H__ 

//#define	UART_DEMO
#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
	CMD_DATA_WRITE,
	CMD_DATA_READ,
	CMD_MAX
}data_dir; 	

typedef enum {
	UART0,
	UART1,
	UART2,
	UART_MAX
}uart_number;

typedef struct _uart_param{
	unsigned char databits;
	unsigned char parity;
	unsigned char stopbits;
	unsigned char flow_ctrl;
	unsigned int baud;
	uart_number id;
}uart_param;
int uart_open(const char *name);

int uart_close(const int fd);


int uart_params_set(const char *name, uart_param *para);
unsigned int uart_data_transfer(const char *name, data_dir dir, char *buf, unsigned int len);
#ifdef __cplusplus
}
#endif

#endif
