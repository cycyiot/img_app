#ifndef __WIFI_H
#define __WIFI_H
#include <sys/socket.h> 
#include <arpa/inet.h>
#include <net/TcpClientConnection.hh>
#include <net/wifi.h>

struct wifi_rssi{
	int rssi;
	int client;
};

void wifi_start(void);
void wifi_stop(void);
int wifi_set_default_param(void);
int set_ssid(const char *ssid);
int set_phrase(const char * phrase);
int wifiParmRead(const void *param, void *value);
int wifiParmSave(void *param, void *value);
void wifi_station_notify(void);
int wifi_station_report_register(int client);
int wifi_station_report_unregister(int client);
int wifi_rssi_get(int num, struct wifi_rssi *wifi_rssi) ;
int wifi_cmd_init(void);
int wifi_init(void);

#endif
