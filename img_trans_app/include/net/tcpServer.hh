#ifndef TCP_SERVIER_HH
#define TCP_SERVIER_HH
#include "GroupsockHelper.hh"
#include "BasicUsageEnvironment.hh"
#include "tcpServer.hh"
#include <pthread.h>
#include "TcpClientConnection.hh"
#include "stdio.h"
#include <netinet/tcp.h>
#include "platform_cfg.h"

#ifdef __cplusplus
extern "C" {
#endif

#define SERVER_SND_BUF_SIZE		 (128 * 1024)

typedef struct{
	//ClientContex *client[NET_CLIENT_NUM];
	unsigned int ClientCount;
	//unsigned curIndex;
	int ourSockets[NET_CLIENT_NUM];
	HashTable* SocketTable;
	UsageEnvironment* Env;
	pthread_mutex_t mutex;
}ClientLink;

int tcpServerCreate(void);
void tcpServerDestroy(void);
int  tcpServerAppSetUp(void);
void tcpServerAppDown(void);
void *tcpServerThread(void *data);
void connectionHandler(void *data, int mas);

#ifdef __cplusplus
}
#endif

#endif
