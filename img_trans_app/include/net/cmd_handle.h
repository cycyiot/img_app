#ifndef __CMD_HANDLE_H
#define __CMD_HANDLE_H

#include <net/TcpClientConnection.hh>
#include <cmd.h>
#include <json/json.h>
#include <json/bits.h>

struct cmd_param{
	CMD id;
	int client;
	struct json_object *param;
};

struct cmd{
	CMD id;
	int (*handler)(struct cmd_param *param);
	struct cmd *ptNext;
};

void cmd_handle(ClientContex *socketData);
void cmd_response(int result, struct cmd_param *cmdParam);
void cmd_response_steam(int result,struct cmd_param * cmdParam,const char * name,const char * thumb_name);
int cmd_init(void);
int cmd_register(struct cmd cmd[], int num);

#endif
