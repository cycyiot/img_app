#include <liver/liverDevice.h>
#include <liver/middleLiver.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <platform_cfg.h>

typedef struct {
	imgJpeg jpegInf;
	audioPcm pcmInf;
	FRAME jpegFrame;
	FRAME h264FrameStream;
	FRAME audioFrame;
	FRAME videoFrame;
	LINKCOM linkStatus;
	pthread_t LiveThreadId;
}liverContex;

static liverContex *gliver=NULL;

int liverCreate(void)
{
	int ret;

	gliver = (liverContex *)malloc(sizeof(*gliver));
	if(gliver == NULL){
		PRINTF("liver Init fail\n");
		return -1;
	}
	
	ret = pthread_create(&gliver->LiveThreadId, NULL, midLiverThread, NULL);
	if(ret < 0){
		PRINTF("liver creat the thread fail\n");
	}else{
		PRINTF("liver running\n");
	}
	
	return ret;	
}

void liverDesytroy(void)
{
	pthread_cancel(gliver->LiveThreadId);
	if(gliver){
		free(gliver);
		gliver = NULL;
	}
}

int liverSetInf(imgJpeg *jpegInf, audioPcm *pcmInf)
{
	if(jpegInf != NULL){
		PRINTF("jpegInf\n");
		gliver->jpegInf = *jpegInf;
		midLiverInf(&gliver->jpegInf, NULL);
		PRINTF("midLiverInf\n");
	}
	if(pcmInf != NULL){
		PRINTF("pcmInf\n");
		gliver->pcmInf = *pcmInf;
		midLiverInf(NULL, &gliver->pcmInf);
	}
	
	return 0;
}

int liverSetStreamParam(h264Head *h264Param)
{
	return midLiverStreamInf(h264Param);
}

void liverTrigger(int id)
{
	midLiverTriggerId(id);
}

int liverGetTriggerID(int type)
{
	return midLivergetTriggerId(type);
}

void liverRegisterFrameCb(FRAME stream,int type )
{
	midLiverRegisterFrameCb(stream,type);
}

void liverRegisterLinkCB(LINKCOM linkStatus)
{
	gliver->linkStatus = linkStatus;
	midLiverRegerterLinkCom(gliver->linkStatus);
}

void liverLiverRegisterStreamCb(INSTREAM inCb,OUTSTREAM outCb,int type)
{
	 midLiverRegisterStreamCb(inCb ,outCb, type);
}

void  liverClose(void)
{
	midLiverClose();
}

void  liverReset(void)
{
	midLiverCloseReset();
}
