#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h> 
#include <errno.h>
#include <malloc.h>
#include <linux/fb.h>  
#include <sys/mman.h>
#include <pthread.h>
#include <fcntl.h> 
#include <liver/liverDevice.h>
#include <liver/liver.h>
#include <platform_cfg.h>

typedef enum{
	LIVE_JPG_CLIENT_QUIT = 0,
	LIVE_JPG_CLIENT_ACESS,
	
	LIVE_VIDEO_PCM_QUIT,
	LIVE_VIDEO_PCM_ACESS,

	LIVE_VIDEO_IMG_QUIT,
	LIVE_VIDEO_IMG_ACESS,

	LIVE_H264_IMG_QUIT,
	LIVE_H264_IMG_ACESS,
	LIVE_JPG_UPDATE = 101,
	LIVE_VIDEO_JPG_UPDATE,
	LIVE_VIDEO_PCM_UPDATE,
	LIVE_VIDEO_H264_UPDATE,
	
	LIVE_JPG_READY_SEND=201,
	LIVE_JPG_SEND_FINSH
	
}Live_Status;

static struct wifi_trans_context_st *gp_wifi_trans_ctx = NULL;

int liver_status_cb(void *Comd)
{
	int *value = (int *)Comd;
	h264Head h264pps;
	
	switch(*value){
	case LIVE_JPG_CLIENT_ACESS:	
		 gp_wifi_trans_ctx->jpg_trigger_id= liverGetTriggerID(JPEG_STREAM);
		 PRINTF("%s:jpgTriggerID = %d\r\n", __func__, gp_wifi_trans_ctx->jpg_trigger_id);
		 break;

	case LIVE_VIDEO_IMG_ACESS:
		 gp_wifi_trans_ctx->video_jpg_trigger_id = liverGetTriggerID(VIDEOJPEG_STREAM);
		 PRINTF("%s:videoJpgTriggerID = %d\r\n", __func__, gp_wifi_trans_ctx->video_jpg_trigger_id);
		 break;
	case LIVE_H264_IMG_QUIT:
		PRINTF("LIVE_H264_IMG_QUIT\n");
		gp_wifi_trans_ctx->h264_trigger_id = 0;
		gp_wifi_trans_ctx->liver_trans_stop();
		break;
	case LIVE_H264_IMG_ACESS :
		gp_wifi_trans_ctx->h264_trigger_id = liverGetTriggerID(H264_STREAM);
		PRINTF("%s:H264TriggerID = 0x%x\r\n", __func__, gp_wifi_trans_ctx->h264_trigger_id);
		gp_wifi_trans_ctx->liver_trans_start();
		gp_wifi_trans_ctx->wifi_get_liver_status(&h264pps);
		liverSetStreamParam(&h264pps);
		break;
	case LIVE_VIDEO_H264_UPDATE:
		gp_wifi_trans_ctx->wifi_liver_ready(NULL);
		break ;
	default:
		 break;
	}
	
	return 0;
}

static int liver_send_Jpg_cb(void *fTo, int maxsize)
{
	if(gp_wifi_trans_ctx->wifi_liver_send_Jpg_cb) return gp_wifi_trans_ctx->wifi_liver_send_Jpg_cb(fTo, maxsize);
	else
		return 0;
}

WIFI_TRANS_CTX_T * wifi_trans_create(void)
{
	if(gp_wifi_trans_ctx) {
		return gp_wifi_trans_ctx;
	}

	gp_wifi_trans_ctx = (struct wifi_trans_context_st *)malloc(sizeof(struct wifi_trans_context_st));
	if(!gp_wifi_trans_ctx) {
		PRINTF("%s:malloc wifi_trans_context_st error\n", __func__);
		return NULL;
	}
	memset(gp_wifi_trans_ctx, 0, sizeof(struct wifi_trans_context_st));
	liverCreate();
	liverRegisterLinkCB(liver_status_cb);
	//liverRegisterJpegCB(liver_send_Jpg_cb);
	liverRegisterFrameCb(liver_send_Jpg_cb, H264_STREAM);
	//liverLiverRegisterStreamCb(img_trans_vid_enc_mgr_ready_buf_get,img_trans_vid_enc_mgr_empty_buf_put,H264_STREAM);
	liverLiverRegisterStreamCb(NULL, NULL, H264_STREAM);

	return (WIFI_TRANS_CTX_T *)gp_wifi_trans_ctx;
}

int wifi_trans_register_liver_control(WIFI_TRANS_CTX_T *p_ctx,int type,TRANS_CONTROL trans)
{
	struct wifi_trans_context_st *p_wifi_trans_ctx = (struct wifi_trans_context_st *)p_ctx;
	
	if(!p_ctx || !trans) {
		PRINTF("%s:liver control invalid param\n", __func__);
		return -1;
	}

	if(type==TRANS_START){
		p_wifi_trans_ctx->liver_trans_start=trans;
	}else if(type==TRANS_STOP){
		p_wifi_trans_ctx->liver_trans_stop=trans;
	}
		return 0;
}

int wifi_trans_register_liver_status(WIFI_TRANS_CTX_T *p_ctx, int (*wifi_get_liver_status)(void *cmd))
{
	struct wifi_trans_context_st *p_wifi_trans_ctx = (struct wifi_trans_context_st *)p_ctx;
	
	if(!p_ctx || !wifi_get_liver_status) {
		PRINTF("%s:invalid param\n", __func__);
		return -1;
	}
	p_wifi_trans_ctx->wifi_get_liver_status = wifi_get_liver_status;
	return 0;
}

int wifi_trans_register_liver_send(WIFI_TRANS_CTX_T *p_ctx, int (*wifi_liver_send_Jpg_cb)(void *fTo, int maxsize))
{
	struct wifi_trans_context_st *p_wifi_trans_ctx = (struct wifi_trans_context_st *)p_ctx;

	if(!p_ctx || !wifi_liver_send_Jpg_cb) {
		PRINTF("%s:invalid param\n", __func__);
		return -1;
	}
	p_wifi_trans_ctx->wifi_liver_send_Jpg_cb = wifi_liver_send_Jpg_cb;

	return 0;
}

int wifi_trans_register_liver_ready(WIFI_TRANS_CTX_T *p_ctx, int (*wifi_liver_ready)(void *p_param))
{
	struct wifi_trans_context_st *p_wifi_trans_ctx = (struct wifi_trans_context_st *)p_ctx;

	if(!p_ctx || !wifi_liver_ready) {
		PRINTF("%s:invalid param\n", __func__);
		return -1;
	}
	p_wifi_trans_ctx->wifi_liver_ready = wifi_liver_ready;
	return 0;
}

int wifi_trans_get_trigger_id(WIFI_TRANS_CTX_T *p_ctx)
{
	struct wifi_trans_context_st *p_wifi_trans_ctx = (struct wifi_trans_context_st *)p_ctx;

	if(!p_ctx) {
		PRINTF("%s:invalid param\n", __func__);
		return -1;
	}

	return p_wifi_trans_ctx->h264_trigger_id;
}

int wifi_trans_trigger(WIFI_TRANS_CTX_T *p_ctx)
{
	struct wifi_trans_context_st *p_wifi_trans_ctx = (struct wifi_trans_context_st *)p_ctx;

	if(!p_ctx) {
		PRINTF("%s:invalid param\n", __func__);
		return -1;
	}

	if(p_wifi_trans_ctx->h264_trigger_id){
	liverTrigger(p_wifi_trans_ctx->h264_trigger_id);
	}
	return 0;
}
