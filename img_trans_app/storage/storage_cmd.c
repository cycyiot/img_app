#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/mount.h> 
#include <sys/vfs.h>
#include <storage_mgr/storage_mgr.h>
#include <system/event.h>
#include <signal.h>
#include <system/sys_param.h>
#include <net/cmd_handle.h>

void card_state_notify(card_s *cardInfo)
{
	struct json_object *jsonCfmPacketObj = NULL;
	struct json_object *jsonParamObj = NULL;
	int len;
	
	jsonCfmPacketObj = json_object_new_object();
	json_object_object_add(jsonCfmPacketObj, "REPORT", json_object_new_int(RE_CARD));
	jsonParamObj = json_object_new_object();
	json_object_object_add(jsonParamObj, "online", json_object_new_int(cardInfo->status));
	
	if(cardInfo->status == MOUNTED && !card_fs_capacity_get(cardInfo)){
		json_object_object_add(jsonParamObj, "freeSpace", json_object_new_int(cardInfo->rsize/(1024*1024)));
		json_object_object_add(jsonParamObj, "usedSpace", json_object_new_int((cardInfo->tsize-cardInfo->rsize)/(1024*1024)));
		json_object_object_add(jsonParamObj, "totalSpace", json_object_new_int(cardInfo->tsize/(1024*1024)));
	}
	json_object_object_add(jsonCfmPacketObj, "PARAM", jsonParamObj);
	
	PRINTF("%s\n", json_object_to_json_string(jsonCfmPacketObj));
	len = strlen(json_object_to_json_string(jsonCfmPacketObj)) + 1;		
	TcpClientSend(0, json_object_to_json_string(jsonCfmPacketObj), len);
	json_object_put(jsonCfmPacketObj);
}

int storage_mgr_cmd_handler(struct cmd_param *cmdParam)
{
	card_fstype fstype;
	struct json_object *jsonSubParamObj;
	int ret= -1;

	if(is_error(cmdParam->param))
		goto err_exit;
	jsonSubParamObj = json_object_object_get(cmdParam->param, "filesystem");
	if(json_object_get_type(jsonSubParamObj) == json_type_int){
		fstype = (card_fstype)json_object_get_int(jsonSubParamObj);
		printf("format disk cmd ...\n");
		ret = card_format(fstype);
		if(!ret){
			user_folder_file_list_free();
			user_folder_refresh();
			user_folder_list_printf();
		 }
	}
err_exit:
	cmd_response(ret, cmdParam);
	return ret;
}

struct cmd storage_mgr_cmd[] = {
	{CMD_DISK_FORMAT, storage_mgr_cmd_handler},
};

int storage_cmd_init(void)
{
	return cmd_register(storage_mgr_cmd, ARRAY_SIZE(storage_mgr_cmd));
}
