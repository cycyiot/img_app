#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/mount.h> 
#include <sys/vfs.h>
#include <storage_mgr/storage_mgr.h>
#include <system/event.h>
#include <signal.h>
#include <system/sys_param.h>

static INT8U STORAGE_MGR_LOG = 1;
#define STORAGE_MGR_DBG_LOG	 if(STORAGE_MGR_LOG)  PRINTF
static pthread_mutex_t storage_mgr_lock;

static void card_detection(int sig_no)
{
	card_s cardInfo;
	struct event_data event;
	
	PRINTF("%s: get signal %d\n", __func__, sig_no);
	memset((void*)&cardInfo, 0 ,sizeof(card_s));
	if(card_insert_detect()){
		if(!card_mount("/mnt/")){
			PRINTF("card mount success !\n");
			user_folder_refresh();
		}		
		card_status_get(&cardInfo); 	
	}else{
		if(!card_unmount()){
			PRINTF("card unmount success !\n");
			user_folder_file_list_free();
		}

		cardInfo.status = UNMOUNT;
		event.id = CARD_DISABLE;
		event.client = 0;
		event_put(&event);
	}
	card_state_notify(&cardInfo);
}



INT32S storage_mgr_init(void)
{

	STORAGE_MGR_DBG_LOG("Entering %s init ...\n", __func__);

	card_init();
	user_folder_init(PHOTO_FOLDER,PHOTO_FILE_TYPE);
	user_folder_init(VIDEO_FOLDER,VIDEO_FILE_TYPE);
	user_folder_init(PHOTO_THUMB_FOLDER,PHOTO_THUMB_FILE_TYPE);
	user_folder_init(VIDEO_THUMB_FOLDER,VIDEO_THUMB_FILE_TYPE);
	pthread_mutex_init(&storage_mgr_lock, NULL);
	signal(SIGUSR2, card_detection);
	if(card_insert_detect())
	{
		if(!card_mount("/mnt/"))
		{
			STORAGE_MGR_DBG_LOG("card mount success !\n");
		//	user_folder_refresh();
		}
	}
	else
		STORAGE_MGR_DBG_LOG("%s: card no detect!\n", __func__);
    user_folder_refresh();
	storage_cmd_init();
	file_cmd_init();

	return 0;
}

INT32S storage_mgr_uninit(void)
{

	STORAGE_MGR_DBG_LOG("Entering %s uninit ...\n", __func__);
	user_folder_exit();
	card_uninit();
	pthread_mutex_destroy(&storage_mgr_lock);
	
	return 0;
}
