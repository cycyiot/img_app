#include <storage_mgr/ap_tf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/mount.h> 
#include <sys/vfs.h>
#include <semaphore.h>
#include <sys/mount.h>
#include <errno.h>

#define		LOCK	1
#if defined(LOCK)
#define	MUTEX_LOCK(mtx)			if(pthread_mutex_lock(&mtx)){\
									PRINTF("%s:%d mutex lock fail\n",__func__,__LINE__);\
								}
#define	MUTEX_UNLOCK(mtx)		if(pthread_mutex_unlock(&mtx)){\
									PRINTF("%s:%d mutex unlock fail\n",__func__,__LINE__);\
								}
#else
#define MUTEX_LOCK(mtx)	 
#define MUTEX_UNLOCK(mtx)
#endif


typedef struct disk_cluster_sz_S
{
	unsigned	disk_sz_min ;	// unit:MByte
	unsigned	disk_sz_max ;
	unsigned	cluster_sz ;	// unit:Byte
}disk_cluster_sz_t;


static int CARD_ERR_L = 1;
static int CARD_WARN_L = 1;
static int CARD_DBG_L = 1;
#define CARD_ERR_LOG	 if(CARD_ERR_L)  PRINTF
#define CARD_WARN_LOG	 if(CARD_WARN_L)  PRINTF
#define CARD_DBG_LOG	 if(CARD_DBG_L)  PRINTF

static card_s *card;

static disk_cluster_sz_t disk_cluster_sz_array[] =
{
	{0L,	4096,	4096},			// <=4G , format to 4K
	{4096,	8192,	16384},			// >4G && <= 8G ,format to 16K
	{8192,	0x1ffffff,	32768},			// >8G ,format to 32K
};



int card_insert_detect(void)
{
	if(NULL == card)
		return -1;
	if(access(CARD_DEV_NODE1,F_OK) != 0){
		MUTEX_LOCK(card->lock);
		card->valid = 0;
		card->status = UNMOUNT;
		MUTEX_UNLOCK(card->lock);
		return 0;
	}else{
		card->valid = 1;
		if(access(CARD_DEV_NODE2,F_OK) != 0){
			MUTEX_LOCK(card->lock);
			memset((void*)card->dname,0,MAX_PATH);
			strcpy(card->dname,(const char*)CARD_DEV_NODE1);
			MUTEX_UNLOCK(card->lock);
		}else{
			MUTEX_LOCK(card->lock);
			memset((void*)card->dname,0,MAX_PATH);
			strcpy(card->dname,(const char*)CARD_DEV_NODE2);
			MUTEX_UNLOCK(card->lock);
		}
	}
	return 1;
}

int card_status_get(card_s *tcard)
{
	if(NULL == card || NULL == tcard)
		return -1;
	memcpy((void*)tcard, (const void*)card, sizeof(card_s));
	return 0;
}
int card_is_mounted(void)
{
	if(NULL == card)
		return 0;
	return (card->status == MOUNTED);
}
int disk_space_is_full(void)
{
	card_s card_cap;

	memset((void*)&card_cap, 0, sizeof(card_s));
	if(card_fs_capacity_get(&card_cap))
		return -1;
	if(card_cap.rsize < TF_FREE_SPACE_MIN)
		return 1;
	return 0;
}
/* get card capacity via file system node,capacity unit is MB */
int card_capacity_get(unsigned int *cap_mb)
{
	FILE *fp;
	char *buf[64];
	int sectors;

	if(NULL == cap_mb)
		return -1;
	*cap_mb = -1;
	if(access(CARD_NSECTORS_NODE,F_OK) != 0) {
		CARD_ERR_LOG("%s:%d-%s file is not exist\n",__func__,__LINE__,CARD_NSECTORS_NODE);
		return -1;
	}
	fp = fopen(CARD_NSECTORS_NODE,"rb");
	if(!fp) {
		CARD_ERR_LOG("%s:%d-open %s file fail\n",__func__,__LINE__,CARD_NSECTORS_NODE);
		return -1;
	}
	memset(buf, 0 ,sizeof(buf));
	fread(buf, 1, 64,fp);	
	fclose(fp);
	sectors = atoi((const char *)buf);
	*cap_mb = sectors/2/1024;
	
	return 0;
}

int card_fs_capacity_get(card_s *tcard)
{
	struct statfs diskinfo;

	if(NULL == tcard)
		return -1;
	//memset(tcard, 0, sizeof(card_s));
	if(card_is_mounted()){
		if(statfs(card->mpath, &diskinfo)){
			CARD_ERR_LOG("%s():%d get disk info fail!\n",__func__,__LINE__);
			return -1;
		}
		MUTEX_LOCK(card->lock);
		card->tsize = (unsigned long long)diskinfo.f_blocks * diskinfo.f_bsize;
		card->rsize = (unsigned long long)diskinfo.f_bfree * diskinfo.f_bsize;
		MUTEX_UNLOCK(card->lock);
		memcpy((void*)tcard, (const char*)card, sizeof(card_s));
		return 0;
	}

	return -1;
}
 
extern int errno;
int card_mount(const char *mpath)
{
	const char *path = mpath;
	char buf[100];
	int ret = -1;
	unsigned int cap;
	
	if(NULL == card || NULL == mpath)
		return -1;
	if(!card->valid){
		CARD_WARN_LOG("%s():%d card is not inserted\n",__func__,__LINE__);
		return -1;
	}
	if(!card_capacity_get(&cap)) {
		if(cap > CARD_CAPACITY_MAX) {
			CARD_WARN_LOG("%s():%d card capacity is larger than 32GB,not support !\n",__func__,__LINE__);
			MUTEX_LOCK(card->lock);
			card->status = NOSUPPORT;
			MUTEX_UNLOCK(card->lock);
			return -1;
		}
	}
	if(card_is_mounted()){
		CARD_WARN_LOG("%s():%d card has already mounted(%s)\n",__func__,__LINE__,card->mpath);
		return 0;
	}
	ret = mount((const char*)card->dname,path,(const char*)"vfat",0,0);
	if(ret) {
		memset(buf, 0, sizeof(buf));
		snprintf(buf, sizeof(buf), "mount %s %s", card->dname, path);
		ret = system(buf);
		if(!ret) {
			CARD_DBG_LOG("%s:%d-mount %s(exFAT) success!\n",__func__,__LINE__,card->dname);
		}
	}
	if(ret){
		CARD_ERR_LOG("%s():%d mount %s fail!\n",__func__,__LINE__,card->dname);
		if(errno == EBUSY){
			CARD_WARN_LOG("%s():%d-card have already mounted\n",__func__,__LINE__);
			goto next;
		}
		MUTEX_LOCK(card->lock);
		card->status = UNMOUNT;
		memset(card->mpath, 0 ,MAX_PATH);
		MUTEX_UNLOCK(card->lock);
		return -1;
	}
next:	
	MUTEX_LOCK(card->lock);
	strcpy(card->mpath, path);
	card->status = MOUNTED;		
	MUTEX_UNLOCK(card->lock);
	return 0;
}
int card_unmount(void)
{
	int ret = -1;
	
	if(NULL == card)
		return -1;
//	if(!card->status || !card->mpath[0])
//		return 0;
	if(!card->mpath[0])
		return -1;
	ret = umount((const char*)card->mpath);
	if(ret){
		CARD_ERR_LOG("%s():%d-umount %s fail\n",__func__,__LINE__,card->mpath);
		return -1;
	}
	MUTEX_LOCK(card->lock);
	card->status = UNMOUNT;
	memset(card->mpath, 0 ,MAX_PATH);
	MUTEX_UNLOCK(card->lock);
	return 0;
}

/*confirmed whether or nor need to format.return 1:is need format,other is not*/
int is_need_format_card(const char *mpath)
{
	int n = 0;	

	for(n = 0; n < 5; n++){
		card_insert_detect();
		if(!card_mount(mpath))
			break;
	}
	if(n >= 5)
		return 1;	
	return 0;
}
static int _card_format(card_fstype ftype)
{
	char cmd[128]={0};
	char buf[50];
	char *fmt = NULL;
	int cluster_size = 0;
	int i = 0;
	int total = 0;
	unsigned int cap = 0;

	if(NULL == card)
		return -1;
	if(!card->valid){
		CARD_WARN_LOG("%s():%d card is not inserted!\n",__func__,__LINE__);
		return -1;
	}
	/*format disk according to file system type*/
	switch(ftype){
	case FAT32_FS:	//FAT32
		memset(buf, 0, 50);
		cluster_size = 16*1024/512;
		if(card_capacity_get(&cap)) {
			
		}else {
			total = (int)cap;
			for(i = 0; i < sizeof(disk_cluster_sz_array)/sizeof(disk_cluster_sz_array[0]); i++){
				if(total > disk_cluster_sz_array[i].disk_sz_min && total < disk_cluster_sz_array[i].disk_sz_max){
					cluster_size = disk_cluster_sz_array[i].cluster_sz/512;
					break;
				}
			}
		}
		snprintf(buf, 49, "mkfs.vfat -s %d", cluster_size);
		fmt = buf;
		break;
	default:
		CARD_WARN_LOG("%s():%d-not supported file system type %d \n", __func__,__LINE__,card->ftype);
		fmt = NULL;
		break;
	}
	if(fmt){
		sprintf(cmd,"%s %s",fmt,card->dname);
		if(system(cmd) < 0){
			CARD_ERR_LOG("%s():%d format %s fail!\n",__func__,__LINE__,card->dname);
			return -1;
		}
		MUTEX_LOCK(card->lock);
		card->ftype = ftype;
		MUTEX_UNLOCK(card->lock);
		return 0;
	}	
	return -1;
}
int card_format(card_fstype ftype)
{
	card_s card_cap;
	
	if(card_unmount()){
		CARD_ERR_LOG("%s():%d-unmount sd card fail\n",__func__,__LINE__);
		return -1;
	}
	if(_card_format(ftype))
		return -1;
	/* must be mount again to ensure get card space is correct*/
	if(card_mount("/mnt/")){
		CARD_ERR_LOG("%s():%d-mount sd card success\n",__func__,__LINE__);
		return -1;				
	}
	card_fs_capacity_get(&card_cap);
	return 0;
}

static int get_disk_bsize(char *mount_path)
{
	struct statfs diskInfo;

	statfs(mount_path, &diskInfo);

	return diskInfo.f_bsize;
}

#if 1

static int judge_format_by_cluster(int total_size, int cluster_size)
{
	int i;
	int ret = 0;
	
	for(i = 0; i < sizeof(disk_cluster_sz_array)/sizeof(disk_cluster_sz_array[0]); i++){
		if(total_size > disk_cluster_sz_array[i].disk_sz_min && total_size < disk_cluster_sz_array[i].disk_sz_max){
			if(cluster_size == disk_cluster_sz_array[i].cluster_sz)
				ret = 1;
			break;
		}
	}

	return ret;
}
#else
static int judge_format_by_hidefile(char *p_mount_point)
{
	char buf[50];
#define HIDE_FILE_NAME    ".mr100_format"

	memset(buf, 0, 50);
	snprintf(buf, 50, "%s/%s", p_mount_point, HIDE_FILE_NAME);
	if(access(buf,F_OK) != 0){
		return 0; /* not exist */
	}else {
		return 1; /* exist */
	}
}
#endif
/* 
 *  
 * return 
 * -1: error
 * 0:  incorrect
 * 1:  correct
 *
 */
int format_is_correct(void)
{
	int total = 0;
	int bsize = 0;
	int ret = 0;
	unsigned int cap_mb = 0;
	char buf[50];
#define TMP_PATH    "/tmp/tfcard/"

	
	if(card_capacity_get(&cap_mb)) {
		return -1;
	}
	total = (int)cap_mb;

	memset(buf, 0, 50);
	snprintf(buf, 50, "mkdir %s", TMP_PATH);
	system(buf);
	ret = mount((const char*)card->dname,TMP_PATH,(const char*)"vfat",0,0);
	if(ret) {
		CARD_WARN_LOG("%s:mount %s %s failed\n", __func__, card->dname, TMP_PATH);
		ret = 0;
		return ret;
	}

	bsize = get_disk_bsize(TMP_PATH);

#if 1
	//printf("%s:get cluster size %d\n", __func__, bsize);
	ret = judge_format_by_cluster(total, bsize);
#else
	ret = judge_format_by_hidefile(TMP_PATH);
#endif

	umount(TMP_PATH);
	memset(buf, 0, 50);
	snprintf(buf, 50, "rm -fr %s", TMP_PATH);
	system(buf);

	return ret;
}


int card_init(void)
{
	/*malloc memory*/
	card = (card_s *)malloc(sizeof(card_s));
	if(NULL == card){
		CARD_ERR_LOG("%s():%d malloc memory fail!\n",__func__,__LINE__);
		return -1;
	}	
	memset((void*)card, 0 , sizeof(card_s));
#if	defined(LOCK)
	/*init mutex lock*/
	if(pthread_mutex_init(&card->lock, NULL)){
		CARD_ERR_LOG("%s():%d mutex initialization fail\n",__func__,__LINE__);
		free(card);
		return -1;
	}	
#endif 	
	return 0;
}
int card_uninit(void)
{
	if(NULL == card)
		return -1;
	if(card_unmount()){
		CARD_ERR_LOG("card unmount fail\n");
		return -1;
	}
#if	defined(LOCK)
	if(pthread_mutex_destroy(&card->lock)){
		CARD_ERR_LOG("%s:%d mutex destroy fail\n",__func__,__LINE__);
		return -1;
	}
#endif	
	memset((void*)card, 0, sizeof(card_s));
	free(card);	
	card = NULL;	
	return 0;
}

#ifdef	_CARD_DEMO_
int card_demo(int argc, char *argv[])
{
	int psts = 0,csts = 0;
	unsigned int card_cap;
	card_s card_info;
	
	PRINTF("=====card demo start run ...=====\n");
	card_capacity_get(&card_cap);
	PRINTF("get tf card capacity is %d MB \n",card_cap);
	if(card_init()){
		PRINTF("card init fail\n");
		return 0;
	}
	while(1){
		csts = card_insert_detect();
		if((csts ^ psts) & csts){
			if(!is_need_format_card("/mnt/")){
				PRINTF("card mount success !\n");
				if(!card_fs_capacity_get(&card_info)){
					PRINTF("card (tsize,rsize)=(%llu,%llu)\n",card_info.tsize,card_info.rsize);
				}
				PRINTF("\n===wirte data to card ...===\n");
				system("dd if=/dev/zero of=/mnt/test.bin bs=8M count=8");
				PRINTF("\n===wirte data to card end===\n");
				if(!card_fs_capacity_get(&card_info)){
					PRINTF("%d-card (tsize,rsize)=(%llu,%llu)\n",__LINE__,card_info.tsize,card_info.rsize);
				}
				sync();
				PRINTF("===foramt tf card ...===\n");
				if(!card_format(FAT32_FS)){
					PRINTF("card format success !\n");
					if(!card_unmount() && !card_mount("/mnt/")){
						if(!card_fs_capacity_get(&card_info)){
							PRINTF("%d-card (tsize,rsize)=(%llu,%llu)\n",__LINE__,card_info.tsize,card_info.rsize);
						}
					}
				}
			}
		}
		psts = csts;
		usleep(1000000);
	}	
	

	return 0;
}
#endif /*_CARD_DEMO_*/
