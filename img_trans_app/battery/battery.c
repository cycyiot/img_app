#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h> 
#include <battery.h>
#include <pthread.h>
#include <signal.h>
#include <net/cmd_handle.h>
#include <system/sys_param.h>
#include <platform_cfg.h>

#define BATTERY_CHARGE_POLL
#define BATTERY_PATH "/dev/lradc_battery"

struct battery_data {
	int battery_fd;
	struct battery_info info;
};

static struct battery_data *gp_battery_dev = NULL;

void battery_state_notify(struct battery_info *battery)
{
	struct json_object *jsonCfmPacketObj = NULL;
	struct json_object *jsonParamBattery;
	int len;
	
	jsonCfmPacketObj = json_object_new_object();
	json_object_object_add(jsonCfmPacketObj, "REPORT", json_object_new_int(RE_BATTERY));
	jsonParamBattery = json_object_new_object();
	json_object_object_add(jsonParamBattery, "charge", json_object_new_int(battery->state));
	json_object_object_add(jsonParamBattery, "power", json_object_new_int((int)battery->voltage));	
	json_object_object_add(jsonCfmPacketObj, "PARAM", jsonParamBattery);

	PRINTF("%s\n", json_object_to_json_string(jsonCfmPacketObj));
	len = strlen(json_object_to_json_string(jsonCfmPacketObj)) + 1;		
	TcpClientSend(0, json_object_to_json_string(jsonCfmPacketObj), len);
	json_object_put(jsonCfmPacketObj);
}

#ifndef BATTERY_CHARGE_POLL
void my_signal_fun(int signum)
{
	struct battery_info battery;

	if(battery_info_get(&battery) > 0)
		battery_state_notify(&battery);
}
#endif

int battery_info_get(struct battery_info *battery)
{
	int ret;
	ret = read(gp_battery_dev->battery_fd, battery, sizeof(struct battery_info));
	battery->voltage = (battery->voltage*2200*(3300+1000)/63/1000 + 150) * 100 / BATTERY_MAX_VOLTAGE;
	gp_battery_dev->info = *battery;

	return ret;
}

enum battery_charge_state battery_is_charging(void)
{
	struct battery_info info;

	if(gp_battery_dev == NULL){
		return 0;
	}
#ifdef BATTERY_CHARGE_POLL
	battery_info_get(&info);
#endif
	return gp_battery_dev->info.state;
}

int battery_init(void)
{
#ifndef BATTERY_CHARGE_POLL
	int Oflags;
#endif
	gp_battery_dev = malloc(sizeof(*gp_battery_dev));
	if(gp_battery_dev == NULL)
	{
		PRINTF("malloc");
		goto exit_err;
	}

	if(access(BATTERY_PATH, F_OK) != 0){
		PRINTF("%s isn't exit\r\n", BATTERY_PATH);
		goto open_err;
	}
	
	gp_battery_dev->battery_fd = open(BATTERY_PATH, O_RDONLY);
	if(gp_battery_dev->battery_fd == -1)
	{
		PRINTF("open");
		goto open_err;
	}

#ifndef	BATTERY_CHARGE_POLL
	fcntl(gp_battery_dev->battery_fd, F_SETOWN, getpid());
	Oflags = fcntl(gp_battery_dev->battery_fd, F_GETFL); 
	fcntl(gp_battery_dev->battery_fd, F_SETFL, Oflags | FASYNC);
	signal(SIGIO, my_signal_fun);
#endif
	battery_info_get(&gp_battery_dev->info);
	
	return 0;
	
open_err:
	if(gp_battery_dev){
		free(gp_battery_dev);
		gp_battery_dev = NULL;
	}
exit_err:
	return -1;
}

int battery_uninit(void)
{
	int ret;
	
	ret = close(gp_battery_dev->battery_fd);
	if(gp_battery_dev){
		free(gp_battery_dev);
		gp_battery_dev = NULL;
	}
	
	return ret;
}
